# Ventajas del desarrollo web

- Escribir una vez, ejecutar en cualquier parte
- Facilidad de actualización
- La carga de trabajo importante se hace en el servidor
- Disponibilidad de herramientas y frameworks
- Base de desarrolladores alta
- Estándares en continua evolución


