# Ejemplo 1
En el siguiente ejemplo tenemos un programa en PHP que contiene código que va a ser ejecutado en el servidor y código que será ejecutado en el cliente. 

```html
<!-- holamundo.php -->
<html>
 <head>
  <title>Hola mundo PHP</title>
  <script>
    alert('Hola mundo JavaScript');
  </script>
 </head>
 <body>
 <?php echo '<p>Hola Mundo PHP</p>'; ?>
 </body>
</html>
```