# Crear un proyecto
En este apartado vamos a crear nuestro proyecto. 

## Crea el proyecto
Para crear el proyecto vamos a partir de las instrucciones el la guía de desarrollo en la sección [starting](https://ionicframework.com/docs/developing/starting). Siguiendo la sección, vamos a irnos al directorio donde vamos a crear nuestro proyecto y vamos a ejecutar la siguiente secuencia de comandos:

```shell
$ ionic start taskmanApp sidemenu --type=angular
[INFO] Existing git project found (C:/Users/Juanjo/RepoRoot/materiales-de-clase/daw/daw2-dwec-profesorado). Git
       operations are disabled.
√ Preparing directory .\taskmanApp in 991.90μs
√ Downloading and extracting sidemenu starter in 146.19ms
> ionic integrations enable capacitor --quiet -- taskmanApp io.ionic.starter
> npm.cmd i --save -E @capacitor/core@latest
npm WARN deprecated @npmcli/move-file@2.0.1: This functionality has been moved to @npmcli/fs

added 1127 packages, and audited 1128 packages in 1m

189 packages are looking for funding
  run `npm fund` for details

2 high severity vulnerabilities

To address all issues (including breaking changes), run:
  npm audit fix --force

Run `npm audit` for details.
> npm.cmd i -D -E @capacitor/cli@latest

added 37 packages, and audited 1165 packages in 10s

189 packages are looking for funding
  run `npm fund` for details

2 high severity vulnerabilities

To address all issues (including breaking changes), run:
  npm audit fix --force

Run `npm audit` for details.
> npm.cmd i --save -E @capacitor/haptics @capacitor/app @capacitor/keyboard @capacitor/status-bar

added 4 packages, and audited 1169 packages in 5s

189 packages are looking for funding
  run `npm fund` for details

2 high severity vulnerabilities

To address all issues (including breaking changes), run:
  npm audit fix --force

Run `npm audit` for details.
> capacitor.cmd init taskmanApp io.ionic.starter --web-dir www
√ Creating capacitor.config.ts in C:\Users\Juanjo\RepoRoot\materiales-de-clase\daw\daw2-dwec-profesorado\taskman\04-taskman-php-ionic\taskmanApp in 3.05ms
[success] capacitor.config.ts created!

Next steps:
https://capacitorjs.com/docs/getting-started#where-to-go-next
[OK] Integration capacitor added!

Installing dependencies may take several minutes.

  ──────────────────────────────────────────────────────────────────────

      Ionic Enterprise, platform and solutions for teams by Ionic

                  Powerful library of native APIs
                 A supercharged platform for teams

         Learn more: https://ion.link/enterprise

  ──────────────────────────────────────────────────────────────────────


> npm.cmd i

up to date, audited 1169 packages in 1s

189 packages are looking for funding
  run `npm fund` for details

2 high severity vulnerabilities

To address all issues (including breaking changes), run:
  npm audit fix --force

Run `npm audit` for details.

Join the Ionic Community! 💙

Connect with millions of developers on the Ionic Forum and get access to live events, news updates, and more.

? Create free Ionic account? No

Your Ionic app is ready! Follow these next steps:

- Go to your new project: cd .\taskmanApp
- Run ionic serve within the app directory to see your app in the browser
- Run ionic capacitor add to add a native iOS or Android project using Capacitor
- Generate your app icon and splash screens using cordova-res --skip-config --copy
- Explore the Ionic docs for components, tutorials, and more: https://ion.link/docs
- Building an enterprise app? Ionic has Enterprise Support and Features: https://ion.link/enterprise-edition
```

En el comando anterior, sidemenu corresponde con la plantilla. Esta plantilla generará un proyecto vacío con un menú lateral. Si queremos ver todas las plantillas podemos ejecutar el comando:

```shell
$ ionic start --list

Starters for @ionic/angular (--type=angular)

name         | description
--------------------------------------------------------------------------------------
tabs         | A starting project with a simple tabbed interface
sidemenu     | A starting project with a side menu with navigation in the content area
blank        | A blank starter project
list         | A starting project with a list
my-first-app | A template for the "Build Your First App" tutorial
```

## Arrancar la aplicación
Para arrancar la aplicación tendrás que ejecuta una secuencia como la que indicaba ionic en la salida:

```shell
$ ionic serve
> ng.cmd run app:serve --host=localhost --port=8100
[ng] - Generating browser application bundles (phase: setup)...
[ng] √ Browser application bundle generation complete.
[ng] Initial Chunk Files                                                                                     | Names                     |  Raw Size
[ng] vendor.js                                                                                               | vendor                    |   2.99 MB |
[ng] polyfills.js                                                                                            | polyfills                 | 318.04 kB |
[ng] styles.css, styles.js                                                                                   | styles                    | 249.32 kB |
[ng] main.js                                                                                                 | main                      |  14.83 kB |
[ng] runtime.js                                                                                              | runtime                   |  14.11 kB |
[ng]
[ng]                                                                                                         | Initial Total             |   3.57 MB
[ng]
[ng] Lazy Chunk Files                                                                                        | Names                     |  Raw Size
[ng] node_modules_ionic_core_dist_esm_swiper_bundle-28080340_js.js                                           | swiper-bundle-28080340-js | 199.26 kB |
[ng] polyfills-core-js.js                                                                                    | polyfills-core-js         | 152.87 kB |
[ng] node_modules_ionic_core_dist_esm_ion-datetime_3_entry_js.js                                             | -                         | 136.28 kB |
[ng] node_modules_ionic_core_dist_esm_ion-item_8_entry_js.js                                                 | -                         | 100.59 kB |
[ng] node_modules_ionic_core_dist_esm_ion-app_8_entry_js.js                                                  | -                         |  84.88 kB |
[ng] node_modules_ionic_core_dist_esm_ion-modal_entry_js.js                                                  | -                         |  84.51 kB |
[ng] node_modules_ionic_core_dist_esm_ion-popover_entry_js.js                                                | -                         |  63.17 kB |
[ng] node_modules_ionic_core_dist_esm_ion-slide_2_entry_js.js                                                | -                         |  61.48 kB |
[ng] default-node_modules_ionic_core_dist_esm_data-cb72448c_js-node_modules_ionic_core_dist_esm_th-29e28e.js | -                         |  56.12 kB |
[ng] node_modules_ionic_core_dist_esm_ion-refresher_2_entry_js.js                                            | -                         |  55.40 kB |
[ng] node_modules_ionic_core_dist_esm_ion-alert_entry_js.js                                                  | -                         |  52.15 kB |
[ng] common.js                                                                                               | common                    |  45.73 kB |
[ng] node_modules_ionic_core_dist_esm_ion-segment_2_entry_js.js                                              | -                         |  43.51 kB |
[ng] node_modules_ionic_core_dist_esm_ion-menu_3_entry_js.js                                                 | -                         |  42.34 kB |
[ng] node_modules_ionic_core_dist_esm_ion-button_2_entry_js.js                                               | -                         |  40.63 kB |
[ng] node_modules_ionic_core_dist_esm_ion-item-option_3_entry_js.js                                          | -                         |  39.61 kB |
[ng] node_modules_ionic_core_dist_esm_ion-range_entry_js.js                                                  | -                         |  38.03 kB |
[ng] node_modules_ionic_core_dist_esm_ion-searchbar_entry_js.js                                              | -                         |  37.04 kB |
[ng] node_modules_ionic_core_dist_esm_ion-nav_2_entry_js.js                                                  | -                         |  36.75 kB |
[ng] node_modules_ionic_core_dist_esm_ion-route_4_entry_js.js                                                | -                         |  36.45 kB |
[ng] node_modules_ionic_core_dist_esm_ion-select_3_entry_js.js                                               | -                         |  34.89 kB |
[ng] node_modules_ionic_core_dist_esm_ion-action-sheet_entry_js.js                                           | -                         |  33.47 kB |
[ng] node_modules_ionic_core_dist_esm_ion-fab_3_entry_js.js                                                  | -                         |  30.55 kB |
[ng] node_modules_ionic_core_dist_esm_ion-accordion_2_entry_js.js                                            | -                         |  28.38 kB |
[ng] polyfills-dom.js                                                                                        | polyfills-dom             |  26.82 kB |
[ng] node_modules_ionic_core_dist_esm_ion-tab-bar_2_entry_js.js                                              | -                         |  26.66 kB |
[ng] node_modules_ionic_core_dist_esm_ion-toast_entry_js.js                                                  | -                         |  26.54 kB |
[ng] node_modules_ionic_core_dist_esm_ion-input_entry_js.js                                                  | -                         |  25.78 kB |
[ng] node_modules_ionic_core_dist_esm_ion-breadcrumb_2_entry_js.js                                           | -                         |  25.34 kB |
[ng] node_modules_ionic_core_dist_esm_ion-progress-bar_entry_js.js                                           | -                         |  24.70 kB |
[ng] node_modules_ionic_core_dist_esm_ion-textarea_entry_js.js                                               | -                         |  23.28 kB |
[ng] node_modules_ionic_core_dist_esm_ion-toggle_entry_js.js                                                 | -                         |  23.13 kB |
[ng] node_modules_ionic_core_dist_esm_ion-picker-internal_entry_js.js                                        | -                         |  22.37 kB |
[ng] node_modules_ionic_core_dist_esm_ion-back-button_entry_js.js                                            | -                         |  19.95 kB |
[ng] node_modules_ionic_core_dist_esm_ion-radio_2_entry_js.js                                                | -                         |  19.78 kB |
[ng] node_modules_ionic_core_dist_esm_ion-virtual-scroll_entry_js.js                                         | -                         |  19.72 kB |
[ng] node_modules_ionic_core_dist_esm_ion-datetime-button_entry_js.js                                        | -                         |  19.62 kB |
[ng] node_modules_ionic_core_dist_esm_input-shims-ca667d14_js.js                                             | input-shims-ca667d14-js   |  18.79 kB |
[ng] node_modules_ionic_core_dist_esm_ion-picker-column-internal_entry_js.js                                 | -                         |  18.70 kB |
[ng] node_modules_ionic_core_dist_esm_ion-card_5_entry_js.js                                                 | -                         |  18.66 kB |
[ng] node_modules_ionic_core_dist_esm_ion-loading_entry_js.js                                                | -                         |  18.06 kB |
[ng] node_modules_ionic_core_dist_esm_ion-infinite-scroll_2_entry_js.js                                      | -                         |  16.19 kB |
[ng] node_modules_ionic_core_dist_esm_ion-col_3_entry_js.js                                                  | -                         |  15.72 kB |
[ng] node_modules_ionic_core_dist_esm_ion-reorder_2_entry_js.js                                              | -                         |  15.17 kB |
[ng] node_modules_ionic_core_dist_esm_ion-checkbox_entry_js.js                                               | -                         |  14.26 kB |
[ng] node_modules_ionic_core_dist_esm_ion-spinner_entry_js.js                                                | -                         |  10.83 kB |
[ng] node_modules_ionic_core_dist_esm_ion-tab_2_entry_js.js                                                  | -                         |  10.32 kB |
[ng] node_modules_ionic_core_dist_esm_ion-split-pane_entry_js.js                                             | -                         |  10.26 kB |
[ng] node_modules_ionic_core_dist_esm_ion-chip_entry_js.js                                                   | -                         |   8.63 kB |
[ng] src_app_home_home_module_ts.js                                                                          | home-home-module          |   8.59 kB |
[ng] node_modules_ionic_core_dist_esm_ion-avatar_3_entry_js.js                                               | -                         |   8.38 kB |
[ng] node_modules_ionic_core_dist_esm_ion-ripple-effect_entry_js.js                                          | -                         |   6.79 kB |
[ng] node_modules_ionic_core_dist_esm_index-0bc00b33_js.js                                                   | index-0bc00b33-js         |   6.25 kB |
[ng] node_modules_ionic_core_dist_esm_ion-img_entry_js.js                                                    | -                         |   4.54 kB |
[ng] node_modules_ionic_core_dist_esm_ion-text_entry_js.js                                                   | -                         |   4.30 kB |
[ng] node_modules_ionic_core_dist_esm_ion-backdrop_entry_js.js                                               | -                         |   3.49 kB |
[ng] node_modules_ionic_core_dist_esm_status-tap-20472ffa_js.js                                              | status-tap-20472ffa-js    |   3.04 kB |
[ng]
[ng] Build at: 2022-11-16T21:49:01.237Z - Hash: 2ed86503a5203b6e - Time: 12884ms
[ng] √ Compiled successfully.

[INFO] Development server running!

       Local: http://localhost:8100

       Use Ctrl+C to quit this process

[INFO] Browser window opened to http://localhost:8100!
```

Si todo fue bien, se habrá abierto un navegador con la dirección http://localhost:8100/home cargada en la barra de direcciones. 


## Configurar vista móvil
Ahora vamos a configurar la vista en el navegador para que nos muestre la aplicación como si se ejecutara en un móvil. La forma más fácil es ir a herramientas de desarrollo y pulsar el icono que hay en la barra superior marcado como "toggle device toolbar".


