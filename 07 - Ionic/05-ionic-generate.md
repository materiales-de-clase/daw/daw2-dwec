# IONIC Generate
Anteriormente hemos visto como el comando ng generate permite generar los diferentes elementos disponibles en Angular. Para IONIC vamos a utilizar la herramienta equivalente ionic generate. Este comando nos permite generar el código para los diferentes tipos de entidades que componen la aplicación. Si quiero ver los diferentes elementos que puedo crear puedo ejecutar:

```shell
$ ionic generate
? What would you like to generate? (Use arrow keys)
> page
  component
  service
  module
  class
  directive
  guard
  ...
```

## Páginas
Para nuestra aplicación vamos a generar directamente todas las páginas que la componen utilizando el comando generate page. Esto va a crear una página dentro de un módulo independiente y va a configurar el enrutamiento a dicha página. Estas son las páginas que vamos a generar.

- pages/login
- pages/dashboard
- pages/listado
- pages/editar
- pages/ver

Por ejemplo, usamos el siguiente comando para crear la página de login:

```shell
$ ionic generate page pages/login --spec=false
> ng.cmd generate page pages/login --spec=false --project=app
CREATE src/app/pages/login/login-routing.module.ts (343 bytes)
CREATE src/app/pages/login/login.module.ts (465 bytes)
CREATE src/app/pages/login/login.page.html (124 bytes)
CREATE src/app/pages/login/login.page.ts (252 bytes)
CREATE src/app/pages/login/login.page.scss (0 bytes)
UPDATE src/app/app-routing.module.ts (631 bytes)
[OK] Generated page!
```

Vemos que ha creado varios archivos y ha actualizado el archivo de rutas. Utilizando el mismo comando, he generado el resto de las páginas.

## Servicios
Tenemos que generar los siguientes servicios

- services/AutenticacionService
- services/TareasService
- services/EstadosTareasService
- services/TiposTareaService
- services/UsuariosService
- services/DialogService: Lo voy a utilizar para acelerar la migración
  pero lo normal sería adaptar esto a IONIC.

Ejemplo

```
$ ionic generate service services/Autenticacion --skip-tests
> ng.cmd generate service services/Autenticacion --skip-tests --project=app
CREATE src/app/services/autenticacion.service.ts (149 bytes)
[OK] Generated service!
```

## Componentes
Agntes de crear los componentes vamos a crear un módulo (se requiere) para contenerlos.

```shell
$ ionic generate module components
> ng.cmd generate module components --project=app
CREATE src/app/components/components.module.ts (196 bytes)
[OK] Generated module!
```
Vamos ahora a crear los componentes que vamos a crear en la aplicación. Serían los siguientes

- components/dashboard/TareasPorEstado
- components/shared/FiltroBusqueda
- components/tasks/TablaTareas

Para crearlo podemos por ejemplo:

```shell
ionic generate component components/dashboard/TareasPorEstado --spec=false
> ng.cmd generate component components/dashboard/TareasPorEstado --spec=false --project=app
CREATE src/app/components/dashboard/tareas-por-estado/tareas-por-estado.component.html (36 bytes)
CREATE src/app/components/dashboard/tareas-por-estado/tareas-por-estado.component.ts (310 bytes)
CREATE src/app/components/dashboard/tareas-por-estado/tareas-por-estado.component.scss (0 bytes)
[OK] Generated component!
```

## Validaciones
Las validaciones van a venir en la forma de servicios. De modo que podemos generarlas del mismo modo. Tenemos que generar las siguientes validaciones:

- validators/shared/ValidacionService
- validators/tasks/ValidacionTareasService
- validators/tasks/ValidacionTituloService

Por ejemplo.
```shell
$ ionic generate service  validators/tasks/ValidacionTareasService --skip-tests
> ng.cmd generate service validators/tasks/ValidacionTareasService --skip-tests --project=app
CREATE src/app/validators/tasks/validacion-tareas-service.service.ts (152 bytes)
[OK] Generated service!
```

## Guards
Generamos los guards en nuestro proyecto

```shell
$ ng generate guard guards/autenticacion --skip-tests 
? Which interfaces would you like to implement? CanActivate, CanLoad
CREATE src/app/guards/autenticacion.guard.ts (660 bytes)
```

