# Componente header
Vamos a crear un componente para los encabezados de las páginas en ionic. De ese modo podemos insertar una cabecera para nuestras páginas sin necesitar hacerlo cada vez.

```
$ ionic generate component components/header --spec=false
CREATE src/app/components/header/header.component.html (25 bytes)
CREATE src/app/components/header/header.component.ts (268 bytes)
CREATE src/app/components/header/header.component.scss (0 bytes)
[OK] Generated component!
```

## html

```html
<ion-header>
  <ion-toolbar>    
    
    <ion-buttons slot="start" *ngIf="volver">
      <ion-back-button defaultHref="/listado-tareas"></ion-back-button>
    </ion-buttons>

    <ion-title>{{titulo}}</ion-title>
  </ion-toolbar>
</ion-header>
```


## ts

```ts
  @Input() titulo : string = '';
  @Input() volver : boolean = true;

  constructor() { }

  ngOnInit() {}

```
