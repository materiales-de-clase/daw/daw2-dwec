# Distribución de la aplicación
En este punto incluimos información para generar nuestro paquete y desplegarlo en alguna plataforma móvil.

## Generar
Utilizando el siguiente comando compilamos los archivos de la aplicación y los deja listos para su despliegue:

```
$ ionic build

> ng.cmd run app:build
√ Browser application bundle generation complete.
√ Copying assets complete.
- Generating index html...10 rules skipped due to selector errors:
  :host-context([dir=rtl]) .ion-float-start -> subselects_1.subselects[name] is not a function
  :host-context([dir=rtl]) .ion-float-end -> subselects_1.subselects[name] is not a function
  :host-context([dir=rtl]) .ion-float-sm-start -> subselects_1.subselects[name] is not a function
  :host-context([dir=rtl]) .ion-float-sm-end -> subselects_1.subselects[name] is not a function
  :host-context([dir=rtl]) .ion-float-md-start -> subselects_1.subselects[name] is not a function
  :host-context([dir=rtl]) .ion-float-md-end -> subselects_1.subselects[name] is not a function
  :host-context([dir=rtl]) .ion-float-lg-start -> subselects_1.subselects[name] is not a function
  :host-context([dir=rtl]) .ion-float-lg-end -> subselects_1.subselects[name] is not a function
  :host-context([dir=rtl]) .ion-float-xl-start -> subselects_1.subselects[name] is not a function
  :host-context([dir=rtl]) .ion-float-xl-end -> subselects_1.subselects[name] is not a function
√ Index html generation complete.

Initial Chunk Files                   | Names                                      |   Raw Size | Estimated Transfer Size
main.9cfa41eecb8fc810.js              | main                                       |  416.29 kB |               106.62 kB
polyfills.b4e8aa928519df4d.js         | polyfills                                  |   33.12 kB |                10.62 kB
styles.89903ec4bb3c75e6.css           | styles                                     |   27.31 kB |                 4.23 kB
runtime.28740f86f7c2d4dd.js           | runtime                                    |    4.63 kB |                 2.22 kB

                                      | Initial Total                              |  481.35 kB |               123.68 kB

Lazy Chunk Files                      | Names                                      |   Raw Size | Estimated Transfer Size
5642.2eaeeeb3054d639f.js              | pages-login-login-module                   |  471.83 kB |                75.86 kB
2698.091c6952271181ff.js              | swiper-bundle-28080340-js                  |   97.16 kB |                23.36 kB
polyfills-core-js.b3eb16f0f64e9b8c.js | polyfills-core-js                          |   91.91 kB |                27.83 kB
4711.1f76125e09cf5a42.js              | -                                          |   82.06 kB |                 8.52 kB
4959.668615ff29075fca.js              | -                                          |   67.91 kB |                11.66 kB
1650.0c5003aa78ea82a5.js              | -                                          |   48.23 kB |                 4.64 kB
3236.5f8bfdeb0f27198a.js              | -                                          |   43.25 kB |                 9.39 kB
1118.5fd09974e22cc27d.js              | -                                          |   34.18 kB |                 5.57 kB
9325.60914821389fb876.js              | -                                          |   28.24 kB |                 4.85 kB
2349.73ce2451665e3a3c.js              | -                                          |   27.71 kB |                 6.75 kB
5652.0f2126d0fe947c2a.js              | -                                          |   25.43 kB |                 4.90 kB
8628.371731bcf9cbb44a.js              | -                                          |   24.20 kB |                 3.80 kB
2175.80979c608bb7ceb8.js              | -                                          |   23.31 kB |                 4.58 kB
3804.c5e6750a99f82d2b.js              | -                                          |   22.18 kB |                 5.53 kB
1709.2f070e25dcd7d0d8.js              | -                                          |   22.16 kB |                 4.59 kB
5836.3389965c0f302d5c.js              | -                                          |   21.68 kB |                 3.41 kB
4651.949eb810a7c2a156.js              | -                                          |   21.46 kB |                 3.76 kB
3648.08b8e914f506c179.js              | -                                          |   21.43 kB |                 3.56 kB
8136.c0db0b30a5e70aa1.js              | -                                          |   20.42 kB |                 4.65 kB
438.3aa1b2330d912846.js               | -                                          |   20.32 kB |                 3.11 kB
4174.c592bcc9e026d64b.js              | -                                          |   19.09 kB |                 1.86 kB
common.0b24ed5b6646ffb0.js            | common                                     |   18.78 kB |                 5.44 kB
polyfills-dom.3a5e3168052f1fc5.js     | polyfills-dom                              |   18.05 kB |                 5.04 kB
2773.b41a63fba41f12fd.js              | -                                          |   16.23 kB |                 4.21 kB
1217.e200ce6fe9c4dd45.js              | -                                          |   15.85 kB |                 3.02 kB
5168.619eb51f0721da5f.js              | -                                          |   15.47 kB |                 2.92 kB
7544.32248891261635d4.js              | -                                          |   14.93 kB |                 2.72 kB
6120.7a6044cc3a0cc65a.js              | -                                          |   14.73 kB |                 3.04 kB
6560.774adc47192eb52f.js              | -                                          |   14.54 kB |                 2.52 kB
2073.42e0e2970d0f4873.js              | -                                          |   13.55 kB |                 2.32 kB
1435.51587804484583eb.js              | -                                          |   12.29 kB |                 4.08 kB
388.c599d8baa2552a5b.js               | -                                          |   11.58 kB |                 2.10 kB
9654.6c435ea99503952b.js              | -                                          |   11.40 kB |                 2.62 kB
5349.94ada57a493064a9.js              | -                                          |   11.35 kB |                 3.60 kB
8286.4183479af5b620c0.js              | pages-editar-tarea-editar-tarea-module     |   11.23 kB |                 2.97 kB
9824.72592d7279d164c7.js              | -                                          |   11.16 kB |                 1.38 kB
4330.26859df2548bcf7a.js              | -                                          |   11.15 kB |                 3.80 kB
4432.b6e5cbb00a985d0c.js              | -                                          |   10.62 kB |                 2.55 kB
9016.c9db6e7c0f38d6ae.js              | -                                          |    9.37 kB |                 2.00 kB
9434.280551d12f7c5a53.js              | -                                          |    8.57 kB |                 2.01 kB
9922.464ca5afd2c999ee.js              | -                                          |    8.44 kB |                 1.95 kB
9374.c906b1c0e09c2029.js              | pages-listado-tareas-listado-tareas-module |    7.57 kB |                 2.58 kB
2289.89804a52faba82eb.js              | -                                          |    7.55 kB |                 2.65 kB
7602.293484c48ca8561d.js              | -                                          |    7.02 kB |                 2.00 kB
9230.bed5e1e31874ed78.js              | -                                          |    6.53 kB |                 1.80 kB
9536.25397f8a125fe6ec.js              | -                                          |    6.37 kB |                 1.29 kB
5817.a096ab3ab0722d3e.js              | -                                          |    6.00 kB |                 1.42 kB
4908.25a86e38e4aa3bcd.js              | -                                          |    5.97 kB |                 1.50 kB
1186.1b5df5f98491d4f7.js              | -                                          |    5.44 kB |                 1.90 kB
657.5eb0b715428bef72.js               | -                                          |    5.06 kB |                 1.25 kB
3093.e29b050c503dd19c.js              | input-shims-ca667d14-js                    |    4.58 kB |                 1.79 kB
4753.4dcf7c28ed683770.js              | -                                          |    4.17 kB |              1009 bytes
1536.f69f1e868cacf9a9.js              | -                                          |    3.76 kB |                 1.27 kB
9958.0b0061aa7ab5be76.js              | -                                          |    3.05 kB |               932 bytes
8802.c5475f2b221bb1c0.js              | pages-login-login-module                   |    2.05 kB |               874 bytes
4749.236473cdee255102.js              | pages-dashboard-dashboard-module           |    1.99 kB |               875 bytes
9718.735f7870bf946271.js              | index-0bc00b33-js                          |    1.82 kB |               816 bytes
1033.f35decb9160b9181.js              | -                                          |    1.65 kB |               681 bytes
8566.c4085910f588deed.js              | pages-ver-tarea-ver-tarea-module           |    1.57 kB |               728 bytes
8939.e268846754d2f8fb.js              | -                                          |    1.50 kB |               514 bytes
4376.464c0cc84ecc987f.js              | -                                          | 1008 bytes |               542 bytes
5780.da9f91dbb2045c61.js              | status-tap-20472ffa-js                     |  529 bytes |               320 bytes

Build at: 2022-11-22T21:42:34.600Z - Hash: e08abcf54a19fbde - Time: 19629ms
```

Esto generará dejará la aplicación lista para despliegue en el directorio www dentro del directorio del proyecto. 

## Desplegar a una plataforma móvil
En este enlace dispones de algunas instrucciones que te ayudarán a desplegar la aplicación en Android o IOS.

- https://ionicframework.com/docs/angular/your-first-app/deploying-mobile