# Environment
Ahora configuramos el entorno del mismo modo que haríamos en Angular.

```ts
export const environment = {
  production: false,
  
  // URL Base de los servicios de taskman
  taskmanBaseUrl: 'http://localhost/daw/daw2-dwec-profesorado/taskman/01-taskman-php-ajax/', 

  // Tiempo en milisegundos que un usuario debe estar sin pulsar una tecla
  // para que se acepte la entrada para lanzar por ejemplo un desplegable
  userInputDebounceDelay: 500,

  // Activa el modo depuración. Desactiva la autenticación.
  debug: 1

};
```