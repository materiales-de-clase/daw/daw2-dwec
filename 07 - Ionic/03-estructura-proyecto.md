# Estructura de directorios
En este apartado vamos a pararnos a echar un vistazo a la estructura de directorios de nuestro proyecto recien creado.

## Ficheros en el raíz del proyecto

- tsconfig.json: Archivo de configuración de TypeScript. Indica cómo se van a hacer las traducciónes de TS a JS. Generalmente no vamos a necesitar hacer cambios.
- tsconfig.spec.json: Está relacionado con las pruebas unitarias o de integración para garantizar que el programa funciona.
- tsconfig.app.json: Extiende al archivo de configuración y es donde se introducen los parámetros relacionados con nuestra aplicación. Normalmente no vamos a necesitar hacer cambios en este fichero.
- Readme.md: Es un archivo con información y ayuda sobre el proyecto.
- capacitor.config.json: archivo de configuración de capacitor.
- ionic.config.json: archivo de configuración de ionic.
- package.json: Relacionado con los paquetes instalados. No deberíamos tocar este archivo. Para actualizarlo se utilizará el comando npm. 
- package-lock.json: Está también relacionado con los paquetes.
- karma.conf.js: Configuración de Karma
- **angular.json**: Configuraciones importantes para nuestra aplicación. Por ejemplo rutas para el favicon, los recursos estáticos, hojas de estilos, etc. Como veremos más adelante, en este archivo instalamos también scripts u hojas de estilos de terceros que queramos distribuir junto con nuestr aplicación.
- .gitignore: Viene ya configurado con archivos que no deberían subirse al repositorio. 
- .editorconfig: Configuración para el editor. Viene con una configuración recomendada. Por ejemplo, tabs de 2 espacios. 
- .browserslistrc: Permite adaptar la salida para que sea compatible con ciertos navegadores.
- .vscode: Este directorio contiene archivos de configuración para VS Code. Esto nos permite, entre otras cosas, lanzar depuración con Chrome de nuestra aplicación desde el propio editor.

## Contenido de la carpeta src
Dentro de esta carpeta tenemos lo siguiente:

- **app**: El raíz de nuestra aplicación. Trabajaremos siempre dentro de este directorio.
  - **app.component.\***: Hacen referencia al appcomponent. Este componente representa mi aplicación. Será el punto de entrada inicial a mi aplicación.
  - **.css**: incluye el css que aplicará solo a este componente.
  - **.html**: incluye el código html.
  - **.ts**: El código TypeScript de nuestro componente
  - .spec.ts: Están relacionados con las pruebas. No vamos a tocar esto. Lo podemos borrar.
  - **.module.ts**: información acerca del modulo/modulos que participan en la aplicación. Más adelante hablaremos sobre esto.
- **assets**: Contiene los recursos estáticos de nuestra página. Imágenes, sonidos, etc.
- **environments**: Lugar para poner variables de entorno. Por ejemplo rutas a recursos externos.
  - environment.prod.ts: Variables de entorno de producción
  - **environment.ts**: Variables de entorno de desarrollo. Este es el que tendremos que referenciar siempre desde nuestra aplicación. Al generar el archivo para producción angular lo cambiará por el de producción.
- theme: 
  - variables.scss: Variables de configuración del tema de ionic. IONIC trabaja con SASS de modo que este archivo utiliza su sintaxis. Aunque se podría tocar para personalizar el interfaz de usuario, no vamos a tocar este archivo y vamos a utilizar la configuración por defecto.
- global.scss: configuración global de scss. Aquí solo tendríamos que importar archivos css si fuera necesario. 
- index.html: Página principal de la aplicación. Contiene etiquetas que serán reemplazadas por código de la aplicación. Será nuestro punto de entrada.
- main.ts: Esto no se toca generalmente. Permite configuraciones relacionadas con la plataforma destino.
- polyfills.ts: No lo vamos a tocar. Permitiría por ejemplo correr aplicaciones en navegadores muy viejos.
- test.ts: configuración de nuestro entorno de pruebas. No lo vamos a tocar.
- zone-glags.ts: Ayuda a prevenir que Ángular detecte ciertos cambios. 

## Directorio e2e
Este directorio está dedicado a la parte de pruebas de integración. No vamos a trabajar con este directorio en estos momentos.
