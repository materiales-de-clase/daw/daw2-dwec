# Introducción
Conceptos

## ¿Qué es una aplicación nativa?
Una aplicación que corre en una plataforma determinada como puede ser Android, IOS, Windows, Linux, etc. Cada plataforma HW o SW será diferente por lo que si queremos desarrollar una aplicación que corra en todas las plataformas vamos a tener que tener en cuenta que necesitaremos:

- Mayor tiempo de desarrollo y pruebas ya que necesitaremos implementar ciertas cosas para que funcionen en todas las plataformas.
- En caso de utilizar lenguajes de programación diferentes o herramientas no compatibles tendríamos que tener proyectos para cada plataforma.
- Contratar desarrolladores especializados en cada una de las plataformas con lo que dispararemos los tiempos de desarrollo.

## ¿Qué es una aplicación híbrida?
Son aplicaciones que se basan en el componente WebView. Este componente permite en esencia mostrar una página web. En todos los sistemas operativos existe este componente de forma nativa de modo que, aprovechando esto, podemos desarrollar aplicaciones que corran dentro de él de modo que:

- Nuestra aplicación ahora es código HTML, CSS y JavaScript
- Ya no requerimos proyectos para cada plataforma
- No requerimos desarrolladores especializados para cada plataforma sino que podemos recurrir a los desarrolladores web que ya tenemos
- La comunidad de desarrolladores web es mucho más grande que la de desarrolladores de aplicaciones nativas
- Desarrollamos una sola vez y podemos ejecutar nuestra aplicación en todas las plataformas
- Existen plugins para permitir a nuestras aplicaciones acceder a características nativas como la cámara o el giroscopio.

Nuestra aplicación híbrida va a ser una aplicación nativa que se puede descargar e instalar del mismo modo que cualquier otra aplicación nativa para las diferentes plataformas de modo que en la mayor parte de los casos no se podrá distinguir de una aplicación nativa a pesar de que la aplicación será html/css/js.

Podemos tener algunos inconvenientes o contextos en que no se debería optar por una aplicación híbrida

- Consumen más recursos que una aplicación nativa, lo que implicaría un mayor consumo de batería. Si este fuera un factor limitante no se debería aportar por este tipo de aplicación.
- Para juegos 2D/3D o aplicaciones que requieran mucho rendimiento la implementación híbrida no sería la mejor opción.
- En una aplicación nativa se pueden utilizar siempre las últimas características disponibles y está todo disponible. En una híbrida dependemos de que se implementen los plugins necesarios y no tenemos acceso a todo.
- Tenemos una gran dependencia en herramientas de terceros. 

## ¿Qué es ionic?
Es un framework open source que permite crear aplicaciones híbridas que van a verse como aplicaciones nativas. Dispone de:

- Conjunto de herramientas que facilitan el desarrollo y pruebas
- Puedes hacer que un desarrollo se ejecute en muchas plataformas sin modificar el código
- Las aplicaciones se ven como las aplicaciones nativas de cada plataforma
- Se integra con otras bibliotecas o frameworks como Angular, React o Vue aunque también puede ser utilizado sin un framework de frontend recurriendo directamente a JavaScript e incluyendo un script en nuestra página. 

## Documentos relacionados
- [Comparativa aplicaciones híbridas vs nativas](recursos/Ebook_Hybrid_vs_Native.pdf)