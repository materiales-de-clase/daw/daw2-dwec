# Tarea 1 - Calculadora - DOM
Los fuentes de este ejercicio deben ponerse en el directorio dom/tareas/calculadora. Antes de comenzar lee el enunciado completamente.

En esta práctica, partiendo de la calculadora diseñada en DIW y la clase Calculadora que habremos creado anteriormente, vamos a hacer una calculadora totalmente funcional. La calculadora tendrá el siguiente aspecto.

![](recursos/calculadora.png)

Tendremos que diseñar el interfaz web para que utiliza una instancia de nuestra clase Calculadora (implemetada con class) que permita implementar todas las operaciones que aparecen en la captura de pantalla. Además, nuestra clase calculadora tendrá que implementar el evento:

- onPantallaActualizada(nuevo valor)

que se va a lanzar cuando el valor de la pantalla cambie. Hablamos de la pantalla como el buffer o la variable acumuladora que es un atributo en la clase calculadora que contiene el valor actual de lo que sería la pantalla. Dicho evento podrá ser asignado con el método setPantallaActualidadListener a una función que será invocada siempre que el valor de pantalla cambie, pasándole como argumento el nuevo valor que hay en la pantalla. De este modo se notificará a un interfaz de usuario cuando se ha producido un cambio en la pantalla que deba actualizarse.

La forma de funcionamiento será la siguiente:

- Se captura un evento de click o teclado.
- Se va actualizando la pantalla de nuestra calculadora gráfica.
- Si se pulsa sobre el botón de realizar alguna operación, se invoca al método correspondiente de la clase calculadora. Ejemplo sumar(XXX).
- Si supone actualización de la pantalla (en caso contrario no) se lanzará el evento para actualizar la pantalla. Lo que hay que hacer, es llamar a la función que nos han pasado previamente como argumento a onPantallaActualizada para que escriba el nuevo valor.
- En caso de error se actualizará la pantalla al valor ERROR (hablando de la clase) y se lanzará un evento para que el interfaz de usuario lo muestre. Para comprobar si hay errores, captura excepciones.

Respecto a la forma de utilización, debe ser similar a la de una calculadora normal. Puedes partir de la calculadora de windows si no tienes claro como funciona una calculadora.

## Entrega
La entrega debes hacerla a través de un archivo comprimido y del repositorio. El archivo comprimido se llamará apellidos_nombre_calculadora. Deberá contener los siguientes archivos (o los que corresponda dependiendo del tipo de entrega):

- calculadora.html: la página. De este documento solo va a ser importante lo que esté relacionado con JavaScript y cómo se identifican los botones para asignar los eventos.
- calculadora.mjs: El código JavaScript asociado a la página web. 
- calculadora-clase.mjs: Declaración de la clase calculadora. 

Además, vamos a tener las siguientes opciones de entrega. Deberás optar por una de ella, teniendo cada una un tope de puntuación :
### Sin clase Calculadora y sin módulos. 6 puntos max.
En esta entrega no existirá el fichero calculadora-clase ya que no hay implementación de la clase y el archivo principal será calculadora.js. Se trata de hacer una página web que se comporte como una calculadora. Podemos tener:
- Un gestor de eventos por botón: max 5 puntos
- Un gestor de eventos para todos los botones: max 6 puntos.

### Con clase Calculadora pero sin módulos. 8 puntos max.
En este caso, tenemos que incluir la clase calculadora en un archivo calculadora-clase.js.

Podemos tener:
- Un gestor de eventos por botón: max 6 puntos
- Un gestor de eventos para todos los botones: max 7 puntos.
- La calculadora lanza el evento para actualizar la pantalla: max 8 puntos.
 
### Diseño modular con clase Calculadora. 10 puntos max.
Todos los archivos tendrán extensión .mjs. En este caso se puede hacer:

- Un gestor de eventos por botón: max 7 puntos
- Un gestor de eventos para todos los botones: max 8 puntos.
- La calculadora lanza el evento para actualizar la pantalla: max 9 puntos.
- Utilizas un array asociativo para invocar a la función que ejecuta la operación: max 10 puntos

### A tener en cuenta para la entrega

- Recuerda el modo estricto
- Recuerda la estructura de los ficheros
- Recuerda incluir los scripts correctamente
- Utiliza comentarios para documentar tu ejercicio
- Deberás utilizar las sentencias, expresiones y estructuras estudiadas en clase. 
- Crea funciones donde sea necesario

## Recursos
Se incluye, por si no quieres perder tiempo maquetando la calculadora, un diseño web de la misma en los siguientes enlaces:

- [Calculadora HTML](recursos-tareas/calculadora/calculadora.html).
- [Calculadora CSS](recursos-tareas/calculadora/calculadora.css).

