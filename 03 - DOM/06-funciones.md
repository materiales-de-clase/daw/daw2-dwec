# Funciones
En este apartado vamos a ver algunas funciones interesantes que tenéis disponibles para vuestras aplicaciones

## eval
La función eval() evalúa una cadena de caracteres que representa código JavaScript. 

```
    // Muestra un alert con el mensaje "Hola Mundo"
    eval("alert('hola mundo')");
    
```

## setTimeout/clearTimeout
Dispones de la documentación en 

https://devdocs.io/dom/settimeout

Permite programar la ejecución de una función en el futuro. Disponeis de un ejemplo hecho con esta función aquí:

[temporizador HTML](ejemplos/temporizador.html)
[temporizador js](ejemplos/temporizador.js)