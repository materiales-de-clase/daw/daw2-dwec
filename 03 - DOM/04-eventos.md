# Eventos
Una vez que nuestros scripts se han ejecutado, el código JavaScript se invocará en rspuesta a acciones llevadas a cabo por el usuario. Estas acciones desencadenan eventos. Podemos registrar funciones para que sean invocadas cuando se desencadena uno de estos eventos.

# Asignación de un gestor de eventos
Existen dos formas en que podemos asignar un gestor de eventos. En este ejemplo se usan las dos formas, que van a ser equivalentes:

```html
    <!DOCTYPE html>
    <html lang=es>
        <head>
            <!-- La codificación. Considéralo una buena práctica -->
            <meta charset=utf-8>

            <!-- Información adicional para buscadores y navegadores -->
            <meta name="author" content="Nombre del autor">
            <meta name="description" content="Estructura de un documento HTML5">

            <!-- Título de la página -->
            <title>Estructura básica de un documento HTML</title>        
        </head>
        <body>
            <!-- Aquí iría la descripción del documento -->
            <button id="bSaludar1" onclick="saluda();">Mi botón 1</button>
            <button id="bSaludar2">Mi botón 2</button>

            <!-- Recuerda. Se tiene que poner al final. No hay src. Se ejecuta cuando se encuentra -->
            <script>
                function saluda() {
                    window.alert("Hola mundo");
                }
            
                // Asigna el gestor de evento de forma programática
                let bSaludar = document.getElementById('bSaludar2');   
                bSaludar.addEventListener("click", saluda);        
            </script>

        </body>
    </html>
```

Aunque se pueden utilizar las dos formas, la recomendable sería utilizar addEventListener. De ese modo, se evita mezclar código HTML y JavaScript. En clase **siempre vamos a utilizar addEventListener**.

# El objeto event
Nuestos gestores de eventos son funciones. Un ejemplo de función gestora de un evento sería:

```javascript
    function miGestorEvento(evento) {
        // Llevo a cabo las tareas.
    }
```

Los gestores de eventos reciben un objeto que describe el evento que hemos recibido. Existen diferentes objetos para representar diferentes eventos. Así, cuando estemos recibiendo un evento de teclado, recibiremos un **KeyboardEvent**. Si, por el contrario, recibimos un evento de ratón, lo que tendremos como parámetro será un **MouseEvent**. La lista de tipos de eventos que vamos a poder gestionar la tenemos en [este enlace](https://www.w3schools.com/Jsref/obj_events.asp).

Todos los eventos heredan del objeto **Event** y añaden algunas propiedades relacionadas con el evento que están tratando. A continuación veremos como gestionar algunos eventos y accederemos a algunas de sus propiedades.


# Eventos disponibles
Son muchos los eventos disponibles que modemos gestionar. En la [referencia del w3c](https://www.w3schools.com/Jsref/dom_obj_event.asp) tienes una tabla con todos los eventos estándar de JavaScript.

De los eventos listados, vamos a ver ejemplos de algunos de ellos que nos van a ser de utilidad.

## load      
El evento *load* nos permite recibir un evento cuando un recurso y recursos dependientes han terminado de cargar. Lo podemos emplear para:

- Controlar la carga de toda la página, asignando el gestor a *window.onload* o *body.onload*.Debemos tener en cuenta que *window.onload* tiene precedencia y si lo asignamos no se invocará a body.onload*.
- Controlar la carga de cualquier elemento de la página.

En el siguiente ejemplo tenemos varias formas de inicialización del evento onload. 

[Ejemplo onload HTML](ejemplos/onload.html)
[Ejemplo onload js](ejemplos/onload.js)

## keypress
Otra posibilidad que tenemos en nuestros scripts, es controlar la entrada por teclado. Esto se puede hacer gestionando eventos de teclado. Con este fin tenemos los eventos [keydown](https://www.w3schools.com/Jsref/event_onkeydown.asp), [keyup](https://www.w3schools.com/Jsref/event_onkeypress.asp) y [keypress](https://www.w3schools.com/Jsref/dom_obj_event.asp). En este caso, vamos a centrarnos en keydown. Este evento toma lugar cuando el usuario está pulsando una tecla. El orden en que se invocarían estos tres eventos sería:

1. onkeydown
2. onkeypress
3. onkeyup

Tomamos este evento por recibir todas las teclas frente a keypress que no va a recibir las teclas especiales.

En el siguiente ejemplo se copian de forma automática caracteres de un campo a otro a medida que se pulsan las teclas.

[Ejemplo keydown HTML](ejemplos/onkeydown.html)
[Ejemplo keydown js](ejemplos/onkeydown.js)

## submit
Este evento ocurre cuando se envía un formulario. Permite llevar a cabo acciones y permitir o no el envío de los datos.

En el siguiente ejemplo se hace una validación de DNI. Si dicha validación falla no se permite enviar el formulario y se cambia el color de fondo a rojo para que el usuario detecte el error.

[Ejemplo submit HTML](ejemplos/onsubmit.html)
[Ejemplo submit js](ejemplos/onsubmit.js)

## change
El evento change ocurre cuando el contenido de un elemento de un formulario cambia. Esto afecta a campos input, select o textarea. En el siguiente ejemplo tenemos un uso sencillo de dicho evento.

[Ejemplo onchange HTML](ejemplos/onchange.html)
[Ejemplo onchange js](ejemplos/onchange.js)

## mousemove
Evento que se produce cuando el ratón se está moviendo sobre un elemento.

[Ejemplo onmousemove HTML](ejemplos/onmousemove.html)
[Ejemplo onmousemove js](ejemplos/onmousemove.js)
           
## mouseover           
Evento que se produce cuando el ratón entra en un elemento.

[Ejemplo onmouseover HTML](ejemplos/onmouseover.html)
[Ejemplo onmouseover js](ejemplos/onmouseover.js)

## click
Evento que se produce cuando hacemos clic en un elemento.

[Ejemplo onclick HTML](ejemplos/onclick.html)
[Ejemplo onclick js](ejemplos/onclick.js)

# Cancelación de eventos
Algunos eventos son cancelables. Esto implica que se pueden cancelar y hacer como si nunca hubieran ocurrido. Para cancelar un evento vamos a utilizar la llamada **preventDefault()**. Este método evitará que se ejecute la acción por defecto del evento.

```javascript
    // Si esta función se llama en el evento submit de un formulario, 
    // evitará que el formulario se envíe.
    function onSubmitForm(e) {
        e.preventDefault();
    }
```

# Bubbling 
El bubbling es el hecho de tener eventos que traspasan el elemento primero y se propagan a sus elementos que los incluyan. Poner ejemplo con console.log un div y dos botones con sus respectivos eventos.

          e.stopPropagation() -> Detiene la propagación del evento.

[Ejemplo bubbling HTML](ejemplos/bubbling.html)          
[Ejemplo bubbling js](ejemplos/bubbling.js)


# Ejercicios
Crea en tu directorio de ejercicios el directorio dom/ejercicios/eventos. Ahora, utiliza el nombre del ejercicio para la página del ejercicio y el script js asociado y otros recursos.

## ejercicio eventos01 (10 min)
Partiendo de alguno de los ejemplos de este bloque, crea ahora un ejemplo para el evento mouseout.

## ejercicio eventos02 (10 min)
Partiendo del ejemplo para el evento click, agrega los eventos mouseup y mousedown. Debes registrar un mensaje en consola para los eventos mousedown, click y mouseup. Observa el orden en que son invocados.

## ejercicio eventos03 (10 min)
Partiendo del ejemplo del onload, implementa los eventos pagehide y pageshow. Registra mensajes en cada uno de ellos por la consola. Observa cuando se producen los eventos.

## ejercicio eventos04 (10 min)
Partiendo del ejemplo con keypress, crea ahora un ejemplo que cature keydown, keypress y keyup. Deberá registrar en el log el evento y la tecla pulsada. Observa el orden en que son invocados.


