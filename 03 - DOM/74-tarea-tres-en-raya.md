# Tarea 4 - TresEnRaya - jQuery
Directorio dom/tareas/tresenraya

Vamos a hacer un tres en raya sencillo. Para hacer esta tarea crea un tablero. Utiliza X y O para marcar las jugadas. No vamos a tener IA. Va a ser un juego de dos jugadores. Tendrá que cumplir:

- Tendrá un marcador de partidas ganadas por X y O. Al cargar la página estará a 0.
- Siempre empiezan jugando las X.
- Los jugadores van alternando turno.
- Cuando tenga 3 en línea se gana la partida. Se muestra un mensaje y se actualiza el marcador. Tras esto, se reinicia la partida a tablero vacío.
- Si no eres capaz de implementar una función que verifique si hay tres en raya puedes utilizar una de un compañero (indicándolo) o considerar que la partida termina cuando el tablero esté lleno. En este caso, el ganador lo calcularás utilizando la función rand().

## Rúbrica de corrección
|Puntuación|Descripción|
|--|--|
|5|El programa funciona de acuerdo a las especificaciones|
|2|Utilización adecuada y óptima de las funciones y eventos siguiendo las directrices marcadas en clase|
|1|Programación modular y código reutilizable|
|1|Sigue un planteamiento original|
|1|Limpieza y documentación|

