## Ejercicios de DOM
Crea en el repositorio el directorio dom/ejercicios/ejercicios-bloque1 y, dentro de ese directorio, crea los archivos con el nombre del ejercicio.

## dom0101 Colores
Crea una web que tenga los siguientes elementos:

- botón rojo
- botón azul
- botón verde
- un campo de texto

Cuando se pulse sobre cualquiera de esos botones. Haz que el campo se ponga del mismo color que el botón. Para hacerlo debes implementar un gestor de evento independiente por cada botón.

## dom0101 Colores II
Crea una web que tenga los siguientes elementos:

- botón rojo
- botón azul
- botón verde
- un campo de texto

Cuando se pulse sobre cualquiera de esos botones. Haz que el campo se ponga del mismo color que el botón. Para hacerlo debes implementar un gestor de evento para todos los botones. Utiliza un switch para determinar el botón que ha sido y cambia el color en consecuencia.

## dom0102 Colores III
Crea una web que tenga los siguientes elementos:

- botón rojo
- botón azul
- botón verde
- un campo de texto

Cuando se pulse sobre cualquiera de esos botones. Haz que el campo se ponga del mismo color que el botón. Para hacerlo debes implementar un gestor de evento para todos los botones. Copia el color del botón al campo.

## dom0103 Copia texto
Crea una página web con varios botones. Cada botón tendrá un texto diferente. Al pulsar sobre cada uno de los botones, deberás concatenar el texto en el botón a un párrafo de texto que también habrá en la página.

## dom0104 Cambia texto
Crea una página que contenga los siguientes tipos de elementos:

- Un párrafo con un texto
- Un input de tipo number. Este debe inicializarse cuando cargue la página al tamaño del texto en puntos.
- Un combo con varios tipos de letra.

Cambiar el tamaño de letra afectará al tamaño de letra del texto en el párrafo. Escoger o cambiar el tipo de letra afectará al tipo de letra en el párrafo.

## dom0105 
Utilizando las funciones que se explican en esta página:

https://www.w3schools.com/jsref/met_table_insertrow.asp

Crea una página que contenga una tabla de frutas que contenga:

- Fruta
- Color

Y varias filas. Puedes insertar las frutas que quieras. 

Ahora crea un par de inputs para introducir el nombre y el color un un botón para añadirlo a la tabla. Cuando se pulse el botón tendrás que añadir una fila a la tabla con la nueva fruta.

## dom0106
Crea una página que contenga una tabla de frutas que contenga:

- Fruta
- Color

Y varias filas. Puedes insertar las frutas que quieras. 

Ahora crea un par de inputs para introducir el nombre y el color un un botón para añadirlo a la tabla. Cuando se pulse el botón tendrás que añadir una fila a la tabla con la nueva fruta. Para añadirla, trabaja con la propiedad innerHTML para añadir el código html que corresponda con la fila.

## dom0107
Partiendo del ejercicio anterior. Crea ahora un botón en cada fila que permita eliminarla. Tendrás que añadir una columna para introducir ese botón. Utiliza la función descrita en este enlace para eliminar la fila.

https://www.w3schools.com/jsref/met_table_deleterow.asp

## dom0108
Crea una página con:

- Campo de entrada
- Botón
- Párrafo

Cuando se pulse el botón, concatena el texto en el campo de entrada al que haya en el párrafo.

## dom0109
Partiendo del ejemplo [posiciona.html](ejemplos/posiciona.html), crea una página que contenga:

- Un div que va a ser la zona de "juego".
- Un elemento como en el ejemplo de otro color. Puedes utilizar el que te resulte más cómodo.
- Alguna zona en la página que muestre las coordenadas del ratón (no valen inputs).

Ahora la página debe hacer lo siguiente:

- Al mover el ratón, debe mostrar en todo momento la posición del mismo.
- El elemento de otro color, siempre que el ratón esté dentro del área de juego, debe estar bajo el puntero del ratón.
- Cuando se haga click sobre el elemento de otro color, debe cambiar a otro color aleatorio.

## dom0110
Crea un formulario que tenga un campo nombre y un campo numérico. Debe controlar:

- El campo nombre no puede estar vacío. Con espacios cuenta como vacío.
- El campo numérico tiene que contener un número par.

Al abandonar cualquiera de estos campos, si no se cumplen las restricciones indicadas, el fondo del campo con error será rojo. En caso de corregirlo se volverá a poner el color por defecto.

## dom0111
Crea un formulario que tenga un campo nombre y un campo numérico. Debe controlar:

- El campo nombre no puede estar vacío. Con espacios cuenta como vacío.
- El campo numérico tiene que contener un número par.

Cuando se vaya a enviar el formulario se deben hacer las comprobaciones. Los campos incorrectos se pondrán en rojo. Si hay errores no se enviará el formulario.
