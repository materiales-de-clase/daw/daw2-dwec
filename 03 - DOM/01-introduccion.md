# Introducción
En esta unidad vamos a poner el foco en la programación de JavaScript en el navegador. Cuando un programa se ejecuta en este entorno de ejecución, tenemos acceso a diversos objetos que no tendremos disponibles en otros. 

En el siguiente gráfico se muestra la jerarquía de objetos/interfaces que el navegador va a exponer a las páginas web: 

![DOM y BOM](recursos/DOMBOM.svg)

En la figura podemos distinguir entre dos ramas o tipos de objetos:

- Aquellos que son utilizados para describir el documento cargado en ese momento
- Los que permiten acceso a el entorno de ejecución, que será el navegador en el caso que nos ocupa.

# El Browser Object Model 
El BOM hace referencia a los objetos/interfaces que están expuestos para que nuestra página pueda acceder a las funciones del navegador. Entre algunos de los objetos tendremos:

- window: el raíz de la jerarquía
- history: para acceder a la historia del navegador
- location: permite acceder a la barra de direcciones

Estos objetos permiten, como se indica, acceder a funcionalidades que no tienen tanto que ver con el documento en sí, sino con la plataforma donde el script se ejecuta. 

```javascript
    // Carga un nuevo documento en el navegador
    window.location.assign("https://www.w3schools.com")
```

# El Document Object Model
El DOM hace referencia a el modelo de objetos del documento. Digamos que cuando nuestro navegador carga un documento, genera una jerarquía de objetos que representan el documento. Estos objetos son expuestos al código JavaScript que se ejecuta en ese documento, de modo que, puede utilizarlo para obtener información acerca del documento o modificarlo según sea necesario. 

En la siguiente imagen tenemos el dom correspondiente a un documento ejemplo. Como podemos ver, el DOM tendrá una estructura arborea del mismo modo que el HTML.

![DOM](recursos/DOM.png)



