# Cuadro resumen
A continuación tenemos un listado con las prinpales funciones y propiedades vistas a lo largo del tema.

## Carga de scripts

```html
<script>
    // El código en este bloque se ejecuta en el momento en que 
    // el navegador lo encuentra        
</script>

<script src="ejemplo.js">
    // Este es equivalente al anterior
</script>

<script src="ejemplo.js" async>
    // Se descarga en segundo plano
    // Se ejecuta cuando termine la carga
</script>

<script src="ejemplo.js" defer>
    // Se descarga en segundo plano
    // Se ejecuta al finalizar el procesado del html
</script>

<script type="module" src="ejemplo.js">
    // Cuando el script es un módulo se carga en modo defer de 
    // forma predeterminada
</script>
```

## Interfaces importantes

|Objeto|Descripción|
|--|--|
|window|Objeto global. Da acceso a los interfaces del DOM y el BOM|
|document|Interfaz que permite llevar a cabo operaciones con el documento|

## Funciones para buscar nodos

|Función|Tipo|Retorna|
|--|--|--|
|getElementById|Element|Elemento con el identificador pasado como argumento
|getElementsByTagName|NodeList|Retorna todos los elementos con el nombre de etiqueta pasado como argumento.
|getElementsByName|NodeList|Retorna todos los elementos cuyo nombre coincide con el que se ha pasad como argumento.
|getElementsByClassName|HtmlCollection|Retorna todos los elementos cuya clase css coincide con la clase o clases pasadas como argumento.
|querySelector|Element|Retorna el primer elemento que coincide con el selector
|querySelectorAll|NodeList|Retorna una lista con elos elementos que coinciden con el patrón

## Atributos interesantes de los nodos

|Atributo|Descripción|
|--|--|
|className|Lista de clases css a aplicar a un elemento. Valor del atributo class en el documento|
|innerText|Texto renderizado dentro del un elemento y sus descencientes|
|textContent|Texto dentro de un elemento y sus descendientes|
|innerHTML|Contenido HTML de un elemento|

## Gestión de eventos

#### Definición
Podemos ver un evento como algo que ha ocurrido, generalmente por acción del usuario o del propio navegador. Ejemplos podrían ser que el usuario haga click o pulse una tecla, que la página termine de cargar o se oculte, etc. 

#### Lista de eventos
Se pueden gestionar multitud de eventos. En clase nos vamos a centrar en algunos de los básicos, ya que el tratamiento va a ser similar. A continuación algunos de los más importantes que vamos a utilizar.

|Evento|Cuándo|
|--|--|
|load|La página o un recurso determinado ha terminado de cargar|
|click|El usuario hace click en un elemento|
|dblclick|El usuario hace doble click en un elemento|
|mousedown|Se pulsa un botón del ratón sobre un elemento|
|mouseup|Se suelta un botón del ratón sobre un elemento|
|keydown|Cuando una tecla es pulsada|
|keyup|Cuando una tecla es soltada|
|submit|Tiene lugar cuando se envía un formulario|
|focusin|Se lanza cuando el foco entra en un elemento|
|change|Se lanza al salir de un campo si el contenido ha cambiado|
|focusout|Se lanza cuando el foco sale de un elemento|
|blur|Se lanza cuando el foco sale de un elemento|


#### Asignar un gestor de eventos
```html
    <!-- Se puede asignar un gestor de eventos utilizando atributos
      en el propio código html -->
    <input id="entrada" onchange="mifuncion()"/>
```

```javascript
    // Podemos asignar una función directamente al elemento 
    // utilizando código JavaScript en cualquier parte del documento
    document.getElementById("entrada").onchange=mifuncion;
```

```javascript
    // Nuetra forma preferida va a ser la función addEventListener
    // Permite asignar un gestor de evento en cualquier momento
    // utilizando código JavaScript
    document.getElementById("entrada").addEventlistener("change", mifuncion);
```

```javascript
    // También podemos pasar directamente el código JavaScript
    // utilizando una función flecha por ejemplo
    document.getElementById("entrada").addEventlistener("change",   
        (evento) => {

        }
    );
```

### Implementar un gestor de evento
Un gestor de eventos es una función. Esta función puede tomar como argumento el objeto que describe lo que ha pasado. Esto permite obtener información precisa sobre el evento ocurrido. Por ejemplo, obtener las coordenadas del ratón en caso de click.

```javascript
    // Función que puede ser pasada como argumento
    function onEntradaClick(evento) {
        // Código aquí
        // evento es una referencia a una subclase de Event
        // La clase precisa depende del evento de que se trate
    }
```

#### Funciones importantes en el evento
Estas funciones permiten controlar algunos aspectos importantes relacionados con el evento.

|Función|Descripción|
|--|--|
|preventDefault()|En un evento cancelable, cancela la acción por defecto|
|stopPropagation()|En un evento con propagación hacia el padre, detiene la propagación del evento.


## Otras funciones interesantes

|Función|Descripción|
|eval()|Permite evaluar código JavaScript pasado como argumento|
|setTimeout()|Nos permite planificar una llamada a una función pasada como argumento en el futuro|
|clearTimeout()|Permite cancelar un timeout configurado previamente|


