# jQuery
jQuery es una biblioteca JavaScript enfocada a hacernos a vida más fácil cuando tenemos que llevar a cabo determinados tipos de desarrollos. Aunque estudiar la biblioteca de forma extensiva escapa al alcance de esta unidad de trabajo, vamos a hacer una pequeña introducción a la biblioteca, de modo que podamos utilizarla al menos para las tareas  que vayamos estudiando. Esto nos permitirá introducirla en nuestros proyectos.

Aquí tenéis algunos enlaces de interés:

- https://jquery.com/ La página principal de jquery
- https://api.jquery.com/ Documentación del API. Es una referencia que merece la pena que por lo menos conozcáis. Ahí tenéis todas las funciones implementadas en jQuery

# Incluir jQuery en nuestra página
Para incluir jQuery en nuestra página podemos utilizar uno de los enlaces disponibles en el enlace https://releases.jquery.com/. De cada una de las versiones dispondremos de varias opciones. Las opciones disponibles son:

- **uncompressed**: esta versión es ideal para las fases de desarrollo o depuración. 
- **minified**: versión pensada para producción. Esta versión permite reducir los tiempos de carga y el ancho de banda requerido, el uso de batería, etc. A nivel funcionalidad, no hay diferencias con la versión sin comprimir.
- **slim**: Esta es una versión de jQuery sin comprimir que no incluye los módulos de ajax y efectos.
- **slim minified**: La versión slim comprimida.

En clase vamos a utilizar la versión **uncompressed**.

Para incluir jQuery en nuestra página web, utilizaremos un código como el siguiente (obtenido de la página https://releases.jquery.com/).

```javascript
    <script src="https://code.jquery.com/jquery-3.6.1.js"
        integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI="
        crossorigin="anonymous">
    </script>
```

# La función $
La función jQuery, también llamada $(), podríamos decir que es el core del flujo de trabajo con esta biblioteca. 

```javascript
    // Se puede invocar a la función jQuery utilizando el nombre
    // largo o el corto. Las siguientes llamadas son equivalentes
    bAceptar = jQuery("#bAceptar");
    bAceptar = $("#bAceptar");
```

Entre sus funciones se encuentran:

- **Selección de nodos**. Permite seleccionar nodos y obtener una lista de elementos que cumplen el criterio. Sobre estos elementos se pueden aplicar diversas funciones.
- **Crear elementos**. Permite crear elementos o grupos de elementos en la página web sobre la marcha.
- **Eliminar elementos**. Del mismo modo que se pueden crear, se pueden eliminar elementos de la página.
- **Manipular propiedades**

# Selección de nodos
Para seleccionar nodos, pasaremos a la función $() un selector o lista de selectores. Tenemos una lista de selectores en la página https://www.w3schools.com/jquery/jquery_ref_selectors.asp. 

```javascript
    // Ejemplos de selectores
    $('.clase'); // Selecciona los elementos con esa clase css
    $('p');      // Todos los párrafos <p>
```

# Crear elementos
Para insertar elementos disponemos de las funciones:

- [append](https://api.jquery.com/append/)
- [prepend](https://api.jquery.com/prepend/)
- [after](https://api.jquery.com/after/)
- [before](https://api.jquery.com/before/)

Estas funciones permiten añadir contenido (que puede ser html) en una determinada posición respecto de los elementos seleccionados.

# Eliminar elementos
Con la siguiente función se pueden eliminar elementos seleccionados.

```javascript
    // El segundo selector es opcional y permite filtrar los resultados devueltos por jQuery
    $(selector).remove(selector);

    // Elimina los elementosr con clase resaltado del elemento importante
    $('#importante').remove('.resaltado');
```
# Manipulación de propiedades
Vamos a ver algunos ejemplos de manipulación de propiedades.
## Obtener/asignar el valor
```javascript
    // Retorna el valor del primer elemento seleccionado
    $("input").val();

    // Asigna el valor a todos los elementos seleccionados
    $("input[type=text]").val('0');
```    
## HTML
La función html permite obtener y asignar el contenido html de un elemento.

```javascript
    // Obtiene el HTML del elemento identificado como raiz
    let html = $( "#raiz" ).html();

    // Asigna contenido html
    $( "#raiz" ).html('<p>Prueba</p>');
```
## TEXT
La función text permite obtener y asignar el contenido de texto de un elemento.
```javascript
    // Asigno al párrafo un texto
    $('#parrafo').text('texto');
```
## CSS
Utilizando la función css se pueden modificar las propiedades css de los elementos seleccionados.
```javascript
    $('div').css("font-size", "32pt"); 
```
## load
Carga una url pasada como argumento desde el servidor y coloca el html resultante dentro de los elementos seleccionados.

```javascript
    $('#mielemento').load('diag/mensaje.html');
```

# Asignación de eventos
Permite asignar y eliminar gestores de eventos utilizando las funciones:

- [ready](https://api.jquery.com/ready/)
- [on](https://api.jquery.com/on/)
- [off](https://api.jquery.com/off/)

```javascript
    // Equivalente al onload de window
    $(document).ready(function(){
   
        // De este modo se puede asignar un gestor de eventos
        $("#elementocontrolado").on("dblclick change",function(){
            alert('Función on() activada.');
        });
    });

    // Con este código puedes eliminar el gestor de eventos
    $( "#elementocontrolado" ).off( "dblclick")
```    
