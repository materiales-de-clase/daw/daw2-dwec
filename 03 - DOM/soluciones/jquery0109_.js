"use strict";


//--------------------------------------------------------------------------
// Programa principal
//--------------------------------------------------------------------------
$(document).ready(() => {
    // Importante. Debe invocarse en el onloads
    console.log("Secuencia de inicialización iniciada");

    // Asigna el gestor de evento de nuestro teclado
    $("#zona").on('mousemove', (evento) => {
        
        // Obtengo la posición del ratón
        const x = evento.offsetX;
        const y = evento.offsetY;
    
        mostrarPosicion(x, y);
        moverCosa(x, y);
    });

    $("#cosa").on('mousedown', (evento) => {
        $("#cosa").css('background-color', 'blue');
    });

    $("#cosa").on('mouseup', (evento) => {
        $("#cosa").css('background-color', 'red');
    });

});



//--------------------------------------------------------------------------
// Funciones 
//--------------------------------------------------------------------------
/**
 * Muestra la posición en pantalla del ratón
 * 
 * @param {*} x 
 * @param {*} y 
 */
function mostrarPosicion(x, y) {
    $('#posx').text(x);
    $('#posy').text(y);
}

/**
 * Mueve la cosa a la posición indicada
 * 
 * @param {*} x 
 * @param {*} y 
 */
function moverCosa(x, y) {

    // Asigna la nueva posición
    $("#cosa").css('left', x-10+"px");
    $("#cosa").css('top', y-10+"px");
}


