"use strict";

//--------------------------------------------------------------------------
// Programa principal
//--------------------------------------------------------------------------
$(document).ready(() => {

    // Importante. Debe invocarse en el onloads
    console.log("Secuencia de inicialización iniciada");

    $("#cosa").on("mousedown", (evento) => {
        $(evento.target).css('background-color', 'blue');
    });
    
    $("#cosa").on("mouseup", (evento) => {
        $(evento.target).css('background-color', 'red');
    });

    $("#zona").on("mousemove", (evento) => {

        // Obtengo la posición del ratón
        const x = evento.offsetX;
        const y = evento.offsetY;

        mostrarPosicion(x, y);
        moverCosa(x, y);
    });
});

//--------------------------------------------------------------------------
// Funciones 
//--------------------------------------------------------------------------
function mostrarPosicion(x, y) {
    $('#posx').text(x);
    $('#posy').text(y);
}

function moverCosa(x, y) {
    // Asigna la nueva posición
    $("#cosa").css('left', x-10+"px");
    $("#cosa").css('top', y-10+"px");
}