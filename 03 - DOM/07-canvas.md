# El API Canvas 
El canvas HTML es un elemento que nos permite dibujar gráficos en el cliente utilizando para ello JavaScript. En el siguiente enlace tenemos un ejemplo donde podemos experiemntar con el canvas.

https://developer.mozilla.org/es/docs/Web/API/Canvas_API

## Elemento HTML
Para poder trabajar con un canvas, tenemos de algún modo que insertarlo o crearlo en nuestro documento. La forma más sencilla es insertar el elemento HTML como podemos ver en el siguiente ejemplo.

```html
 <canvas id="lienzo" width="100" height="100"></canvas>
```

Tenemos una pequeña referencia aquí 
https://www.w3schools.com/html/html5_canvas.asp


## API Canvas
El canvas HTML es un elemento que nos permite dibujar gráficos en el cliente utilizando para ello JavaScript. En la siguiente página disponemos de una referencia con los métodos diponibles en el objeto.

https://developer.mozilla.org/es/docs/Web/API/HTMLCanvasElement

Para dibujar, como veremos más abajo, será necesario obtener un contexto. Aquí os incluyo además algunos objetos que es interesante que sepáis que existen.

- **HTMLCanvasElement**: el propio elemento Canvas
- **ImageData**: Representa el mapa de pixels dentro del canvas. 
- **OffscreenCanvas**: Provee un canvas que puede ser renderizado fuera de la pantalla.

## El contexto
Cuando hacemos referncia al canvas, hablamos del elemento html que se encuentra dentro de nuestro documento HTML. El contexto, es un objeto con propiedades y métodos que puede ser utilizado para renderizar gráficos dentro de un elemento canvas. Un contexto puede por ejemplo ser 2d para trabajar con gráficos 2d o webgl para trabajar con gráficos 3d. 

Un canvas puede tener asociado un único contexto. Si utilizamos getContext más de una vez devolverá una referencia al mismo contexto, salvo que solicitemos tipos de contextos no compatibles. En este último caso se obtendría null en la segunda llamada.

```javascript
    const canvas = document.getElementById("lienzo");		
	
    // Obtiene un contexto para trabajar en 2d
    const ctx2d = canvas.getContext("2d");
    
    // Esta llamada devuelve null porque ya hemos solicitado un contexto 2d
    // los contextos 2d y webgl son incompatibles
    const ctxgl = canvas.getContext("webgl"); 
```
Os dejo aquí un enlace a la referencia de este método.

- https://developer.mozilla.org/es/docs/Web/API/HTMLCanvasElement/getContext

### API 2D
Aquí tenemos un enlace a la referencia del contexto 2d. Permite por ejemplo dibujar líneas, rectángulos, texto y otras formas.

- https://developer.mozilla.org/es/docs/Web/API/CanvasRenderingContext2D
- https://www.w3schools.com/tags/ref_canvas.asp

### API WebGL
Provee un interfaz que permite utilizar OpenGL para renderizar contenido en nuestro canvas.

- https://developer.mozilla.org/es/docs/Web/API/WebGL_API/Tutorial/Getting_started_with_WebGL#crear_el_contexto_de_webgl

# Canvas.js
El canvas es un componente que permite llevar al cliente la creación de diversos tipos de contenido que anteriormente debían renderizarse en el servidor. Esto permite desarrolar APIs como la que nos ocupa que permite traspasar la creación de gráficos con datos a la parte cliente de nuestra aplicación web. De este modo, a parte de pasar carga de trabajo al cliente
y crear gráficos interactivos, se reduce el volumen de datos que hay que intercambiar entre cliente y servidor.

EL API al que hacemos referencia es este:

- https://canvasjs.com/


## Importar a nuestro proyecto
Lo primero será importar el API a nuestro proyecto. En el directorio de nuestro proyecto podremos utilizar la siguiente línea de comandos.

```cmd
npm install --prefix=. canvasjs
```


## Insertar nuestro gráfico
Este ejemplo lo he sacado directametne de la web del API aquí podemos ver la simplicidad del API
a la hora de definir las características de nuestro gráfico.

Partiendo de este sencillo ejemplo, experimenta y crea diferentes gráficos.

```html
<!DOCTYPE HTML>
<html>
    <head>

        <script src="node_modules/canvasjs/dist/canvasjs.min.js" defer> </script>

        <script type="text/javascript">
            window.onload = function () {

            var chart = new CanvasJS.Chart("chartContainer", {
                theme: "light1", // "light2", "dark1", "dark2"
                animationEnabled: false, // change to true		
                title:{
                    text: "Basic Column Chart"
                },
                data: [
                {
                    // Change type to "bar", "area", "spline", "pie",etc.
                    type: "column",
                    dataPoints: [
                        { label: "apple",  y: 100  },
                        { label: "orange", y: 15  },
                        { label: "banana", y: 25  },
                        { label: "mango",  y: 30  },
                        { label: "grape",  y: 28  }
                    ]
                }
                ]
            });
            chart.render();

            }
        </script>
    </head>
    <body>
        <div id="chartContainer" style="height: 370px; width: 100%;"></div>
    </body>
</html>
```

## Generar gráficos a partir de la base de datos
Vamos a hacer algunos gráficos partiendo de datos en el servidor para que veamos como podríamos llevar a cabo el proceso.

