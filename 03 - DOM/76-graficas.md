# Tarea 6 - Gráficas 
Hemos visto como con la biblioteca [CanvasJS](https://canvasjs.com) podemos crear gráficos ricos a partir de los datos de nuestra aplicación. Con esta información podemos crear cuadros de mando o informes a partir de nuestros datos. A la hora de generar los gráficos podemos seguir dos enfoques:

- Genero el gráfico en el servidor: Tanto el código del gráfico como los datos los inyecto desde el servidor.
- Genero el gráfico en el cliente: Inserto el gráfico en el documento, pero descargo los datos del servidor utilizando algún tipo de llamada AJAX.

En nuestro caso vamos a hacer un ejemplo siguiendo cada uno de los enfoques. El ejercicio lo vamos a hacer sobre la aplicación que estamos desarrollando en PHP y vamos a partir de la siguiente consulta SQL.

```sql
   select month(fecha_pedido), count(*) from pedidos group by month(fecha_pedido)
```

Esta consulta permite obtener el número de pedidos por mes del año. De este modo podemos saber en qué meses se concentran los pedidos.

## Gráfico generado en el servidor 1
Crea un informe que corresponda con las ventas por mes. Sigue el mismo patrón que en el ejemplo para generar el gráfico.

https://canvasjs.com/php-charts/chart-index-data-label/

## Gráfico generado en el cliente con JQuery
Ahora vamos a seguir el siguiente ejemplo:

https://canvasjs.com/jquery-charts/resizable-chart/

Verás que este ejemplo en realidad es muy similar al anterior. Lo que hemos añadido es utilizar jQuery para generar el gráfico.

## Gráfico generado en el cliente con AJAX y JSON
Ahora vamos a seguir las instrucciones en el siguiente enlace para generar un gráfico en el cliente partiendo de los datos descargados por AJAX en formato JSON.

https://canvasjs.com/docs/charts/how-to/javascript-charts-from-json-data-api-and-ajax/

El funcionamiento será similar con la salvedad de que los datos los tendremos que descargar primero del servidor.