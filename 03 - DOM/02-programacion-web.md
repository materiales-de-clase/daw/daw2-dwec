# Programación web
En este apartado vamos a tratar los aspectos básicos de la programación JavaScript en la web. Estamos hablando por supuesto de desarrollo del lado del cliente.

# JavaScript en HTML "script"
Para que el navegador ejecute código JavaScript en un navegador tenemos que referenciarlo de algún modo. Existen dos formas en que podemos referenciar el código JavaScript.

## Código en línea
Utilizando el tag script podemos escribir dentro de la misma página código JavaScript.

```html
    <!-- Un script javascript -->
    <script>
        function saluda() {
            alert('hola');
        }
    </script>

    <!-- Un módulo de primer nivel escrito en línea -->
    <script type="module">
        /* El código del módulo JavaScript va aquí */
    </script>
```

## Referenciando un archivo
Utilizando el tag script también podemos cargar código desde un archivo externo al documento HTML. Esta sería la forma más común de incluir código JavaScript en nuestro sitio web y la forma que vamos a utilizar generalmente en clase, salvo que se indique lo contrario.

```html
    <!-- Incluye varios scripts -->
    <script src="js/utilidades.js"></script> 
        <!-- Notese que hay que cerrar el tag. Es obligatorio aunque
             esté vacío -->
    <script src="js/validaciones.js"></script>    

    <!-- Incluye un módulo -->
    <script type="module" src="js/utilidades.js"></script> 
```

Entre las ventajas de utilizar src tenemos:

- No metemos mucho código mezclado con el HTML. Existe separación entre HTML y lógica de nuestra aplicación.
- Si varias páginas comparten el mismo código no hay que duplicarlo.
- Si varias páginas comparten el mismo código se descarga una sola vez. Permitimos al navegador hacer uso de la caché.
- Podemos utilizar código exportado por otros sitios web.


# Ejecución del código JavaScript
Por defecto, **siempre que el navegador se encuentra con un elemento script va a ejecutar el script**. Este comportamiento es heredado desde los inicios de la web, pero en la actualidad no está recomendado salvo que sea algo necesario. Ejecutar los scripts a medida que se cargan puede **ralentizar la carga y renderizado de nuestro documento** así que, salvo causa justificada, **deberíamos evitar el modo por defecto**.

Para controlar el momento en que el script se ejecute tenemos diversas opciones.:

## El atributo async
El script se descarga de forma asíncrona, es decir, sin detener el análisis HTML. Eso sí, una vez el archivo se ha descargado, se pausa la carga del documento para ejecutar el script. Tras la ejecución del script se reanuda el análisis.

```html
    <script src="js/inicializacion.js" async></script>
```

Hay que tener presente que, ya que los documentos podrían terminar la carga en diferente orden, **no se garantiza que se ejecuten en el mismo orden de aparición**.

Esta forma de carga se puede utilizar para **scripts sin dependencias** que necesiten manipular el DOM antes de que finalice la carga del documento.

## El atributo defer
Al igual que el atributo async, defer hacer que la carga del documento se haga en segundo plano mientras que continúa el análisis del documento. La diferencia respecto a async es que estos scripts no van a procesarse hasta que finalice la **carga y análisis completo del documento HTML**. 

```html
    <!-- Carga el script -->
    <script src="js/inicializacion.js" defer></script>

    <!-- Los módulos cargan de forma diferida de forma predeterminada.
         De modo que no sería necesario especificar el atributo -->
    <script type="module" src="js/utilidades.js"></script>
```

En este caso, la ejecución de todos los scripts defer se hará **en el mismo orden de aparición en el documento**.

Podemos considerar esta la mejor solución de forma general salvo que el script tenga que manipular el DOM antes de que haya terminado la carga. Es importante también si el script tiene dependencias lo que implica que el orden es importante.

## Fases en la ejecución de código JavaScript
En programación en entorno cliente no existiría el concepto de programa como tal. En este caso, la ejecución de código se haría del siguiente modo:

1. Se van cargando los scripts de acuerdo a los elementos script.
2. Se ejecutan los scripts. Dependiendo de la configuración del elemento script esto puede pasar en diferentes momentos.
3. Entramos en la fase de ejecución guiada por eventos. Se ejecutan funciones en respuesta a acciones llevadas a cabo por el usuario.

# El objeto global
En le navegador, sobre el objeto global, tendremos en cuenta lo siguiente:

- El objeto global, es donde se define la biblioteca standard de JavaScript. Es donde están los objetos/interfaces que el navegador nos expone. En el caso de páginas web podemos referenciar a este objeto como globalThis o window.
- Hay un objeto global por **cada ventana independiente o pestaña del navegador**. Es decir, cada página cargada tendrá un objeto window asociado.
- Todo el código JavaScript que se ejecuta en la misma ventana o pestaña comparte este código. Aunque tengamos varios scripts, es como si todos los scripts fueran uno.
- Además de contener las funciones y objetos globales de JavaScript, el objeto global da acceso a la ventana del navegador.
- Una de las propiedades del objeto global es **window**. Dicha propiedad es un puntero a si mismo.
- En nuestros scripts podremos referenciar cualquier atributo o función de este objeto utilizando window.XXX.

En el siguiente enlace tenemos una referencia a las propiedades y métodos disponibles en el objeto [window](https://www.w3schools.com/jsref/obj_window.asp).

Por ejemplo, para invocar al método alert para mostrar un mensaje, podríamos utilizar un código como el siguiente.

```javascript     
    window.alert('Hola mundo');

    // Puedo referenciar también al método sin el prefijo window
    // esto es así por ser window solo una referencia al objeto global
    alert('hola mundo');
```

Aunque podemos referenciar a alert utilizando window.alert y alert, **utilizar el prefijo window puede hacer el código más claro y reducir ambiguedades** en algunos contextos al ofrecer un información adicional sobre la propiedad que estamos utilizando.

# Espacio de nombres compartido
**En scripts no modulares**, todos los scripts que carguemos en un documento comparten espacio de nombres. Esto quiere decir que constantes, variables, funciones o clases globales definidas en cualquier script cargado en una página son visibles para el resto.

Por ejemplo:
```html    
    <script>
        // Declara una variable a  
        let a = 10;
    </script>

    <script>
        // La variable a es visible a este script
        // Ya que todos los scripts terminan integrados
        // en la misma página
        if(a == 10) {
            window.alert("Veo a");
        }
    </script>
```

# Modelo de hilos
JavaScript es un **lenguaje monohilo**. Por eso, podemos estar seguros de que dos gestores de eventos nunca se van a ejecutar al mismo tiempo. Esto también tiene sus problemas. Por ejemplo, **el navegador deja de responder** mientras que los gestores de evento están en ejecución. Por lo tanto, los gestores deben siempre **ejecutarse rápido** o de lo contrario el usuario percibirá que la aplicación no responde.

Si necesitamos hacer cálculos pesados existen diversas herramientas como el concepto de webworker. En el tema de AJAX nos extenderemos sobre este asunto.

# Entrada/Salida
A continuación listo los orígenes/destinos de información disponibles para nuestro script:

- Contenido del documento
- Campos de formularios
- Eventos
- document.URL
- document.cookie
- document.navigator
- console.log

# Gestión de errores
Los programas que corren en el navegador no pueden romperse. Si no gestionas adecuadamente las excepciones se registrarán errores en la consola, pero
el documento seguirá funcionando y se seguirá invocando a los gestores de eventos que ya se hayan registrado. Podemos gestionar de forma global los
errores no gestionados por los scripts de forma normal definiendo la función **window.onerror**. En caso de un error que un error no sea gestionado en toda la pila de llamadas, se llamará a esta función para gestionar el problema y, por ejemplo, avisar al usuario de que ha ocurrido un error.


# Otros objetos
A parte del objeto window, en el BOM tenemos otros objetos que permiten acceder al contexto en que se ha cargado nuestra página web

## El objeto location
Este objeto nos va a permitir obtener información sobre la url cargada en el navegador.

- window.location.href
- window.location.hostname
- window.location.pathname
- window.location.protocol

Asigna una URL. Permite cargar otra página web.

- location.assign()

## El objeto history
Permite acceso al historial de sesión del navegador. Ejemplos de métodos:

- history.back();
- history.forward();

## El objeto navigator
El objeto navigador permite obtener información de estado e identidad del user agent.

- navigator.appName
- navitatod.appCodeName
- navitador.platform

