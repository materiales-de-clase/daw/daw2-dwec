"use strict";


//--------------------------------------------------------------------------
// Programa principal
//--------------------------------------------------------------------------

// Importante. Debe invocarse en el onloads
console.log("Secuencia de inicialización iniciada");

// Cuando termine de cargarse la página va a inicializar el script
window.addEventListener('load',  function (evento) {

    // Asigna el gestor de evento de nuestro teclado
    window.addEventListener('keydown', onKeyDown);    
});

console.log("Secuencia de inicialización finalizada");


//--------------------------------------------------------------------------
// Funciones 
//--------------------------------------------------------------------------

function onKeyDown(e) {
    
    // Obtengo la cosa
    let cosa = document.getElementById('cosa');

    // Obtengo la posición actual
    let top  = Number(cosa.style.top.replace('px', ''));
    let left = Number(cosa.style.left.replace('px', ''));

    // En función de la tecla, mueve
    switch(e.keyCode) {

        case 38: // Arriba
            top--;
            break;
        
        case 40: // Abajo
            top++;
            break;

        case 37: // Izquierda
            left--;
            break;

        case 39: // Derecha
            left++;
            break;
    }

    // Asigna la nueva posición
    cosa.style.left = left+'px';
    cosa.style.top = top+'px';

    e.stopPropagation();
}


