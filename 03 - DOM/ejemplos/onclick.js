"use strict";

//--------------------------------------------------------------------------
// Programa principal
//--------------------------------------------------------------------------

// Importante. Debe invocarse en el onloads
console.log("Secuencia de inicialización iniciada");

// Cuando termine de cargarse la página va a inicializar el script
window.addEventListener('load', function (evento) {

    document.getElementById('area').addEventListener('click', areaOnClick);
    
});


console.log("Secuencia de inicialización finalizada");


//--------------------------------------------------------------------------
// Funciones
//--------------------------------------------------------------------------

function areaOnClick(e) {
    
    // Posición del ratón
    let x = e.offsetX;
    let y = e.offsetY;

    // Asigna el valor
    document.getElementById('x').textContent = x;
    document.getElementById('y').textContent = y;
}
