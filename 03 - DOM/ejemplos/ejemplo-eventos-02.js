"use strict";

//--------------------------------------------------------------------------
// Programa principal
//--------------------------------------------------------------------------

window.addEventListener('load', (evento) => {

    // Este código se ejecuta cuando termina de cargar la página
    console.log("Inicio load");

    // Inicializa el formulario
    inicializarFormulario(document.frm);
});


//--------------------------------------------------------------------------
// Funciones
//--------------------------------------------------------------------------
function inicializarFormulario(formulario) {

    formulario.addEventListener('input', (evento) => {
        
        const campo = evento.target;
        
        // Valida un campo        
        if(campo.tagName == 'INPUT') {
            validarCampo(campo);
        }
        
    });
}

/**
 * Valida un campo
 * 
 * @param {*} campo 
 */
function validarCampo(campo) {

    // Necesito saber la validación a aplicar. La escribí en un atribto
    const validacionAttr = campo.attributes['validacion'].value;

    // Lando la validación
    let r = window['validacion'+validacionAttr](campo.value);
   
    // Muestra el error
    if(!r) {
        mostrarError(campo);
    } else {
        limpiarError(campo);
    }
}

/**
 * Mostramos un error.
 */
function mostrarError(campo) {
    campo.style.backgroundColor = 'red';
}

/**
 * Limpiamos un error
 */
 function limpiarError(campo) {
    campo.style.backgroundColor = 'white';
}


//--------------------------------------------------------------------------
// Validaciones
//--------------------------------------------------------------------------

/**
 * Comprueba si un campo está vacío
 * 
 * @param {*} valor 
 */
function validacionNoVacio(valor) {
    return valor.length > 0;
}

/**
 * Comprueba si un campo está vacío
 * 
 * @param {*} valor 
 */
function validacionEsPar(valor) {
    return Number(valor) % 2 == 0;
}

/**
 * Comprueba si un campo está vacío
 * 
 * @param {*} valor 
 */
function validacionEsNumero(valor) {
    return Number(valor) != NaN;
}

