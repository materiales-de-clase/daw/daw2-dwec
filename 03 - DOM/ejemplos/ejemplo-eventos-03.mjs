
import { inicializarFormulario } from './ejemplo-eventos-03-validaciones.mjs'

//--------------------------------------------------------------------------
// Programa principal
//--------------------------------------------------------------------------

window.addEventListener('load', (evento) => {

    // Este código se ejecuta cuando termina de cargar la página
    console.log("Inicio load");

    // Inicializa el formulario
    inicializarFormulario(document.frm);
});


//--------------------------------------------------------------------------
// Funciones
//--------------------------------------------------------------------------
