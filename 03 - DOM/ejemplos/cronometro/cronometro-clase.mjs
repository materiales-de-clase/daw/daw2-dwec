
export { Cronometro };

class Cronometro {

    contador = 0;

    escuchadores = [];

    inicializar(cuanto = 0) {
        this.contador = cuanto;

        this.notificar();
    }

    avanzar(cuanto = 1) {
        this.contador += cuanto;

        this.notificar();
    }

    retroceder(cuanto = 1) {
        this.contador -= cuanto;

        this.notificar();
    }

    /** Arranca el avance automático */
    avanzarAutomaticamente(cuanto = 1) {
        
        function timeout(cronometro) {

            // Incremento el contador
            cronometro.contador += cuanto;
            
            // Notifico lo que ha pasado
            cronometro.notificar();

            // Programo la siguiente notificación
            setTimeout(() => { timeout(cronometro); }, cuanto*1000);
        }

        // Inicia el conteo
        setTimeout(() => { timeout(this); } , 1000*cuanto);
    }

    /**
     * 
     * @param {*} funcion función que recibe como argumento una instancia de Cronometro
     */
    addUpdatedListener(funcion) {
        this.escuchadores.push(funcion);
    }

    notificar() {
        for(let f of this.escuchadores) {
            f(this);
        }
    }
}