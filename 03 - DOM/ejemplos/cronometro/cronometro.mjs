
import { Cronometro } from './cronometro-clase.mjs';

//--------------------------------------------------------------------------
// Constantes
//--------------------------------------------------------------------------
const BOTON_INICIALIZAR = document.getElementById('inicializar');

//--------------------------------------------------------------------------
// Programa principal
//--------------------------------------------------------------------------

// Inicializa la página
window.addEventListener('load', function (evento) {

    // Creo el cronómetro
    const cronometro = new Cronometro();

    // Eventos a los botones
    document.getElementById('inicializar').addEventListener('click', (evento) => {
        cronometro.inicializar();
    });

    document.getElementById('amanual').addEventListener('click', (evento) => {
        cronometro.avanzar();
    });

    document.getElementById('rmanual').addEventListener('click', (evento) => {
        cronometro.retroceder();
    });    

    document.getElementById('avance').addEventListener('click', (evento) => {
        cronometro.avanzarAutomaticamente();
    });    
    
    // Añado listeners. Cuando se actualiza el cronometro avisa para que se actualicen
    // los diferentes componentes del interfaz de usuario
    cronometro.addUpdatedListener((cronometro) => {
        document.getElementById('cronometro1').innerText = cronometro.contador;
    });

    cronometro.addUpdatedListener((cronometro) => {
        document.getElementById('cronometro2').innerText = cronometro.contador;
    });

    cronometro.addUpdatedListener((cronometro) => {
        document.getElementById('cronometro3').innerText = cronometro.contador;
    });

});



//--------------------------------------------------------------------------
// Funciones
//--------------------------------------------------------------------------