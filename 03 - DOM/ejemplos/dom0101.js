"use strict";

//--------------------------------------------------------------------------
// Programa principal
//--------------------------------------------------------------------------

window.addEventListener('load', (evento) => {

    // Este código se ejecuta cuando termina de cargar la página
    console.log("Inicio load");

    // Gestion de los eventos
    document.getElementById('b1').addEventListener('click', (evento) => {
        document.getElementById('i1').style.backgroundColor = evento.target.style.backgroundColor;
    });

    // Gestion de los eventos
    document.getElementById('b2').addEventListener('click', (evento) => {
        document.getElementById('i1').style.backgroundColor = evento.target.style.backgroundColor;
    });

    // Gestion de los eventos
    document.getElementById('b3').addEventListener('click', (evento) => {
        document.getElementById('i1').style.backgroundColor = evento.target.style.backgroundColor;
    });

});



//--------------------------------------------------------------------------
// Funciones
//--------------------------------------------------------------------------

