"use strict";

console.log(suma('.fila1 input'));
console.log(suma('.fila2 input'));

console.log(suma('tr td:nth-child(1) input'));




function suma(selector) {
    // Campos en la fila 1
    const campos = document.querySelectorAll(selector);
    
    let total = 0;
    for(let c of campos) {
        total += Number(c.value);
    }

    return total;
}


function sumaFila(numeroFila) {

    // Campos en la fila 1
    const campos = document.querySelectorAll('.fila'+numeroFila+' input');
    
    let total = 0;
    for(let c of campos) {
        total += Number(c.value);
    }

    return total;
}