"use strict";
//--------------------------------------------------------------------------
// Programa principal
//--------------------------------------------------------------------------

// Inicializa la página
window.addEventListener('load', function (evento) {

    // Asigna el gestor de evento de nuestro teclado
    document.frm.addEventListener('click', (e) => {

        // Obtengo el elemento sobre el que se ha hecho click
        const elemento = e.target;
        
        // Si y solo si es un botón, tengo que hacer la operación
        if(elemento.tagName == "BUTTON") {
            
            // Elementos para hacer el cálculo
            const campos = document.frm.querySelectorAll('input');

            // Obtiene el texto dentro el botón
            let operador = elemento.innerText;
            
            // Calcula el resultado
            let resultado = window[operador](campos);

            // Asigna el resultado al div
            document.getElementById('res').innerText = resultado;
        }
    });
});

//--------------------------------------------------------------------------
// Funciones
//--------------------------------------------------------------------------
// Asigno la función al atributo '+' de window
window['+'] = (campos) => {

    // Inicializa el resultado
    let resultado = 0;
    
    // Recorre los campos y acumula
    for(let e of campos) {
        resultado += Number(e.value);
    }

    // Devuelve el rsultado
    return resultado;
}

// Asigno la función al atributo '-' de window
window['-'] = (campos) => {
    // Inicializa el resultado
    let resultado = 0;
    
    // Recorre los campos y acumula
    for(let e of campos) {
        resultado -= Number(e.value);
    }

    // Devuelve el rsultado
    return resultado;
}

