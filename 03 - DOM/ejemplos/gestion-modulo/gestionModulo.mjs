
import { Alumno } from './alumno.mjs';
import { Modulo } from './modulo.mjs';


//--------------------------------------------------------------------------
// Constantes y Variables globales al módulo
//--------------------------------------------------------------------------

const modulo = new Modulo("Desarrollo Web En Entorno Cliente");

//--------------------------------------------------------------------------
// Programa principal
//--------------------------------------------------------------------------

window.addEventListener('load', (evento) => {
    
    inicializarFormulario();
    inicializarModulo();
    inicializarBotonEliminar();
    mostrarTablaAlumnos();
       
});

//--------------------------------------------------------------------------
// Funciones
//--------------------------------------------------------------------------

function inicializarFormulario() {
    document.frm.addEventListener('submit', (evento) => {

        // No permite que se envíe el formulario
        evento.preventDefault();

        // Tomar los datos en el formulario
        const nombre = document.frm.nombre.value;
        const fechaNacimiento = document.frm.fechaNacimiento.value;
        const notaMedia = document.frm.nota.value;

        // Crear un Alumno
        const alumno = new Alumno(nombre, fechaNacimiento, notaMedia);

        // Añadirlo al Módulo
        modulo.addAlumno(alumno);
    });    
}

function inicializarModulo() {
    modulo.listaAlumnosModificadaListener = () => {
        mostrarTablaAlumnos();
    }
}

function inicializarBotonEliminar() {
    document.getElementById('borrarAlumno').addEventListener('click', (evento) => {
        modulo.borrarAlumno();
    });
}

function mostrarTablaAlumnos() {
    // Obtengo la tabla de alumnos
    const tablaAlumnos = document.getElementById("tablaAlumnos");
    
    // Mostrar los alumnos
    const html = getHtmlAlumnosUL(tablaAlumnos, modulo);    
    tablaAlumnos.innerHTML = html;
}


/**
 * Genera el HTML de  los alumnos en elemento pasado como argumento
 */
function getHtmlAlumnosUL(elemento, modulo) {
    let html = "<lu>"
    for(let e of modulo.alumnos) {
        html += "<li>";
            html += e.nombre + " " + e.fechaNacimiento;
        html += "</li>";
    }    
    html += "</lu>"
    
    return html;
}