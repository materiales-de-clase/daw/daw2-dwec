
export class Alumno {

    // Propiedades / Atributos / Estado / Variables
    nombre;
    fechaNacimiento;
    notaMedia;
    
    constructor(nombre, fechaNacimiento, notaMedia = 0) {

        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.notaMedia = notaMedia;

    }

    mostrar() {
        console.log(this.nombre, this.fechaNacimiento, this.notaMedia);
    }
}


