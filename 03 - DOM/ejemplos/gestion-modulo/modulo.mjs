import { Alumno } from './alumno.mjs';

export class Modulo {

    nombre;

    alumnos = [];

    listaAlumnosModificadaListener;

    constructor(nombre) {
        this.nombre = nombre;

        const paco = new Alumno('Paco', '2001-01-01');
        this.addAlumno(paco);

        const manolo = new Alumno('Manolo', '2001-01-01');
        this.addAlumno(manolo);
    }

    /**
     * Recibe un alumno y lo añade al módulo
     * 
     * @param {*} alumno 
     */
    addAlumno(alumno) {
        this.alumnos.push(alumno);

        if(this.listaAlumnosModificadaListener)
            this.listaAlumnosModificadaListener();
    }

    borrarAlumno() {
        this.alumnos.pop();

        if(this.listaAlumnosModificadaListener)
            this.listaAlumnosModificadaListener();
    }

    mostrarAlumnos() {
        for(let a of this.alumnos) {
            a.mostrar();
        }
    }
}

const dwec = new Modulo("Desarrollo Web Entorno Cliente");

const paco = new Alumno('Paco', '2012-01-01');
dwec.addAlumno(paco);

const manolo = new Alumno('Manolo', '2012-01-01');
dwec.addAlumno(manolo);

dwec.mostrarAlumnos();