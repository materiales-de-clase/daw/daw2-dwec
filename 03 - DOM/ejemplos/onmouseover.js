"use strict";

//--------------------------------------------------------------------------
// Programa principal
//--------------------------------------------------------------------------

// Importante. Debe invocarse en el onloads
console.log("Secuencia de inicialización iniciada");

// Cuando termine de cargarse la página va a inicializar el script
window.addEventListener('load', function (evento) {

    // Asigna el gestor de evento de nuestro teclado
    document.getElementById('area').addEventListener('mouseover', areaOnMouseOver);
});

console.log("Secuencia de inicialización finalizada");


//--------------------------------------------------------------------------
// Funciones
//--------------------------------------------------------------------------

function areaOnMouseOver(e) {
    
    // Posición del ratón
    let x = e.offsetX;
    let y = e.offsetY;

    // Asigna el valor
    document.getElementById('x').textContent = x;
    document.getElementById('y').textContent = y;
}
