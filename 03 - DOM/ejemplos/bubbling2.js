"use strict";
// Pone en rojo los campos del formulario dond escribamos más de
// 4 caracteres

//--------------------------------------------------------------------------
// Programa principal
//--------------------------------------------------------------------------

// Inicializa la página
window.addEventListener('load', function (evento) {

    // Asigna el gestor de evento de nuestro teclado
    document.frm.addEventListener('input', (e) => {

        const campo = e.target;
        if(campo.value.length > 4) {
            campo.style.background = 'red';
        }

    });
});

//--------------------------------------------------------------------------
// Funciones
//--------------------------------------------------------------------------
