"use strict";

//--------------------------------------------------------------------------
// Programa principal
//--------------------------------------------------------------------------

window.addEventListener('load', (evento) => {

    // Este código se ejecuta cuando termina de cargar la página
    console.log("Inicio load");

    document.getElementById('i1').addEventListener('focus', (evento) => {
        console.log('Has entrado en el campo i1');
        console.log(evento.constructor.name);
    });

    document.getElementById('i1').addEventListener('change', (evento) => {
        console.log('Has cambiado el campo i1');
        console.log(evento.constructor.name);

        // Obtiene el objeto sobre el que se ha disparado el evento
        const campo = evento.target;
        // En este caso, otra opción para coger el campo
        //const campo = document.getElementById('i1');

        // Obtiene el valor
        let valor = campo.value;

        // Validación. Si no es hola da error
        if(valor != 'hola') {
            campo.style.background = 'red';
        } else {
            campo.style.background = 'white';
        }
    });

    document.getElementById('b1').addEventListener('click', (evento) => {
        console.log('Has pulsado sobre botón 1');
        console.log(evento.constructor.name);
    });

    document.getElementById('b2').addEventListener('click', (evento) => {
        console.log('Has pulsado sobre botón 2');
        console.log(evento.constructor.name);
    });

    document.getElementById('b3').addEventListener('click', (evento) => {
        console.log('Has pulsado sobre botón 3');
        console.log(evento.constructor.name);
    });

});


//--------------------------------------------------------------------------
// Funciones
//--------------------------------------------------------------------------
