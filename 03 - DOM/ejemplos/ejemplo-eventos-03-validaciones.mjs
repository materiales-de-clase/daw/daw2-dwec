
export { inicializarFormulario };

function inicializarFormulario(formulario) {

    formulario.addEventListener('input', (evento) => {
        
        const campo = evento.target;
        
        // Valida un campo        
        if(campo.tagName == 'INPUT') {
            validarCampo(campo);
        }
        
    });
}

/**
 * Valida un campo
 * 
 * @param {*} campo 
 */
function validarCampo(campo) {

    // Necesito saber la validación a aplicar. La escribí en un atribto
    const validacionAttr = campo.attributes['validacion'].value;

    const llamada = 'validacion'+validacionAttr+'(campo.value)';

    // Lando la validación
    let r = eval(llamada);

    // Muestra el error
    if(!r) {
        mostrarError(campo);
    } else {
        limpiarError(campo);
    }
}

/**
 * Mostramos un error.
 */
function mostrarError(campo) {
    campo.style.backgroundColor = 'red';
}

/**
 * Limpiamos un error
 */
 function limpiarError(campo) {
    campo.style.backgroundColor = 'white';
}


//--------------------------------------------------------------------------
// Validaciones
//--------------------------------------------------------------------------

/**
 * Comprueba si un campo está vacío
 * 
 * @param {*} valor 
 */
function validacionNoVacio(valor) {
    return valor.length > 0;
}

/**
 * Comprueba si un campo está vacío
 * 
 * @param {*} valor 
 */
function validacionEsPar(valor) {
    return Number(valor) % 2 == 0;
}

/**
 * Comprueba si un campo está vacío
 * 
 * @param {*} valor 
 */
function validacionEsNumero(valor) {
    return Number(valor) != NaN;
}

