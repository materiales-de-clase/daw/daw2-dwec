"use strict";

let campos = document.querySelectorAll(".operando");
let campo1 = Number(document.getElementsByName('campo1')[0].value);
let campo2 = Number(document.getElementsByName('campo2')[0].value);

let operador = document.getElementsByName('operador')[0].value;

let r = 0;

switch(operador) {
    case '+': 
        r = campo1+campo2;
        break;
    case '-': 
        r = campo1-campo2;
        break;
}

let resultado = document.getElementsByName('resultado')[0];
resultado.value = r;