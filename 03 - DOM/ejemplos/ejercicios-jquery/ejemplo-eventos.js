"use strict";

//--------------------------------------------------------------------------
// Programa principal
//--------------------------------------------------------------------------
$(document).ready(() => {

    // Inicialización del programa
    console.log('Inicialización del script');

    // Asigno gestores de eventos a cada uno de los botones
    $("#b1").on("click",(evento) => {
        console.log("Has pulsado botón 1");
    });

    $("#b2").on("click",(evento) => {
        console.log("Has pulsado botón 2");
    });

    $("#b3").on("click",(evento) => {
        console.log("Has pulsado botón 3");
    });
});



//--------------------------------------------------------------------------
// Funciones
//--------------------------------------------------------------------------
