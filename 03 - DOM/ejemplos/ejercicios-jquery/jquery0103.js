"use strict";

//--------------------------------------------------------------------------
// Programa principal
//--------------------------------------------------------------------------
$(document).ready(() => {

    // Inicialización del programa
    console.log('Inicialización del script');

    // Asigno el gestor de eventos
    $('#b1, #b2, #b3').on('click', (evento) => {

        // Texto en el botón
        const texto = evento.target.innerText+"<br>";

        // Obtengo referencia al párrafo
        $('.parrafo').append(texto);
    });
});



//--------------------------------------------------------------------------
// Funciones
//--------------------------------------------------------------------------
