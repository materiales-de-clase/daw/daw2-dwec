# Nodos del DOM
Un documento es representado como un árbol de elementos. Hay muchos tipos de nodos en el DOM. DocumentType, Element, Entity, EntityReference, Notation, ProcessingInstruction, Text...

De entre todos ellos, hay cuatro con los que generalmente vamos a trabajar.

- **Document** : Representa el documento. Se puede utilizar como punto de partida para navegar por el árbol de objetos que representa el documento.
- **Element** : Cada elemento tiene asociada un nodo de este tipo.
- **Attr** : Representa un atributos de una etiqueta HTML    
- **Text** : Almacena el texto de una etiqueta

En [este enlace de w3schools](https://www.w3schools.com/jsref/) hay disponible una referencia con todos los elementos disponibles en JavaScript.

# Obtener nodos del documento
Para acceder a los diferentes nodos del documento, el objeto document dispone de una serie de métodos que pueden ser utilizados: 

- getElementById = el elemento con el ID
- getElementsByTagName
- getElementsByName
- getElementsByClassName
- querySelector

Estos métodos nos va a devolver una referencia al objeto solicitado o null si no hay coincidencia.

```javascript
    // En la variable campo tengo una referencia al objeto 
    // que representa el campo
    let campo = document.getElementById('campo');

    // Utilizando la referencia podemos modificar sus propiedades
    campo.disabled = true;
```

# Operaciones con nodos
Con los nodos obtenidos podemos realizar diversas operaciones. Por ejemplo:

- Acceder al valor de un atributo
- Establecer el valor de un atributo
- Crear o añadir nuevos nodos
- Borrar nodos
- Mover nodos de su lugar en el árbol
    
## Para crear un nuevo nodo
En el siguiente ejemplo vemos código que podría ser utilizado para crear un nodo.

```javascript
    // 1. Crear un nodo de tipo element
    let parrafo = document.createElement("p");

    // 2. Crear un nodo de tipo Text para el contenido
    let contenido = document.createTextNode("Sintesis");

    // 3. Vincular el nodo Text con el nodo
    parrafo.appendChild(contenido);

    // Vinvular el element con el documento
    document.body.appendChild(parrafo);
```

## Para eliminar un nodo
El siguiente ejemplo elimina un nodo del documento.

```html
    <p id="borrame">Párrafo a eliminar</p>
```

```javascript
    let parrafo = document.getElementById("borrame");
    parrafo.parentNode.removeChild(parrafo);
```

## Desabilitar elementos
En ocasiones, por ejemplo en formularios, puede darse que queramos desactivar un campo determinado para evitar que el usuario pueda introducir datos. Esto lo podemos hcar con el atributo disabled.

```javascript
    // Desactiva un botón
    document.getElementById("boton").disabled = true;     
```

## Ocultar elementos
Para ocultar elementos, podemos añadir a el atributo elemento.className la clase que lo va a volver oculto o trabajar directamente con el atributo style. A continuación un par de ejemplos.

Utilizando el atributo className
```javascript
    let nodo = document.getElementById('elnodo');
    
    // Asigno el atributo class del elemento (se llama className para evitar conflictos)
    nodo.className = 'oculto';
```

Utilizando el atributo style
```javascript
    document.getElementById('elnodo').style.visibility = 'hidden'; 
```

## Acceder al valor de un campo
Podemos acceder al valor de los campos de un formulario. Por ejemplo:

```javascript
    
    // Asigna el valor Paco en el campo paco
    document.getElementsByName("nombre")[0].value = "Paco"; 

    // Obtiene el valor en el campo
    let nombre = document.getElementsByName("nombre")[0].value; 
```
## Modificar/obtener el contenido de un elemento
Disponemos de dos propiedades interesantes:

- [innerText](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/innerText): representa el texto renderizado dentro de un elemento y sus descendientes. 
- [textContent](https://developer.mozilla.org/en-US/docs/Web/API/Node/textContent): representa el texto dentro de un elemento y sus descendientes. 
- [innerHTML](https://developer.mozilla.org/es/docs/Web/API/Element/innerHTML): devuelve o asigna el contenido HTML de un determinado elemento.

```javascript
    document.body.innerHTML = "";  // Reemplaza el contenido de <body> con una 
                                   // cadena vacía
```

```javascript
<div id="divA">This is <span>some</span> text!</div>

let text = document.getElementById('divA').textContent;
// The text variable is now: 'This is some text!'
```

# Ejercicios
Crea el 
## ejercicio nodos01
Crea un pequeño formulario en html que tenga los campos:

- Campo1
- Campo2
- Campo3
- Total

Junto a cada campo pon dos botones. El botón a la izquierda permitirá ocultar/mostrar. El botón a la derecha permitirá desactivar/activar el campo.

Ahora inserta un botón que permita calcular la suma de campo1, campo2 y campo3 y ponga el resultado en total.
