# Tarea 5 - Juego 2D
El objetivo de esta tarea es crear un pequeño juego como el de la captura.

![imagen](recursos/juego2d.jpg)

En dicho juego, se mostrarán en alguna parte de la pantalla un bloque cuyo tamaño y tiempo de espera dependerá del nivel de dificultad. El jugador tendrá que hacer click en el bloque antes de que finalice la cuenta atrás (que también deberá aparecer en pantalla). Si no hace click a tiempo el jugador perderá. Si hace click a tiempo, se sumará el tiempo restante al marcador de puntos.


# Descripción del juego
- Deberá permitir de algún modo escoger el nivel de dificultad entre fácil, normal, difícil.
- El nivel de dificultad afectará al tamaño de los bloques, el tiempo en pantalla, y la el incremento de la dificultad después de cada acierto del jugador.
- El incremento de la dificultad se implementará de modo que cada vez que el jugador acierta:
  - El cuadrado será X más pequeño dependiendo del nivel de dificultad.
  - El tiempo disponible para acertar disminuirá den función del nivel de dificultad.
- El juego será infinito, terminando cuando el tiempo de jugada sea menor de 0 o el tamaño del cuadrado sea 0. 

# Entrega
Comprime el directorio del proyecto y adjúntalo a la tarea.
