# Tarea 2 - Formulario
Directorio dom/tareas/formulario. Lee todo el enunciado antes de empezar.
## Descripción general
Vamos a crear un formulario web interactivo que permite recoger los datos de un cliente. El formulario debe recoger los siguientes datos:

- **Datos personales**
  - Nombre
  - Apellidos
  - DNI
- **Cuenta**
  - email
  - contraseña1
  - contraseña2

En formulario tendrá dos botones:

* Uno para aceptar y 
* otro para limpiar el formulario.

## Características
Aquí indico las funcionalidades que deben implementarse en el formulario. Criterios generales:

- No se utilizan módulos (-1 punto)
### ayuda (3 puntos)
Debajo del formulario tendremos un div con un parrafo donde mostraremos la ayuda de cada uno de los campos. Se mostrará en dicho div al entrar en cada uno de los campos. Esta ayuda consistirá en un pequeño texto descriptivo del campo. Criterios de valoración (indico lo que resta hacer o no hacer algo):

- Crea una función para mostrar la ayuda que recibe el mensaje a mostrar (-1 punto)
- Utiliza el evento adecuado para controlar cuando se entra en un campo (-1 punto)
- Un array asociativo por algún tipo de clave que permita enlazar un texto a un campo. (-1 puntos)
- Un atributo en la página web que permita especificar la ayuda directamente en el HTML. (-0 puntos)
- Si un campo no tiene ayuda, debe borrarse la ayuda que se esté mostrando en el div. (-1 punto)

Existen diferentes eventos que puedes utilizar para controlar las entradas y salidas de los campos. Utiliza un evento que permita detectar cuando se entra en un campo.

### validación de campos (4 puntos)
Vamos a validar campo a campo y no se deje abandonar un campo si el valor es  erróneo. Indico a continuación lo que hay que hacer junto con los criterios:

- Crea una función para mostrar errores que recibe el campo y el mensaje de error (-1 punto)
- Crea una función para limpiar un mensaje de error. (-1 punto)
- Si el campo es erróneo, se cambiará el estilo del campo para mostrarlo (fondo rojo). Se mostrará además en el mismo sitio que antes se mostraba la ayuda (reemplazándola) el error que se ha detectado con una cadena de texto descriptiva. (-1)
- Será necesario implementar las siguientes validaciones:
  - Las validaciones se implementarán como funciones en un módulo a parte (-1 punto)
  - El mapeo de la validación a un campo se hará utilizando una de las siguientes opciones:
    - Un atributo del elemento en el HTML (-0 puntos).
    - Un array o mapa que asocie un tipo de validación a un campo utilizando alguna clave en el campo (-1 punto)
  - En el caso del campo DNI, tendrá que ser un DNI válido (implementar validación del DNI) (-0.5 punto)
  - En el caso de nombre, etc, se consideran campos obligatorios que estarán mal si están vacíos o son contienen espacios espacios. (-0.5 puntos)
  - El email tiene que contener una arroba. (-0.5 puntos)
  - Los dos campos de contraseña deben ser iguales. Esta validación se hace en el segundo campo. (-0.5 puntos)

### validación del formulario (3 puntos)
Que se valide el formulario al enviarlo. Debe implementar lo siguiente:

- Si hay errores no se envía el formulario (-1 punto)
- Si tiene errores, poner el foco en el campo con el primer error y llevar a cabo las mismas acciones que cuando se valida un campo independiente. (-2 punto)
- Los campos de contraseña han de ser iguales. Si no lo son, el error se marcará en el primer campo invicanto al usuario a introducir la contraseña en los dos de nuevo. (-1 punto)


## Entrega
La entrega la harás por el repositorio, pero además será necesario que adjuntes a la tarea un archivo comprimido con el nombre apellidos_nombre_formulario que contenga los siguientes archivos:

- formulario.html: el código html del formulario
- formulario.mjs: el código JavaScript asociado al formulario
- formulario-validaciones.mjs: las funciones de validaciones

## Rúbrica de corrección
|Puntuación|Descripción|
|--|--|
|5|El programa funciona de acuerdo a las especificaciones|
|2|Utilización adecuada y óptima de las funciones y gestión de eventos siguiendo las directrices marcadas en clase|
|0.5|Programación modular y código reutilizable|
|2|Sigue un planteamiento original|
|0.5|Limpieza y documentación|
