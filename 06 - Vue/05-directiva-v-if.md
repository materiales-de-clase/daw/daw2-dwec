# Directiva v-if/v-show
La directiva *v-if* nos permite renderizar contenido en función de una condición mientras que v-show lo renderiza pero utiliza css para mostrarlo u ocultarlo basado también en una condición. La documentación está disponible en:

https://es.vuejs.org/v2/guide/conditional.html

Nosotros vamos a trabajar sobre los siguiente ejemplos:

- [v-if.html](recursos/04-ejemplo-v-if.html)
- [v-if.js](recursos/04-ejemplo-v-if.js)

