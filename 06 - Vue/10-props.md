# Properties
Permite definir propiedades de los componentes de modo que pueda tener diferentes configuraciones para el mismo componente. Un componente puede tener ciertos atributos que le puedo pasar desde el componente padre. Lo que sería @Input en Angular

https://vuejs.org/guide/components/props.html
https://vuejs.org/guide/components/props.html#prop-types

Estas propiedades pueden ser también validadas. Dispones de más información sobre como definir propiedades y para qué en los enlaces suministrados.

Componente Counter
```html
<template>
    <h2>{{ customTitle }} </h2>
    <p> {{ counter }} <sup>2</sup> = {{ squareCounter }} </p>

    <div>
        <button @click="increase">+1</button>
        <button @click="decrease">-1</button>
    </div>

</template>

<script>
export default {
  
    // Propiedades de este componente.
    // Las propiedades además pueden validarse.
    // Las propiedades están definidas en la opción properties. 
    props: {
        title: String,

        // Las propiedades tienen también propiedades. Estas pueden ser validadas, tener valores por defecto, un tipo, etc.
        start: {
            type: Number,
            default: 100,                
            // required: true
            https://vuejs.org/guide/components/props.html#prop-validation
            validator( value ) {
                return value >= 0
            }
        }
    },
    
    data() {
        return {
            counter: this.start
        }
    },

    methods: {
        getSquareValue() {
            return this.counter * this.counter
        },
        increase() {
            this.counter++
        },
        decrease() {
            this.counter--
        },
    },
    computed: {
        squareCounter() {
            return this.counter * this.counter
        },
        customTitle() {
            return this.title || 'Counter'
        }
    }
}
</script>

<style>

button{
    background-color: #64B687;
    border-radius: 5px;
    border: 1px solid white;
    color: white;
    cursor: pointer;
    margin: 0 5px;
    padding: 5px 15px;
    transition: 0.3s ease-in-out;
}

button:hover {
    background-color: #5aa67b;
    transition: 0.3s ease-in-out;
}

</style>
```

Página principal

```html
<template>
  <img alt="Vue logo" src="./assets/logo.png">
  
  <!-- 
    Estoy pasando un valor al atributo start. Esto hace que que exista un atributo en el objeto con nombre start
    y el valor pasado como argumento. Este atributo se puede referenciar como this.start en las diferentes partes de la
    definición del componente.

    A la hora de definir el atributo, si lo pongo sin : (sin v-bind) asigno el valor tal cual. Siempre va a ser una cadena.
    De modo que utilizo v-bind. Esto enlaza en campo con la propiedad.

    Los dos siguientes elementos son equivalentes. (v-bind se puede comprimir como : )
    Además de poner el valor podemos poner una variabla de JavaScript
    -->    
    <Counter v-bind:start="100" title="contador 2" />  
    <Counter :start="100" />

</template>

<script>
import Counter from './components/Counter.vue'

export default {
  name: 'App',
  components: {
    Counter
  }
}
</script>

<style>
#app {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #2c3e50;
  margin-top: 60px;
}
</style>

```

