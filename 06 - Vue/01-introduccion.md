# Introducción 
Vue es un **framework progresivo**. Esto implica que se pueden migrar a Vue los componentes que se vayan necesitando dentro de la aplicación, es decir, no es necesario que toda nuestra aplicación sea reconstruida utilizando Vue. Podemos tener por ejemplo una aplicación que utilice cualquier otro framework o no y tener un contenedor controlado por Vue y hacer que se integre con el resto de la aplicación. Además Vue se encuentra entre los framworks más queridos por los desarrolladores de acuerdo con las estadísticas ofrecidas por stackoverflow en el siguiente enlace:

https://insights.stackoverflow.com/survey/2020#technology-most-loved-dreaded-and-wanted-web-frameworks-loved2 
https://insights.stackoverflow.com/survey/2021#most-popular-technologies-webframe

# Características
A continuación algunas características del framework Vue.

- **Fácil de aprender** si conoces HTML, CSS y tienes una buena base de JavaScript
- **Versátil y escalable**: Se pueden hacer crecer las aplicaciones de acuerdo con nuetras necesidades y además hay disponible un buen abanico de paquetes disponibles para Vue.
- **Rendimiento**: la implementación del DOM es muy rápida.
- **Documentación**: La documentación del framework es muy buena y completa.
