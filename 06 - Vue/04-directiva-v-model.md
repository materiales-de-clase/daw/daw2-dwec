# Directiva v-model
La directiva v-model nos permite enlazar bidireccionalmente nuestro modelo de datos con los campos de un formulario. Dispones de la documentación a completo en el siguiente enlace:

https://es.vuejs.org/v2/guide/forms.html

Nosotros vamos a trabajar sobre los siguiente ejemplos:

- [v-model.html](recursos/02-ejemplo-v-model.html)
- [v-model.js](recursos/02-ejemplo-v-model.js)
  
  