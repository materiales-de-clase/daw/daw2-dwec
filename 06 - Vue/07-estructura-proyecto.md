# Estructura de directorios
En este apartado vamos a pararnos a echar un vistazo a la estructura de directorios de nuestro proyecto recien creado.

## Ficheros en el raíz del proyecto

- **dist**: será donde se generará el build de la aplicación
- **public**: directorio público que sirve como punto de partida para montar la aplicación. Actualmente tenemos un index.html y el favicon. Este directorio contiene el punto de entrada de la aplicación.
- **src**: Código fuente de la aplicación.
- tests: Contendría las pruebas de aplicación. Actualmente no vamos a profundizar en eso.
- .browserslistrc: Permite adaptar la salida para que sea compatible con ciertos navegadores. 
- .gitignore: Viene ya configurado con archivos que no deberían subirse al repositorio.  
- babel.config.js: Archivo de configuración de babel.
- js.config.json: Archivo de configuración de JavaScript
- package.json: Relacionado con los paquetes instalados. No deberíamos tocar este archivo. Para actualizarlo se utilizará el comando npm. 
- package-lock.json: Está también relacionado con los paquetes.
- Readme.md: Es un archivo con información y ayuda sobre el proyecto.
- vue.config.js: Archivo de configuración de Vue.

## Contenido de la carpeta src
Dentro de esta carpeta tenemos lo siguiente:

- **assets**: Contiene los recursos estáticos de nuestra página. Imágenes, sonidos, etc.
- **components**: Este directorio contiene los componentes de view. 
- main.js: Crea la aplicación y la monta en el elemento con id app que está en el index.html. Es decir, monta el componente raíz de la aplicación.

