# Instalar bootstrap
Para instalar bootstrap en nuestro proyecto seguiremos los siguientes pasos.

En primer lugar, instalamos boostrap utilizando npm.

```shell
$ npm install bootstrap@latest

up to date, audited 947 packages in 1s

104 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```

Ahora tendrás que editar el archivo main.js para importar bootstrap al proyecto del siguiente modo:

```js
import { createApp } from 'vue'
import App from './App.vue'

// Importa boostrap
import "bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';

createApp(App).mount('#app')
```

En este punto, ya debería ser posible utilizar bootstrap dentro del proyecto.


# Instalar bootstrap-icons
Primero instalamos el paquete

```shell
$ npm install bootstrap-icons

added 1 package, and audited 948 packages in 3s

104 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```

Ahora tendrás que editar el archivo main.js para importar bootstrap-icons al proyecto del siguiente modo:

```js
import { createApp } from 'vue'
import App from './App.vue'

// Importa boostrap
import "bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-icons/font/bootstrap-icons.css';

createApp(App).mount('#app')
```

En este punto, ya debería ser posible utilizar bootstrap-icons dentro del proyecto.


# Instalar jQuery
Para instalar jquery habría que seguir unos pasos similares. En primer lugar, utilizando npm instalamos el paquete.

```shell
npm install jquery

up to date, audited 947 packages in 1s

104 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```

Para poder utilizalo, tendremos que añadir a nuetro fichero main.js la siguiente sentencia:

```js
import { createApp } from 'vue'
import App from './App.vue'

// Importa boostrap
import "bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';

// Importa jquery
window.$ = window.jQuery = require('jquery');

createApp(App).mount('#app')
```

Si no obtenemos errores, tendremos ya instalado jquery y podremos utilizarlo en nuestro proyecto con normalidad.


