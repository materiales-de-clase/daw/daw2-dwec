# Instalaciones
A lo largo de la unidad de trabajo serán necesarias las herramientas que se indican en las siguientes secciones. Algunas de ellas serán obligatorias y otras opcionales. En todo caso, es imrescincible asegurarse de que se tienen todas las herramientas necesarias para trabajar instaladas antes de pasar a la siguiente sección.

## Programas 
Los siguientes programas van a ser imprescindibles para seguir las clases.

- [Node.js](https://nodejs.org/es/): Vamos a utilizar node para gestionar los proyectos en Vue.
- [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur): Extensión de Vue para VS Code.
- [Postman](https://www.postman.com/): Programa que vamos a utilizar para hacer pruebas de llamadas a nuestros endpoints. 
- [Google Chrome](https://www.google.com/intl/es/chrome/?brand=YTUH&gclid=Cj0KCQiA1ZGcBhCoARIsAGQ0kkrSgp_ZaqrkVCpnbu9fp-ylzGIvvnueBqCXS2jqJSw5y96M39tNETsaAiX-EALw_wcB&gclsrc=aw.ds)


## Instalación de Vue CLI
Para trabajar con Vue, será imprescindible instalar Vue CLI, ya que es lo que vamos a utilizar para gestionar nuestros proyectos. Para instalarlo sigue los siguientes pasos:

1. **Abre un terminal como administrador**. En linux puedes ejecutar los comandos con sudo
2. **Ejecuta el siguiente comando**
```shell
$ npm install -g @vue/cli

npm WARN deprecated source-map-url@0.4.1: See https://github.com/lydell/source-map-url#deprecated
npm WARN deprecated urix@0.1.0: Please see https://github.com/lydell/urix#deprecated
npm WARN deprecated source-map-resolve@0.5.3: See https://github.com/lydell/source-map-resolve#deprecated
npm WARN deprecated resolve-url@0.2.1: https://github.com/lydell/resolve-url#deprecated
npm WARN deprecated apollo-server-errors@3.3.1: The `apollo-server-errors` package is part of Apollo Server v2 and v3, which are now deprecated (end-of-life October 22nd 2023). This package's functionality is now found in the `@apollo/server` package. See https://www.apollographql.com/docs/apollo-server/previous-versions/ for more details.
npm WARN deprecated apollo-server-plugin-base@3.7.1: The `apollo-server-plugin-base` package is part of Apollo Server v2 and v3, which are now deprecated (end-of-life October 22nd 2023). This package's functionality is now found in the `@apollo/server` package. See https://www.apollographql.com/docs/apollo-server/previous-versions/ for more details.
npm WARN deprecated apollo-datasource@3.3.2: The `apollo-datasource` package is part of Apollo Server v2 and v3, which are now deprecated (end-of-life October 22nd 2023). See https://www.apollographql.com/docs/apollo-server/previous-versions/ for more details.
npm WARN deprecated apollo-server-types@3.7.1: The `apollo-server-types` package is part of Apollo Server v2 and v3, which are now deprecated (end-of-life October 22nd 2023). This package's functionality is now found in the `@apollo/server` package. See https://www.apollographql.com/docs/apollo-server/previous-versions/ for more details.
npm WARN deprecated apollo-reporting-protobuf@3.3.3: The `apollo-reporting-protobuf` package is part of Apollo Server v2 and v3, which are now deprecated (end-of-life October 22nd 2023). This package's functionality is now found in the `@apollo/usage-reporting-protobuf` package. See https://www.apollographql.com/docs/apollo-server/previous-versions/ for more details.
npm WARN deprecated apollo-server-env@4.2.1: The `apollo-server-env` package is part of Apollo Server v2 and v3, which are now deprecated (end-of-life October 22nd 2023). This package's functionality is now found in the `@apollo/utils.fetcher` package. See https://www.apollographql.com/docs/apollo-server/previous-versions/ for more details.
npm WARN deprecated apollo-server-express@3.11.1: The `apollo-server-express` package is part of Apollo Server v2 and v3, which are now deprecated (end-of-life October 22nd 2023). This package's functionality is now found in the `@apollo/server` package. See https://www.apollographql.com/docs/apollo-server/previous-versions/ for more details.
npm WARN deprecated subscriptions-transport-ws@0.11.0: The `subscriptions-transport-ws` package is no longer maintained. We recommend you use `graphql-ws` instead. For help migrating Apollo software to `graphql-ws`, see https://www.apollographql.com/docs/apollo-server/data/subscriptions/#switching-from-subscriptions-transport-ws    For general help using `graphql-ws`, see https://github.com/enisdenjo/graphql-ws/blob/master/README.md
npm WARN deprecated apollo-server-core@3.11.1: The `apollo-server-core` package is part of Apollo Server v2 and v3, which are now deprecated (end-of-life October 22nd 2023). This package's functionality is now found in the `@apollo/server` package. See https://www.apollographql.com/docs/apollo-server/previous-versions/ for more details.

added 849 packages, and audited 850 packages in 41s

64 packages are looking for funding
  run `npm fund` for details

4 vulnerabilities (2 moderate, 2 high)

To address all issues (including breaking changes), run:
  npm audit fix --force

Run `npm audit` for details.
```

3. Para **comprobar si está correctamente instalado**, podemos ejecutar la siguiente secuencia:

```shell
$ vue --version
@vue/cli 5.0.8
```

## Extensiones de Chrome
- [Vue Developer Tools](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd?hl=en): No soporta todas las nuevas opciones.
- [Vue Developer Tools Beta](https://chrome.google.com/webstore/detail/vuejs-devtools/ljjemllljcmogpfapbkkighbhhppjdbg?hl=en): Para las últimas versiones de Vue.
- [Json Viewer Awesome](https://chrome.google.com/webstore/detail/json-viewer-pro/eifflpmocdbdmepbjaopkkhbfmdgijcc): 
