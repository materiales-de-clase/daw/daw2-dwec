# Proyecto
Deberás implementar un proyecto que cubra todas las tareas anteriores. 

Sugerencias:

- Gestor de contactos
- Gestor de contraseñas
- Lista de la compra

## Enunciado
Propón un proyecto que incluya todo lo que hemos hecho en tareas anteriores. El proyecto debe cumplir con lo siguiente:

- La aplicación debe tener un backend php o que utilice cualquiera de las tecnologías empleadas en servidor. Esta parte no influirá en la calificación.
- Debe tratarse de una aplicación completa.
- Debe existir inicio de sesión por usuario/contraseña
- Debemos tener una gestión principal que conste de una o varias tablas. No es necesario complicarlo mucho, pero que haya varios campos para que de juego con las validaciones y con cargar datos desde la base de datos. Como entidad principal podemos usar lo que queramos.
- Los formularios deben validar los datos de entrada.
- Se debe implementar la gestión de las tablas relacionadas con la principal. 
- Se debe implementar gestión de usuarios.
- Se debe configurar con variables de entorno
- Se debe implementar algún componente
- En general deberás meter todo lo que vamos a dar en clase.

## Agrupamientos
Vamos a tener equipos de 3-4 alumnos. Cada equipo va a tener un capitán. Los capitanes van a ser los alumnos seleccionados para empresa. Van a tener que hacer una defensa del proyecto para verificar la participación. Todos deben controlar.

## Entrega
La entrega de la tarea se hará por moodle por la cuenta del capitán.

- daw-vue-nombre-proyecto-nombre-grupo.7z

El archivo con la entrega contendrá lo siguiente:

- INTEGRANTES.txt con los nombres de los integrantes del grupo.
- Proyecto comprimido y descarga de la base de datos.
- Proyecto generado para desplegar.
- PDF donde se incluyan un listado con los ficheros y una breve explicación del contenido y la finalidad de cada uno.
- Video explicativo donde se vea el proyecto en acción y todo lo que se ha implementado.
