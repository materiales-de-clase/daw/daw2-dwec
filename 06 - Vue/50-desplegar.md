# Despliegue
En el despliegue hay únicamente aquello que la aplicación necesita. Esto quiere decir que para hacer el despliegue es necesario hacer la construcción de nuestra aplicación para despliegue en producción. Una vez que tengamos nuestra build podemos desplegarlo en cualquier sistema. Básicamente vamos a obtener una aplicación web de modo que podemos desplegarlo en cualquier servidor de internet donde tengamos acceso ftp.

## Generar build de producción
Antes de generar el build a producción, debemos verificar que funciona. Esto lo podemos comprobar por ejemplo con el comando *npm run serve*. 

```
Debemos ser cuidadosos y eliminar referencias y elementos que nuestras páginas no estén utilizando. Evita subir código muerto a producción.
```

Una vez tengamos nuestra aplicación lista, para generar nuestro build para producción podemos utilizar la siguiente secuencia de comandos:

```cmd
C:/MIPROYECTO> npm run build

> taskman@0.1.0 build
> vue-cli-service build

All browser targets in the browserslist configuration have supported ES module.
Therefore we don't build two separate bundles for differential loading.


-  Building for production...

 WARNING  Compiled with 3 warnings                                                                                                                                      12:52:55

 warning

asset size limit: The following asset(s) exceed the recommended size limit (244 KiB).
This can impact web performance.
Assets:
  css/chunk-vendors.d23996a2.css (267 KiB)
  js/chunk-vendors.d6ebf05a.js (244 KiB)

 warning

entrypoint size limit: The following entrypoint(s) combined asset size exceeds the recommended limit (244 KiB). This can impact web performance.
Entrypoints:
  app (516 KiB)
      css/chunk-vendors.d23996a2.css
      js/chunk-vendors.d6ebf05a.js
      css/app.b2d625b8.css
      js/app.f2b69a08.js


 warning

webpack performance recommendations:
You can limit the size of your bundles by using import() or require.ensure to lazy load some parts of your application.
For more info visit https://webpack.js.org/guides/code-splitting/

  File                                   Size                                                              Gzipped

  dist\js\chunk-vendors.d6ebf05a.js      244.31 KiB                                                        81.33 KiB
  dist\js\app.f2b69a08.js                4.41 KiB                                                          1.96 KiB
  dist\css\chunk-vendors.d23996a2.css    266.88 KiB                                                        39.33 KiB
  dist\css\app.b2d625b8.css              0.16 KiB                                                          0.14 KiB

  Images and other types of assets omitted.
  Build at: 2022-12-13T11:52:55.730Z - Hash: 285cad8bdd19c711 - Time: 7569ms

 DONE  Build complete. The dist directory is ready to be deployed.
 INFO  Check out deployment instructions at https://cli.vuejs.org/guide/deployment.html
```
Vemos que se han generado varios archivos. Estos archivos se van a encontrar en el directorio dist dentro de nuestro proyecto. Dentro de dicho directorio aparecen los archivos:

- index.html: el punto de entrada a nuestra aplicación
- js: Directorio donde se van a poner los archivos js
- css: donde van a estar los archivos css

Hay archivos que tienen una hash en el nombre. Esto lo hace vue para evitar que los navegarores puedan guardar en caché archivos actualizados. De este modo, ante una nueva versión en que uno de los archivos ha cambiado, se obliga a que el archivo de la nueva versión se cargue de nuevo.

## Desplegarlo
```
Va a ser imprescindible desplegar en un servidor para poder probar 
la aplicación. Si abrimos directamente el index.html no va a funcionar nuestra aplicación.
```
El despliegue de la aplicación va a consistir generalmente en tomar la carpeta dist de nuestra aplicación, donde se encontrarán los archivos HTML, CSS y JS y desplegarla mediante FTP a un servidor de internet. 

Debemos tener en cuenta que lo que estamos desplegando es un frontend de modo que si nuestra aplicación necesitara de un backend tendríamos que desplegar también el backend.

## Proveedores
Lista de proveedores que pueden ser utilizados para desplegar nuestros frontends desarrollados en Angular.

- https://www.netlify.com/


