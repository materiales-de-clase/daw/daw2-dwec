# Crear un proyecto
En este apartado vamos a crear nuestro proyecto utilizando Vue CLI. 

## Antes de comenzar
Antes de comenzar, vamos a verificar que tenemos instalado nuestro cliente de Angular utilizando el siguiente comando.

```batch
C:\MIDIR\> vue --version
@vue/cli 5.0.8
```

## Crea el proyecto
Para crear el proyecto, sitúate en el directorio donde quieres hubicar el código fuente del proyecto y escribe la siguiente secuencia:

```batch
C:\MIDIR\> vue create taskman
?  Your connection to the default npm registry seems to be slow.
   Use https://registry.npmmirror.com for faster installation? No

Vue CLI v5.0.8
? Please pick a preset: Default ([Vue 3] babel, eslint)


Vue CLI v5.0.8
✨  Creating project in C:\Users\Juanjo\RepoRoot\materiales-de-clase\daw\daw2-dwec-profesorado\taskman\04-taskman-php-vue\taskman.
⚙️  Installing CLI plugins. This might take a while...


added 847 packages, and audited 848 packages in 39s

91 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
🚀  Invoking generators...
📦  Installing additional dependencies...


added 96 packages, and audited 944 packages in 9s

102 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
⚓  Running completion hooks...

📄  Generating README.md...

🎉  Successfully created project taskman.
👉  Get started with the following commands:

 $ cd taskman
 $ npm run serve
```

Durante la instalación se han planteado las siguientes cuestiones:

- **Versión de Vue a utilizar** : En este curso vamos a utilizar la versión 3.

Una vez que termine todo el proceso, monta el directorio de la aplicación en tu editor de código.

## Levantar la aplicación
Antes de continuar, es bueno comprobar que nuestra aplicación funciona y somos capaces de acceder a ella a través de la URL. Desde el directorio recién creado vamos a ejecutar el siguiente comando:

```batch
C:\MIDIR\taskman\> npm run serve

  App running at:
  - Local:   http://localhost:8080/
  - Network: http://192.168.1.166:8080/

  Note that the development build is not optimized.
  To create a production build, run npm run build.

```

Este comando ha llevado a cabo las siguientes tareas:

- Monta un servidor mediante webpack

Cuando hagamos cambios a nuestra aplicación se actualizará automáticamente de modo que cuando vayamos a trabajar con angular **tendremos siempre el servidor levantado**.

## Notas sobre el proyecto
### El archivo gitignore
El proyecto se genera con un archivo gitignore que viene configurado para que en caso de que usemos control de versiones con git no subamos archivos que se generan o que son dependencias de nuestro proyecto descargadas de internet. No se recomendaría eliminar nada salvo que se esté seguro de qué se está haciendo, pero sí se puede añadir algún recurso a ignorar si se considera necesario, aunque en nuestro caso no vamos a trabajar con este archivo. 

### El directorio node_modules
Contendrá todos los paquetes instalados con npm de forma automática al crear el proyecto o posteriormente por nosotros si lo hemos necesitado. Este directorio no se sube al repositorio git, por lo tanto, cuando hagamos un clone del repositorio necesitaremos instalar las dependencias en la nueva réplica. Para descargar las dependencias podemos ejecutar el siguiente comando:

```
$ npm install
```

Al ejecutarlo desde el directorio del proyecto se instalarán en ese equipo todas las dependencias necesarias para hacer funcionar nuestro proyecto.

