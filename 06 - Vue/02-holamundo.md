# Hola Mundo
Como se ha explicado anteriormente, Vue no requiere que nuestra aplicación esté desarrollada íntegramente en Vue, sino que permite que desarrollemos componentes o partes de la aplicación con Vue de modo que estas partes/componentes se integren con el resto de nuestra aplicación web. En este apartado vamos a tratar algunos conceptos sencillos de Vue importando directamnete Vue a nuetra página html. 

El código de los ejemplos se encuentra aquí:

- [ejemplo.html](recursos/01-ejemplos.html)
- [ejemplo.js](recursos/01-ejemplos.js)

