# La directiva v-for
Esta directiva permite renderizar una lista de elementos a partir de un array. En el siguiente enlace tenemos disponible la documentación de esta directiva:

https://vuejs.org/guide/essentials/list.html

Vamos a trabajarla con los siguientes ejemplos:

- [v-for.html](recursos/02-ejemplo-v-for.html)
- [v-for.js](recursos/02-ejemplo-v-for.js)