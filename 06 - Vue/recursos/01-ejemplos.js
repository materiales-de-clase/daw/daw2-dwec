
//-----------------------------------------------------------------
// Ejemplo 1: App utilizando el atributo template
//-----------------------------------------------------------------

// Vamos a trabajar en este archivo con el objeto Vue que declara el framework 
// este objeto da acceso a las funcionalidades del framework
// Con el método createApp creamos la aplicación suministrando la descripción de la misma.
// https://vuejs.org/guide/essentials/application.html
const app1 = Vue.createApp({

    // Plantilla html del componente
    template: `
        <h1>Hola mundo</h1>        
        <p> {{ "Vue "+3 }} </p> <!-- Expresión de JavaScript válida -->
    `
});

// Renderiza el componente dentro del elemento HTML que le hemos indicado
app1.mount('#app1');


//-----------------------------------------------------------------
// Ejemplo 2: App escribiendo el html en línea
//-----------------------------------------------------------------
// https://vuejs.org/guide/essentials/application.html
const app2 = Vue.createApp({
});

// Renderiza el componente dentro del elemento HTML que le hemos indicado
app2.mount('#app2');


//-----------------------------------------------------------------
// Ejemplo 3: Ejemplo de componente con estado
//-----------------------------------------------------------------
// https://vuejs.org/guide/essentials/application.html
const app3 = Vue.createApp({
    
    // Definimos los datos que nuestro componente va a manejar. Es una función.
    data() {

        // Devuelvo un objeto con las propiedades de mi objeto. Este objeto es reactivo
        // de modo que cualquier cambio en las propiedades va a provocar que Vue lo detecte
        // y actualice adecuadamente la plantilla.
        return {
            // Estoy crendo 
            mensaje: 'Hola mundo Vue',
            version: 3
        }
    }
});

// Renderiza el componente dentro del elemento HTML que le hemos indicado
app3.mount('#app3');


//-----------------------------------------------------------------
// Ejemplo 4: Eventos
//-----------------------------------------------------------------
// https://vuejs.org/guide/essentials/application.html
const app4 = Vue.createApp({
    
    // Definimos los datos que nuestro componente va a manejar. Es una función.
    data() {

        // Devuelvo un objeto con las propiedades de mi objeto. Este objeto es reactivo
        // de modo que cualquier cambio en las propiedades va a provocar que Vue lo detecte
        // y actualice adecuadamente la plantilla.
        return {
            // Estoy crendo 
            mensaje: 'Hola mundo Vue',
            version: 3
        }
    },

    // En esta opción definimos aquí los métodos en nuestro componente
    methods: {

        // Incrementa la versión sumando el parámetro
        incrementarVersion(evento, incremento) {
            
            // Muestro el evento
            console.log(evento);

            // Muestra el parámetro
            console.log(incremento);

            // Para referenciar a los datos es necesario utilizar el this. Con this
            // podemos acceder a cada uno de los datos en este objeto
            // Al cambiar aquí la versión, se refresca automáticamente la pantalla
            this.version += incremento;

            // Puedo invocar a otros métodos definidos en esta opción
            if(this.version > 10) {
                console.log('Se ha superado la versión máxima');
                this.reiniciarVersion();
            }
        },

        // Defino otro método
        reiniciarVersion() {
            this.version = 3;
        }
    }
});

// Renderiza el componente dentro del elemento HTML que le hemos indicado
app4.mount('#app4');
