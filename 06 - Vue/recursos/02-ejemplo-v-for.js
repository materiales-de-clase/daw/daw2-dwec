

//-----------------------------------------------------------------
// Ejemplo 4: v-for
//-----------------------------------------------------------------

// Defino un array de objetos que representan personas. Estas personas las vamos a representar posteriormente
// utilizando v-for
const personas = [
    { nombre: 'Manolo' },
    { nombre: 'Paco' }
]

const app = Vue.createApp({
    
    data() {

        return {
            mensaje: 'Hola mundo',
            personas: [] 
        }
    },

    // En esta opción definimos aquí los métodos en nuestro componente
    methods: {

        // Función que obtiene las personas a saludar.
        saludar() {
            // Asigna a este objeto las personas a saludar
            this.personas = personas;            
        },
    }
});

// Renderiza el componente dentro del elemento HTML que le hemos indicado
app.mount('#app');
