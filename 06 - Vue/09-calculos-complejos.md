# Cálculos complejos en Vue
Los componentes deben ser lo más sencillos posibles con la menor cantidad de cálculos posibles en el template. Se deben evitar además los cálculos complejos en esta parte. De modo que Vue para cálculos complejos permite cosas como

https://vuejs.org/guide/essentials/computed.html#computed-properties

<script>
    export default {

        methods: {
            getValorCalculado() {
                // Lo podríamos hacer aquí, pero sigue siendo un cálculo complejo si lo referenciamos desde 
                // la plantilla. Esto no sería recomendado, ya que si se trata de un cálculo pesado podría bloquear la web.
                // lo ideal sería mantener en cache el resultado a no ser que el valor de los parámetros de entrada cambie                
            }
        }
    }
</script>

Para este tipo de cálculos Vue dispone de lo que llama "computed properties". Son propiedades que se calculan a partir de una entrada.

<script>
    export default {

        methods: {
            getValor() {
                // Lo podríamos hacer aquí, pero sigue siendo un cálculo complejo si lo referenciamos desde 
                // la plantilla. Esto no sería recomendado, ya que si se trata de un cálculo pesado podría bloquear la web.
                // lo ideal sería mantener en cache el resultado a no ser que el valor de los parámetros de entrada cambie                
            }
        },

        // Aquí se declaran las propiedades computadas. Estas se guardan en caché de modo que se evita que tengan que recalcularse.
        // Vue detectará cuando las dependencias de las propiedades computadas cambian y solo ejecutará la función para llevar a cabo
        // el cálculo cuando alguna de las dependencias cambie. Incluso si las dependencias cambian y hay que refrescar, solo se va a 
        // invocar una vez.
        computed: {
            getValorCalculado() {
                return micalculo;
            }
        }
    }
</script>
