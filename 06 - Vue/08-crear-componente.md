# Crear un componente
47 mi primer componente

Crear un archivo Hola.vue en directorio src/comnponents. Contendría lo siguiente

```html
<template>
    <h2>Hola</h2>
</template>

<script>
  export default {
  }
</script>

<style>
</style>
```

<!-- Plantilla HTML del componente -->
<template>
  <img alt="Vue logo" src="./assets/logo.png">
  <Hola msg="Welcome to Your Vue.js App"/>
</template>

<!-- Parte JavaScript -->
<script>

// Para referenciar componentes, tengo que importar el archivo. Si en la ruta utilizo @ equitale a src
import Hola from './components/Hola.vue'

// Aquí tengo la declaración del componente. Si tengo estado tengo que definir la función data, etc.
export default {
  name: 'App',

  // Aquí listo los componentes disponibles que voy a utilizar en la plantilla
  components: {
    Hola
  }
}
</script>

<!-- Aquí está la parte de estilo css. Este código corresponde con una aplicación por eso aparece el estilo para #app -->
<style>    
#app {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #2c3e50;
  margin-top: 60px;
}
</style>

