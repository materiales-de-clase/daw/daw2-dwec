# Vue Router
En esta sección vamos a dar los pasos previos necesarios para utilizar Vue router de modo que podamos crear SPAs utilizando Vue.

## Instalación
En primer lugar vamos a ejecutar la siguiente secuencia:

```shell
$ npm install vue-router

added 2 packages, and audited 950 packages in 2s

105 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```

Si ejecutamos npm list podemos ver la versión instalada. Ya tenemos la dependencia necesaria para trabajar con las rutas.

## Referencias

- https://router.vuejs.org/introduction.html

