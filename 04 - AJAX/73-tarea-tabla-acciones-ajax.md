```
Tarea parte de desarrollo paso a paso de aplicación de gestión de tareas
```
# Acciones AJAX
Esta es una tarea en la que vamos a pulir nuestra aplicación añadiendo diversas acciones a través de llamadas ajax que luego verán reflejado su resultado en nuestra web.

Vamos a incluir al menos las siguientes acciones:

- Eliminar elementos de tablas. 
- Cambiar el estado de una tarea

## Enunciado
En clase le echaremos un vistazo a la aplicación para obtener un listado de todas las acciones que se van a implementar y luego:

- Crearemos una lista de tareas que hay que implementar. Para cada acción habrá que revisar qué retroalimentación va a recibir el usuario.
- Implementaremos en diversos ficheros las diferentes APIs. Por ejemplo:
  - crud.js (u otro) para las operaciones crud vía ajax.

## Entrega
La entrega se hará por repositorio y en un archivo comprimido. La tarea debes resolverla en el directorio:

- AJAX/tareas/taskman

El archivo con la entrega contendrá lo siguiente:

- Proyecto comprimido y descarga de la base de datos.
- PDF donde se incluyan los ficheros implicados y el funcionamiento de cada una de las funcionalidades explicado brevemente.
- Video explicativo de las diferentes acciones


