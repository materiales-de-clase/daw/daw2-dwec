# Web Workers
Son un medio sencillo para ejecutar código JavaScript en segundo plano. Hay que tener en cuenta que los web workers trabajan en un contexto global diferente de la ventana. No será posible acceder a objetos como window o modificar el DOM. Para este tipo de tareas, será necesario establecer comunicación mediante paso de mensajes entre nuestra página y los workers. Algunos recursos interesantes.

- [Funciones y clases disponibles para los workers](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Functions_and_classes_available_to_workers)
- Deberemos utilizar la función global [structuredClone](https://developer.mozilla.org/en-US/docs/Web/API/structuredClone) para crear copias de objetos que podamos pasar a nuestros workers.


Ejemplo de código que crea un web worker
```javascript
    // Crea el worker
    worker = new Worker('js/ww_contador.js');
    
    // Asigna el gestor de eventos para cuando el worker envíe un mensaje
    worker.addEventListener('message', function(event) {
        console.log(event.data);

        // El valor pasado en el evento va al contador
        contador.value = event.data;        
    });

    // Pasa un mensaje al worker. 
    worker.postMessage(1);
```

Ejemplo de código en el webworker

```javascript

// Variables que definen el estado del contador
let actual = 0;

// Define una función que va a generar el temporizador
function temporizador() {

    // Incremento el valor del temporizador
    actual++;

    // Envío un mensaje con el valor actual
    postMessage(actual);

    // Hago que se vuelva a llamar al temporizador en 1 segundo
    setTimeout(temporizador, 1000);
}


onmessage = function(e) {
   
    // Toma el valor de los datos
    actual = Number(e.data);

    // Primera llamada a la función
    temporizador();    
}

```
