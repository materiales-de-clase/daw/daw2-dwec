```
Tarea parte de desarrollo paso a paso de aplicación de gestión de tareas
```
# Formulario - Validaciones en servidor
En anteriores actividades hemos creado una biblioteca que permite llevar a cabo validaciones de campos en el cliente. Este tipo de validaciones tiene algunas limitaciones. Por ejemplo:

- No podemos comprobar si un objeto (cliente, empresa, etc) existe en la base de datos sin consultarla.
- No podemos comprobar si hay stock de un determinado producto para satisfacer una petición.

En esta tarea, vamos a extender nuestra biblioteca de validaciones para incluir validaciones de tipo servidor. Así por ejemplo para el campo NIF/CIF de una empresa o persona podemos incluir las siguientes validaciones:

- No está vacío
- Es un NIF/CIF válido
- El CIF existe en la base de datos.

De este modo, podemos hacer una comprobación más completa para que el usuario envíe el formulario únicamente si los datos son correctos. 

## Consideraciones previas
Para esta tarea nos vamos a apoyar en la biblioteca de validaciones que hemos creado previamente y vamos a añadir algunas validaciones de servidor. De momento nos vamos a quedar en validaciones, aunque en tareas posteriores iremos más allá y traeremos datos del servidor al cliente. Antes de proceder con la tarea, vamos a **determinar en clase qué validaciones tendrían que ser implementadas**. Por ejemplo:

- NIF/CIF existe. Es decir, podemos validar en el cliente que el NIF/CIF existe, pero también podemos añadir posteriormente la validación de que el NIF/CIF está registrado en la base de datos.

Esto dependerá de la aplicación que estemos desarrollando.

## Enunciado
Implementa todas las validaciones que hemos definido e integrarlas en tu aplicación para todos los campos que deben aplicarlas.

## Entrega    
La entrega se hará por repositorio y en un archivo comprimido. La tarea debes resolverla en el directorio:

- AJAX/tareas/taskman

El archivo con la entrega contendrá lo siguiente:

- Escribe un pequeño documento pdf (a parte de los comentarios) donde:
  a. enumeres los archivos js implicados y su función.
  b. describe brevemente el proceso de validación que has implementado y funciones implicadas.
- Graba un video breve con audio donde se vea tu aplicación en acción y muestra las diferentes validaciones así como la forma en se configuran los formularios para aplicarlas. Formato mkv/mp4






