```
Tarea parte de desarrollo paso a paso de aplicación de gestión de tareas
```
# Formulario
En esta tarea vamos a crear el formulario para dar de alta o modificar una tarea. Vamos a utilizar bootstrap.

## Enunciado
En este apartado deberás preparar lo siguiente:

- Revisa el formulario facilitado por el profesor e inserta las notas que consideres oportunas.

## Entrega
La entrega se hará por repositorio y en un archivo comprimido. La tarea debes resolverla en el directorio:

- AJAX/tareas/taskman

El archivo con la entrega contendrá lo siguiente:

- Archivos implicados
- PDF donde se listen y expliquen los documentos implicados






