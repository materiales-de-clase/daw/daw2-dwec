```
Tarea parte de desarrollo paso a paso de aplicación de gestión de tareas
```
# Formulario - AJAX alta
En esta tarea vamos a crear el alta de una tarea en el servidor utilizando AJAX. Es decir, no vamos a hacer el submit como tal, sino que vamos a enviar los datos en el formulario utilizando una función JavaScript y enviando los datos utilizando para ello AJAX.

## Enunciado
La tarea consiste en:

- Definir una forma en que puedas identificar los campos a enviar al servidor
- Definir una función que permita enviar el formulario utilizando AJAX. Tendrás que capturar el SUBMIT y evitar que se envíen los datos para, posteriormente, enviar los datos en formato JSON.

## Entrega
La entrega se hará por repositorio y en un archivo comprimido. La tarea debes resolverla en el directorio:

- AJAX/tareas/taskman

El archivo con la entrega contendrá lo siguiente:
- Archivos del proyecto
- Documento PDF donde 
  - enumeres los archivos js implicados y su función.
  - expliques brevemente cómo funciona tu implementación y cuáles son las principales funciones/gestores de eventos implicados en el proceso.
- Graba un video breve con audio donde se vea tu aplicación en acción y explica cómo funcionan de cara al usuario todos los diferentes detalles que has implementado. 
