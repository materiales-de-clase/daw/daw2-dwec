# Servicios
A continuación tienes los enlaces que vas a necesitar para construir la aplicación junto con un ejemplo de objeto de entrada y otro de salida.

## Tareas por título

URL
```url
http://localhost/ajax.php?s=_getTareasPorTitulo&__debug
```

Entrada
```json
{
    "filtro": "%"
}
```

Salida
```json
{
    "ok": "1",
    "mensaje": "",
    "datos": [
        {
            "id_tarea": 16,
            "0": 16,
            "titulo": "Mi tarea",
            "1": "Mi tarea",
            "id_informador": 1,
            "2": 1,
            "informador": "admin",
            "3": "admin",
            "id_asignado": 1,
            "4": 1,
            "asignado": "admin",
            "5": "admin",
            "id_tipo_tarea": 1,
            "6": 1,
            "tipo": "Tarea",
            "7": "Tarea",
            "id_estado": 1,
            "8": 1,
            "estado": "Abierta",
            "9": "Abierta",
            "descripcion": "",
            "10": "",
            "fecha_alta": "0000-00-00 00:00:00",
            "11": "0000-00-00 00:00:00",
            "fecha_vencimiento": "0000-00-00",
            "12": "0000-00-00",
            "hora_vencimiento": "00:00:00",
            "13": "00:00:00"
        }    ]
}
```
## Eliminar tarea

Url
```
http://localhost/ajax.php?s=_deleteTarea&__debug
```

Entrada
```
{
    "id": "1"
}
```

Salida
```
{
    "ok": "0",
    "mensaje": "",
    "datos": ""
}
```

## Usuarios por nombre

Url
```
http://localhost/ajax.php?s=_getSelectUsuariosPorNombre&__debug
```

Entrada
```
{
    "filtro": "%"
}
```

Salida
```
{
    "ok": "1",
    "mensaje": "",
    "datos": [
        {
            "id": 1,
            "0": 1,
            "texto": "admin",
            "1": "admin"
        },
        {
            "id": 2,
            "0": 2,
            "texto": "usuario",
            "1": "usuario"
        }
    ]
}
```

## Tipos de tarea

Url
```
http://localhost/ajax.php?s=_getSelectTiposTarea&__debug
```

Entrada
```
{
    "filtro": "%"
}
```

Salida
```
{
    "ok": "1",
    "mensaje": "",
    "datos": [
        {
            "id": 1,
            "0": 1,
            "texto": "Tarea",
            "1": "Tarea"
        },
        {
            "id": 2,
            "0": 2,
            "texto": "Recordatorio",
            "1": "Recordatorio"
        }
    ]
}
```

## Estados por tipo de tarea

Url
```
http://localhost/ajax.php?s=_getSelectEstadosTareaPorTipo&__debug
```

Entrada
```
{
    "filtro": "1"
}
```

Salida
```
{
    "ok": "1",
    "mensaje": "",
    "datos": [
        {
            "id": 1,
            "0": 1,
            "texto": "Abierta",
            "1": "Abierta"
        },
        {
            "id": 2,
            "0": 2,
            "texto": "En progreso",
            "1": "En progreso"
        },
        {
            "id": 3,
            "0": 3,
            "texto": "Cerrada",
            "1": "Cerrada"
        }
    ]
}
```

## Etiquetas por nombre

Url
```
http://localhost/ajax.php?s=_getSelectEtiquetasPorNombre&__debug
```

Entrada
```
{
    "filtro": "%"
}
```

Salida
```
{
    "ok": "1",
    "mensaje": "",
    "datos": [
        {
            "id": 28,
            "0": 28,
            "texto": "adas",
            "1": "adas"
        }        
    ]
}
```

## Resumen de tareas por estado

Url
```
http://localhost/ajax.php?s=_getResumenTareasPorEstado&__debug
```

Entrada
```
NADA
```

Salida
```
{
    "ok": "1",
    "mensaje": "",
    "datos": [
        {
            "contador": 7,
            "0": 7,
            "estado": "Abierta",
            "1": "Abierta"
        },
        {
            "contador": 1,
            "0": 1,
            "estado": "Abierto",
            "1": "Abierto"
        },
        {
            "contador": 1,
            "0": 1,
            "estado": "En progreso",
            "1": "En progreso"
        }
    ]
}
```

## Añadir tarea

Url
```
http://localhost/ajax.php?s=_addTarea&__debug
```

Entrada
```
{
    "titulo":"asdas",
    "informador":"usuario",
    "informadorId":"2",
    "asignado":"1",
    "tipo":"1",
    "estado":"1",
    "fechaalta":"",
    "fechavencimiento":"",
    "horavencimiento":"",
    "etiqueta":"",
    "descripcion":""
}
```

Salida
```
{
    "ok": "1",
    "mensaje": "",
    "datos": {
        "id": "32",
        "titulo": "asdas",
        "informadorId": "2",
        "asignado": "1",
        "tipo": "1",
        "estado": "1",
        "descripcion": "",
        "fechaalta": "",
        "fechavencimiento": "",
        "horavencimiento": ""
    }
}
```

## Actualizar tarea

Url
```
http://localhost/ajax.php?s=_setTarea&__debug
```

Entrada
```
{
    "tareaId":"1",
    "titulo":"asdas",
    "informador":"usuario",
    "informadorId":"2",
    "asignado":"1",
    "tipo":"1",
    "estado":"1",
    "fechaalta":"",
    "fechavencimiento":"",
    "horavencimiento":"",
    "etiqueta":"",
    "descripcion":""
}
```

Salida
```
{
    "ok": "1",
    "mensaje": "",
    "datos": {
        "id": "1",
        "titulo": "asdas",
        "informadorId": "2",
        "asignado": "1",
        "tipo": "1",
        "estado": "1",
        "descripcion": "",
        "fechaalta": "",
        "fechavencimiento": "",
        "horavencimiento": ""
    }
}
```

## Get tarea por id

Url
```
http://localhost/ajax.php?s=_getTarea&__debug
```

Entrada
```
{
    "id":"16"
}
```

Salida
```

    "ok": "1",
    "mensaje": "",
    "datos": {
        "id_tarea": 16,
        "0": 16,
        "titulo": "Mi tarea",
        "1": "Mi tarea",
        "id_informador": 1,
        "2": 1,
        "informador": "admin",
        "3": "admin",
        "id_asignado": 1,
        "4": 1,
        "asignado": "admin",
        "5": "admin",
        "id_tipo_tarea": 1,
        "6": 1,
        "tipo": "Tarea",
        "7": "Tarea",
        "id_estado": 1,
        "8": 1,
        "estado": "Abierta",
        "9": "Abierta",
        "descripcion": "",
        "10": "",
        "fecha_alta": "0000-00-00 00:00:00",
        "11": "0000-00-00 00:00:00",
        "fecha_vencimiento": "0000-00-00",
        "12": "0000-00-00",
        "hora_vencimiento": "00:00:00",
        "13": "00:00:00"
    }
}
```