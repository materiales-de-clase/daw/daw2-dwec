----------------------------------------------------------------
-- Crea la base de datos
----------------------------------------------------------------
create database taskman;

----------------------------------------------------------------
-- Crea la tabla de usuarios
----------------------------------------------------------------
create table usuarios (

    id_usuario      int not null auto_increment,
    usuario         varchar(16) not null,
    password        varchar(32) not null,
    nombre_completo varchar(200),

    primary key (id_usuario),
    constraint usuarios_uq_usuario unique (usuario)
);

insert into table usuarios (usuario, password, nombre_completo) values ('admin', '1234', 'Administrador');


----------------------------------------------------------------
-- Crea la tabla de tareas
----------------------------------------------------------------
create table tareas (

    id_tarea    int not null auto_increment,
    titulo          varchar(100) not null,
    -- Revisar la tarea que tengo puesta para validar los campos que tengo que insertar

    primary key (id_tarea)
);

----------------------------------------------------------------
-- Comentarios
--
-- Comentarios asociados a la tarea. Cada comentario
-- está asociado a una tarea y un usuario.
----------------------------------------------------------------
create table comentarios (
);

----------------------------------------------------------------
-- Tabla de etiquetas
--
-- Las etiquetas pueden ser asociadas a las tareas
-- Esta es la tabla de etiquetas que han sido utilizadas
-- anteriormente.
----------------------------------------------------------------
create table etiquetas (
    
    id_etiqueta     int not null auto_increment,
    nombre          varchar(100) not null,

    primary key (id_etiqueta)
);

