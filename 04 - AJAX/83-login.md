```
Tarea parte de desarrollo paso a paso de aplicación de gestión de tareas
```
# Login
En esta tarea vamos a implementar la pantalla de inicio de sesión. 

## Enunciado
Implementa la pantalla login.php que acepte:

- Usuario
- Contraseña

En caso de éxito, se inicia sesión y se va al index. Si falla el inicio de sesión se muestra un error y se solicitan de nuevo los datos.

## Entrega
La entrega se hará por repositorio y en un archivo comprimido. La tarea debes resolverla en el directorio:

- AJAX/tareas/taskman

El archivo con la entrega contendrá lo siguiente:

- Proyecto comprimido y descarga de la base de datos.
- PDF donde se incluyan los ficheros implicados y el funcionamiento de cada una de las funcionalidades explicado brevemente.
- Video explicativo de las diferentes acciones



