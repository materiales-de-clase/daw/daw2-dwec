# Simon
El objetivo de esta tarea va a ser poner en práctica diversos contenidos relacionados con el DOM, CSS/SASS y ejecución de tareas de forma asíncrona. Lo primero, el tablero de juego tendrá este aspecto:

![SIMON](recursos/simon.png)

Podemos distinguir los siguientes elementos:
- Botones verde, rojo, amarillo, azul
- Botón para iniciar la partida. Se utilizará además para mostrar el estado - del juego. Ten en cuenta esto a la hora de escoger los tamaños de fuente y botones.


## Apartado 1. Diseño del tablero de juego
Antes de implementar el juego, será necesario diseñar el tablero de juego. Para diseñarlo ten en cuenta que:

- Los botones pueden ser simples div con bordes redondeados de modo que den la forma que aparece en pantalla.
- El botón central lo puedes colocar ahí jugando con el atributo position. Mira esta página para obtener más información. https://developer.mozilla.org/es/docs/Web/CSS/position
- El tablero debe estar centrado horizontal y verticalmente.

En este apartado deberás utilizar SASS. Deberás prestar atención a que tu fichero fuente contemple al menos:

- Colores de los botones “apagados” como variables.
- Colores de los botones con hover derivados con alguna de las funciones de SASS a partir de los colores de los botones apagados.
- Se tendrá que definir un tamaño de fuente en el documento. Este tamaño de fuente se utilizará como referencia para el tamaño del resto de elementos, es decir, se utilizarán medidas relativas.
- El tamaño del tablero y los botones se definirán utilizando unidades relativas a partir del tamaño de la fuente.
- El tamaño del tablero será una constante.
- El tamaño de botones y resto de elementos se calculará a partir del tamaño del tablero.
- Debe haber variables para las fuentes utilizadas y los tamaños.

## Apartado 2. Implementación de la partida
Lo primero, recuerda que la lógica del juego va a ir dirigida por eventos. Al diseñar la partida se tendrán en cuenta las siguientes pautas u orientaciones:

1. El botón central es también la pantalla de juego que va a mostrar lo que va pasando y va a permitir iniciar/finalizar la partida. Funcionará del siguiente modo:
   
    a. Al inicio invita a pulsarlo para iniciar la partida.
    b. Al pulsar start, se inicia la secuencia con el primer nivel.
    c. Antes de iniciar la reproducción de una secuencia para el usuario, mostrará el nivel en el que se encuentra el usuario durante unos segundos.
    d. Cuando se está reproduciendo una secuencia, lo indicará y no permitirá interacción con el usuario en ninguno de los botones.
    e. Cuando la secuencia termina, indica al usuario que tiene que introducir la suya.
    f. En caso de fallo del usuario, lo indicará con algún mensaje visual. Deberá mostrar el error unos segundos y volver a invitar a pulsar el botón iniciar partida.
    g. En caso de victoria, se incrementará el nivel en 1 y vuelta a empezar.

2. Los botones de colores permitirán, una vez el juego ha mostrado su secuencia, introducir la secuencia de usuario. Tener en cuenta que cuando el juego está mostrando la secuencia de colores, los botones no deben aceptar entrada del usuario.

Ten en cuenta, que al mostrar varios mensajes consecutivos es posible que tengas que hacer una pausa para que el usuario pueda leerlos. 

Sobre la implementación:

- Para la implementación será necesario utilizar la función setTimeout y objetos Promise
- Ten en cuenta que hay una separación clara entre la parte donde se muestra la secuencia, que debe hacerse en segundo plano utilizando setTimeouu y promise
- y la parte donde el usuario introduce su respuesta, que va a ser guiada totalmente por eventos. Esto deberá controlarse utilizando variables de estado de modo que el programa sepa en todo momento en qué etado se encuenta.

## Apartado 3. Mientras que se muestra el start
Como complemento, cuando el juego esté detenido, intenta que los colores de los botones roten o cambien aleatoriamente entre ellos. Al pulsar start el tablero volverá al estado inicial.

## Entrega
Se entregará un zip (daw2-apellidos-nombre.zip) que contenga:

- html
- js
- css
- scss





