# JSON
JSON es un formato ligero de intercambio de datos. Es fácil de leer y escribir para las personas y para las máquinas. En JavaScript disponemos del objeto global JSON para serializar/deserializar objetos en formato JSON. Aquí disponemos de más información:

- https://devdocs.io/javascript/global_objects/json
- https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON

Aquí tenéis un artículo para trabajar con JSON.

- https://developer.mozilla.org/es/docs/Learn/JavaScript/Objects/JSON




