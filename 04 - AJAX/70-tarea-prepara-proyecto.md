```
Tarea parte de desarrollo paso a paso de aplicación de gestión de tareas
```
# Preparar proyecto
En esta tarea vamos a preparar nuestro proyecto para trabajar.

## Enunciado
En este apartado deberás preparar lo siguiente:

- Crear el esquema de base de datos utilizando el script facilitado.
- Preparar el esqueleto de la página web facilitado de modo que pueda ser utilizado como base.

## Entrega
La entrega se hará por repositorio y en un archivo comprimido. La tarea debes resolverla en el directorio:

- AJAX/tareas/taskman

El archivo con la entrega contendrá lo siguiente:

- Proyecto comprimido



