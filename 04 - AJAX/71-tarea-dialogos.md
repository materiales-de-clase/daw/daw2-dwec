```
Tarea parte de desarrollo paso a paso de aplicación de gestión de tareas
```
# Dialogos
En esta tarea vamos a crear una pequeña biblioteca para mostrar de forma sencilla cuadros de diálogos bootstrap simples en nuestra aplicación.

## Enunciado
Vamos a implementar los siguientes tipos de cuadros de diálogo

- Mostrar un mensaje
- Pedir confirmación para hacer algo

## Entrega
La entrega se hará por repositorio y en un archivo comprimido. La tarea debes resolverla en el directorio:

- AJAX/tareas/taskman

El archivo con la entrega contendrá lo siguiente:

- Documento PDF que explique los ficheros relacionados con la tarea y lo que son.
- Video donde se muestren los diálogos en acción.





