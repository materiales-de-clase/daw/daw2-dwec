```
Tarea parte de desarrollo paso a paso de aplicación de gestión de tareas
```
# Dashboard
Vamos ahora a crear algunos gráficos para mostrar información de la aplicación en forma bonita.

## Enunciado
Vamos a incluir en el dashboard al menos los siguientes gráficos:

- Tareas por estado
- Tareas por usuario

## Entrega    
La entrega se hará por repositorio y en un archivo comprimido. La tarea debes resolverla en el directorio:

- AJAX/tareas/taskman

El archivo con la entrega contendrá lo siguiente:

- Escribe un pequeño documento pdf (a parte de los comentarios) donde:
  a. enumeres los archivos js implicados y su función.
  b. describe brevemente el proceso de validación que has implementado y funciones implicadas.
- Graba un video breve con audio donde se vea tu aplicación en acción y muestra las diferentes validaciones así como la forma en se configuran los formularios para aplicarlas. Formato mkv/mp4






