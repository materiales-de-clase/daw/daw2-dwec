```
Tarea parte de desarrollo paso a paso de aplicación de gestión de tareas
```
# Formulario - AJAX edit
En esta tarea, el objetivo es hacer la modificación de una tarea previamente registrada.

## Enunciado
La tarea consiste en:

- Definir el modo en que se lanza la modificación de una tarea e implementarla.
- Definir cómo puedes enviar los datos al servidor de modo que se sepa que estamos hablando de modificación.
- Hacer el envío al servidor de los datos de modo que se actualice la tarea.

## Entrega
La entrega se hará por repositorio y en un archivo comprimido. La tarea debes resolverla en el directorio:

- AJAX/tareas/taskman

El archivo con la entrega contendrá lo siguiente:
- Archivos del proyecto
- Documento PDF donde 
  - enumeres los archivos js implicados y su función.
  - expliques brevemente cómo funciona tu implementación y cuáles son las principales funciones/gestores de eventos implicados en el proceso.
- Graba un video breve con audio donde se vea tu aplicación en acción y explica cómo funcionan de cara al usuario todos los diferentes detalles que has implementado. 
