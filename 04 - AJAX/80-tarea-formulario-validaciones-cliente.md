```
Tarea parte de desarrollo paso a paso de aplicación de gestión de tareas
```
# Formulario - Validaciones en cliente
En esta actividad vamos a implementar las validaciones en cliente de nuestros formularios. Para ello, vamos a partir de la aplicación de contactos que estamos desarrollando en PHP. Nuestro objetivo va a ser dotar a todos los formularios en la aplicación de validaciones adecuadas que permitan evitar que se envíen datos no válidos al servidor.

## Consideraciones previas
Para la correcta realización de la actividad será necesario llevar a cabo un análisis previo donde recojamos los requisitos de nuestra aplicación. Esto lo haremos en clase de modo que todos definamos los mismos objetivos. En nuestro caso vamos a incluir al menos las siguientes tareas:

- Determinación de las validaciones que debemos implementar a lo largo de la aplicación. Ejemplos:
  - Formato de DNI
  - Cadenas no vacías
  - Cadenas de una mínima longitud
  - Cadenas alfabéticas
  - Cadenas alfabéticas
  - ...
- Un campo puede tener más de una validación aplicada de modo que debes permitir que se puedan aplicar varias validaciones a un campo.
- Definir en qué momento se van a llevar a cabo las validaciones. Hay dos opciones:
  - A medida que introducimos los datos en el formulario.
  - Antes de enviar el formulario al servidor.
- La forma en que se muestran los errores al usuario de modo que sepa qué está fallando. Son varias las acciones que se pueden llevar a cabo:
  - Marcar los campos erróneos
  - Añadir algún tipo de mensaje con el error en alguna parte del formulario o, en casos muy concretos, un popup.

También deberíamos analizar el modo en que vamos a llevar a cabo la implementación en nuestra aplicación. Esto es algo que se debe diseñar antes de implementarlo. En nuestro caso, vamos a partir de una implementación similar a la que hicimos en clase, de modo que, tendremos que tener en cuenta que:

- Nuestra biblioteca de control de formulario va a encontrarse en uno o varios archivos js que deberemos incluir en las páginas web de nuestra aplicación. La biblioteca en sí, no debe ser modificada para adaptarse a cada una de las páginas.
- Deberás definir de algún modo como indicarle a la biblioteca las validaciones a aplicar sobre cada uno de los campos. Por ejemplo, indicar que un campo contiene un DNI.
- En ningún momento debe incluirse JavaScript en las páginas HTML más allá de incluir los archivos js. Todo el código de inicialización y gestión de eventos tiene que estar en los JS adjuntos. 
- Debe haber unos pasos claros a seguir para utilizar nuestra biblioteca JS en un determinado formulario. Ten en cuenta que puede haber más de un formulario en un html.

## Enunciado
La tarea consistirá en:

- Crear una biblioteca de validaciones en JavaScript que:
  - Soporte todas las validaciones que hemos determinado necesarias en clase.
  - Implemente las validaciones en el momento de edición y al enviar el formulario.
  - Muestre los errores con mensajes y marcando el campo como erróneo.
- Utilizarla en todos los formularios de tu ERP

## Otras consideraciones
Hay que tener en cuenta que la información de colores, fuentes, etc asociados a los errores deberá estar en la guía de estilos que vamos a definir para el sitio.

## Entrega
La entrega se hará por repositorio y en un archivo comprimido. La tarea debes resolverla en el directorio:

- AJAX/tareas/taskman

El archivo con la entrega contendrá lo siguiente:

- Escribe un pequeño documento dam2-apellidos-nombre.pdf (a parte de los comentarios) donde:
  - enumeres los archivos js implicados y su función.
  - expliques brevemente cómo funciona tu implementación y cuáles son las principales funciones/gestores de eventos implicados en el proceso.
  - Expliques cómo asocias campos a las validaciones que les corresponden.
  - Indiques cómo se muestran los errores.

- Graba un video breve con audio donde se vea tu aplicación en acción y explica cómo funcionan de cara al usuario todos los diferentes detalles que has implementado. Ejemplo: como se muestran los errores, diferentes formularios, diferentes validaciones, etc. Incluye también en el video una breve explicación de cómo se configuran los formularios para aplicar las diferentes validaciones. Formato mkv/mp4

 






