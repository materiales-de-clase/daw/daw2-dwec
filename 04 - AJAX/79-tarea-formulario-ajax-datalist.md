```
Tarea parte de desarrollo paso a paso de aplicación de gestión de tareas
```
# Formulario - AJAX DataList
La tarea consistirá en la carga de forma asíncrona de datos de formularios. 
En este caso, vamos a implementar un datalist que permita buscar la opción que queremos seleccionar.

## Enunciado
En esta tarea vamos a:

- Crea una biblioteca JS que permite crear un datalist que se cargue desde la base de datos con unos u otros valores dependiendo de la entrada del usuario utilizando AJAX.

## Entrega
La entrega se hará por repositorio y en un archivo comprimido. La tarea debes resolverla en el directorio:

- AJAX/tareas/taskman

El archivo con la entrega contendrá lo siguiente:
- Documento PDF donde 
  - enumeres los archivos js implicados y su función.
  - expliques brevemente cómo funciona tu implementación y cuáles son las principales funciones/gestores de eventos implicados en el proceso.
- Graba un video breve con audio donde se vea tu aplicación en acción y explica cómo funcionan de cara al usuario todos los diferentes detalles que has implementado. 





