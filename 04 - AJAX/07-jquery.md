# jQuery
jQuery es una biblioteca JavaScript enfocada a hacernos a vida más fácil cuando tenemos que llevar a cabo determinados tipos de desarrollos. Aunque estudiar la biblioteca escapa al alcance de esta unidad de trabajo, me gustaría que por lo menos os suene e intentemos utilizar alguna de las funciones que incluye.

Aquí tenéis algunos enlaces de interés:

- https://jquery.com/ La página principal de jquery
- https://api.jquery.com/ Documentación del API. Es una referencia que merece la pena que por lo menos conozcáis. Ahí tenéis todas las funciones implementadas en jQuery

## Ajax con jQuery
Podemos implementar algunas de las operaciones que hemos estado realizando utilizando para ello jQuery. Esto es algo que podemos hacer utilizando algunas de las funciones ofrecidas. Merece la pena echar un vistazo a las funciones en la sección Ajax:

- [Helper Functions](https://api.jquery.com/category/ajax/helper-functions/)
- [Low-Level Interface](https://api.jquery.com/category/ajax/low-level-interface/)
- [Shorthand Methods](https://api.jquery.com/jQuery.getJSON/)

Nosotros vamos a probar a utilizar la función ajax para hacer una llamada del mismo modo que hemos hecho con XmlHttpRequest y Fetch para de algún modo trabajar con la biblioteca.

### Petición sin parámetros
En este caso vamos a utilizar la función de bajo nivel ajax. Lo normal sería utilizar la función de alto nivel que está diseñada para el tipo de petición que vamos a hacer.

```javascript
    // Asigna el evento click a la funcion callback
    $("button").click(function(){
        $.ajax({

            // La url
            url: "http://...", 

            // Función que se invoca cuando el resultado está ok
            success: function(result){
                $("#div1").html(result);
            }
        });
    });
```

### Hace un post
En este ejemplo, se hace una petición de tipo post enviando 

```javascript
    $.post( 
        "http://...",       

        // Este objeto son los parámetros de entrada.  ¿En qué formato se está enviando?
        { nombre: 'Paco', apellido1: 'R%' },

        function( data ) {
            // Mete en el elemento con clae result la respuesta recibida del servidor
            $( ".result" ).html( data );
        },
        // 'json', 'html', ...
    );
```

### Carga en un elemento algo que está en el servidor
En este ejemplo vemos como podemos descargar fragmentos HTML de nuestra página y mostrarlos directamente.

```javascript
    // Carga en el elemento result el documento html
    $( "#result" ).load( "ajax/test.html", function() {
        alert( "Load was performed." );
    });
```

## Manipupación del DOM
jQuery dispone de diversas funciones que pueden ser utilizadas para manipular el DOM:

- [Eliminar elementos](https://api.jquery.com/remove/#remove-selector).
- [Inserción de HTML](https://api.jquery.com/category/manipulation/dom-insertion-outside/)
- [Manipulación de estilos](https://api.jquery.com/css/)

