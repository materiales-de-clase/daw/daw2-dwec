"use strict";

const ID_COLORES = [ 'verde', 'rojo', 'amarillo', 'azul']; 
const NIVEL_INICIAL = 4;
const TIEMPO_ESPERA_ENCENDIDO = 2000;
const TIEMPO_ESPERA_APAGADO = 1000;

let nivelActual = NIVEL_INICIAL;
let secuenciaActual = new Array();
let aceptandoRespuestaJugador = false;

window.addEventListener("load", function () {
    document.getElementById("start").addEventListener("click", jugar);

    document.getElementById("verde").addEventListener("click", procesarPulsacionBoton);
    document.getElementById("rojo").addEventListener("click", procesarPulsacionBoton);
    document.getElementById("amarillo").addEventListener("click", procesarPulsacionBoton);
    document.getElementById("azul").addEventListener("click", procesarPulsacionBoton);

});

function inicializarPartida() {
    nivelActual = NIVEL_INICIAL;
    secuenciaActual = new Array();
    aceptandoRespuestaJugador = false;
}

function jugar() {
    // Inicializo la partida
    inicializarPartida();

    // Genero la secuencia
    generarSecuencia()
}

function jugarSiguienteNivel() {
    nivelActual++;
    secuenciaActual = new Array();
    aceptandoRespuestaJugador = false;

    // Genero la secuencia
    generarSecuencia()
}

async function generarSecuencia() {

    for(let n = 0;n < nivelActual;n++) {
        
        // Obtener un número aleatorio 1 - 4
        const color = Math.floor(Math.random() * 4);

        // Guardo la jugada en el array con la secuencia actual
        secuenciaActual.push(color);

        // Encender la luz asociada al número
        encender(color);

        // Esperar TIEMPO_ESPERA milisegundos
        await esperar(TIEMPO_ESPERA_ENCENDIDO);

        // Apagar la luz
        apagar(color);

        // Esperar TIEMPO_ESPERA milisegundos
        await esperar(TIEMPO_ESPERA_APAGADO);        
    } 

    // Empieza a jugar el jugador
    aceptarRespuesta();
}

function encender(color) {
    
    // Obtiene el botón
    const boton = document.getElementById(ID_COLORES[color]);

    // Cambia el color de borde
    boton.style.borderColor = "pink";
}

function apagar(color) {
  // Obtiene el botón
  const boton = document.getElementById(ID_COLORES[color]);

  // Cambia el color de borde
  boton.style.borderColor = "black";
}

/** Espera el tiempo indicado y vuelve */
async function esperar(tiempo) {
    return new Promise(resolve => {    
        setTimeout(() => { resolve(); }, tiempo);
    });  
}

function aceptarRespuesta() {
    aceptandoRespuestaJugador = true;
}

function procesarPulsacionBoton(evento) {
    
    // Si no está jugando el jugador ignoro el click del ratón
    if(!aceptandoRespuestaJugador)
        return;

    // Obtengo el color
    let color = Number(evento.target.getAttribute('color'));

    // Compruebo si la entrada actual corresponde con el siguiente
    if(color == secuenciaActual.shift()) {
        if(secuenciaActual.length == 0) {
            jugarSiguienteNivel();
        }
    } else {
        alert("Has perdido");
    }
}