// Ejemplo que toma el contenido del formulario
// lo envía al servidor y muestra el resultado.
// Para probarlo, necesitas subir al servidor formulario_json.php
// y actualizar la url en este script.

const xhr = new XMLHttpRequest();

// Declaro un objeto
const objeto = {
    valor1: document.getElementById('valor1').value,
    valor2: document.getElementById('valor2').value,
    valor3: 5
};

// Tenemos la cadena de texto
const json = JSON.stringify(objeto);

xhr.open('POST', 'formulario_json.php');

xhr.onload = function () {
    const respuesta = JSON.parse(xhr.responseText);
    mostrarResultado(respuesta);
};
xhr.setRequestHeader('Content-Type', 'application/json');

xhr.send(json);

// Lo que recibe ahora es un objeto
function mostrarResultado(respuesta) {
    document.getElementById('resultado').value = respuesta.resultado;
}