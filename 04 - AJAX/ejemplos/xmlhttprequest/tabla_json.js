"use strict";

document.forms.filtro.addEventListener('submit', enviarFormulario);

function enviarFormulario(evento) {
    evento.preventDefault();

    // Declaro un objeto
    const parametros = {
        nombre: document.forms.filtro.nombre.value
    };

    const modoPeticion = document.forms.filtro.modoPeticion.value;
    if(modoPeticion == 1) {
        enviarFormularioXmlHttpRequest(parametros);
    } else if(modoPeticion == 2) {
        enviarFormularioFetch(parametros);
    } else {
        enviarFormularioJQuery(parametros);
    }
}

function enviarFormularioXmlHttpRequest(parametros) {
    const xhr = new XMLHttpRequest();
    
    // Tenemos la cadena de texto
    const json = JSON.stringify(parametros);
    
    xhr.open('POST', 'tabla_json.php');
    
    xhr.onload = function () {
        const respuesta = JSON.parse(xhr.responseText);
        mostrarResultado(respuesta);
    };
    xhr.setRequestHeader('Content-Type', 'application/json');
    
    xhr.send(json);
}

function enviarFormularioFetch(parametros) {
    let url = 'tabla_json.php';
  
    fetch(url, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(parametros), // data can be `string` or {object}!
      headers:{
        'Content-Type': 'application/json'
      }
    })
    .then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => mostrarResultado(response));
}

function enviarFormularioJQuery(parametros) {
    $.post(
        'tabla_json.php',
        JSON.stringify(parametros),
        function (respuesta) {
            mostrarResultado(respuesta);
        }
    );
}

function mostrarResultado(respuesta) {

    const modoRender = document.forms.filtro.modoRender.value;
    
    if(modoRender == 1) {
        mostrarResultadoJavaScript(respuesta);
    } else if(modoRender == 2) {
        mostrarResultadoJson2Html(respuesta);
    } else {
        mostrarResultadoJQuery(respuesta);
    }
}

// Lo que recibe ahora es un objeto
function mostrarResultadoJavaScript(respuesta) {
    
    // Código HTML
    let html = '';

    // Respuesta es un objeto.
    for(let cliente of respuesta) {
        html += '<tr>';
            html += mostrarCliente(cliente);
        html += '</tr>';
    }
   
    document.getElementById('resultado').innerHTML = html;
}

function mostrarCliente(cliente) {
    let html = '';
    html += mostrarAtributo(cliente['nombre']);
    html += mostrarAtributo(cliente['apellido1']);
    html += mostrarAtributo(cliente['apellido2']);
    html += mostrarAtributo(cliente['nif']);

    return html;
}

function mostrarAtributo(attr) {
    return '<td>'+attr+'</td>';    
}


function mostrarResultadoJson2Html(resultado) {
    const plantilla = {'<>':'tr','html':[
        {'<>':'td','html':'${nombre}'},
        {'<>':'td','html':'${apellido1}'},
        {'<>':'td','html':'${apellido2}'},
        {'<>':'td','html':'${nif}'}
    ]};
    
    document.getElementById('resultado').innerHTML =
        json2html.render(resultado, plantilla);
}

function mostrarResultadoJQuery(respuesta) {
    $( "#resultado" ).empty();
    
    for(let cliente of respuesta) {
        $( "#resultado" ).append(mostrarCliente(cliente));
    }
}