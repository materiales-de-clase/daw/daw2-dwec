<?php
    // Pasa la entrada a json
    $json = file_get_Contents('php://input');

    // Aquí tenemos un array con los parámetros de entrada
    $objeto = json_decode($json, TRUE);

    // Obtiene los valores que vienen en el array asociativo
    $valor1 = intval($objeto["valor1"]);
    $valor2 = intval($objeto["valor2"]);
    $valor3 = intval($objeto["valor3"]);

    // Tenemos un array con la respuesta
    $respuesta['resultado'] = $valor1 + $valor2 + $valor3;

    // Tenemos que convertir el objeto en un kson
    $respuesta_json = json_encode($respuesta);

    // Prepara la respuesta HTTP
    header('Content-Type:application/json');  
    echo($respuesta_json);
?>