"use strict";

// Instanciamos el objetito
const xhr = new XMLHttpRequest();

// Inicializa la petición
xhr.open("GET", "holamundo.txt");

xhr.onload = function() {
    const respuesta = xhr.responseText;
    mostrarSaludo(respuesta);
}

// Enviar petición al servidor
xhr.send();

function mostrarSaludo(saludo) {
    document.getElementById('saludo').innerText = saludo;
}