// Ejemplo que envía los campos al servidor y muestra el resultado
// Para probarlo, tienes que subir al servidor formulario.php y actualizar
// aquí la URL
const xhr = new XMLHttpRequest();

const formulario = new FormData(document.getElementById('suma'));
formulario.append('valor3', '1');

xhr.open('POST', 'formulario.php');

xhr.onload = function () {
    const resultado = xhr.responseText;
    mostrarResultado(resultado);
};

xhr.send(formulario);

function mostrarResultado(resultado) {
    document.getElementById('resultado').value = resultado;
}