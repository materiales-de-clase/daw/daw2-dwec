
function cargarComboAjax(url, idElementoDestino) {
    
    const elementoDestino = document.getElementById(idElementoDestino);

    fetch(url)
        .then(response => response.json() )
        .then(objeto => { 
        
            const html = json2html.render(objeto, 
                {'<>': 'option', 'value': '${id}','html': '${nombre}'}
            );
        
            elementoDestino.innerHTML = html;

            // Si tiene enlazado un combo, fuerza su refresco
            if(elementoDestino.elementoDestino) {
                __cargarCombo(elementoDestino);
            }
        });  
}

// Funcion que inicializa un combo para que en función de otro input se refresque.
// Asignar los eventos para cuando se modifica la opción seleccionada
// Hacer la llamada al servidor y cargar el combo destino
function enlazarComboAjax(idElementoOrigen, url, idElementoDestino) {

    // Obtgiene las referencias a los elementos
    const elementoOrigen = document.getElementById(idElementoOrigen);
    const elementoDestino = document.getElementById(idElementoDestino);

    // Guardo en el elemento origen los datos que voy a necesitar
    elementoOrigen.elementoDestino = elementoDestino;
    elementoOrigen.url = url;

    // Inicializa los eventos del elemtno origen
    elementoOrigen.addEventListener('change', __onComboEventoRecibido);

    // Rellenamos el combo
    __cargarCombo(elementoOrigen);
}

function __onComboEventoRecibido(evento) {
    __cargarCombo(evento.target);
}

function __cargarCombo(elementoOrigen) {

    // Coge los datos que he guardado previamente en el origen
    const valor = elementoOrigen.value;
    const url = elementoOrigen.url;
    const elementoDestino = elementoOrigen.elementoDestino;

    fetch(url, {
        method: 'POST', 
        body: JSON.stringify({ parametro: valor}), 
        headers:{
            'Content-Type': 'application/json'
        }
    })
    .then(response => { 
        return response.json(); 
    })
    .then(objeto => { 
        
        const html = json2html.render(objeto, 
            {'<>': 'option', 'value': '${id}','html': '${nombre}'}
        );
    
        elementoDestino.innerHTML = html;
    });   
}
