
function inicializarPedidosPorMes(elemento) {
    // Inicializo los puntos a 0
    let dataPoints = [];

    var chart = new CanvasJS.Chart(elemento, {
        animationEnabled: true,
        exportEnabled: true,
        theme: "light1", // "light1", "light2", "dark1", "dark2"
        title:{
            text: "Pedidos por mes"
        },
        axisY:{
            includeZero: true
        },
        data: [{
            type: "column", //change type to bar, line, area, pie, etc
            //indexLabel: "{y}", //Shows y value on all Data Points
            indexLabelFontColor: "#5A5757",
            indexLabelPlacement: "outside",   
            dataPoints: dataPoints
        }]
    });

    $.getJSON("ajax.php?script=grafico_pedidosPorMes", function(data) {  

        data.forEach((e) => dataPoints.push(
            {
                label: e.label,
                y: Number(e.y)
            }
        ));
        chart.render();
    });
}

