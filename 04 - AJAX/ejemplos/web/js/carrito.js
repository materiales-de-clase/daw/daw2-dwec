"use strict";

const __URL_AJAX_CARRITO = 'ajax.php?script=carrito';

/** Añade al carrito X unidades de un producto determinado */
function añadirProductoAlCarrito(id_producto,  unidades, onOk, onError) {

    // Tabla con los parámetros
    const parametros = {
        id_producto: id_producto,
        unidades: unidades
    };

    fetch(
        __URL_AJAX_CARRITO, 
        {
        method: 'POST', 
        body: JSON.stringify(parametros), 
        headers:{
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => {
        if(response && response.resultado) {
            onOk();
        } else {
            onError();
        }
    });

}


function actualizarContadorCarrito(unidades) {
    let contadorCarrito = Number($('.contadorCarrito').first().text().trim());

    contadorCarrito += unidades;

    $('.contadorCarrito').text(contadorCarrito);
} 

function refrescarProductosCarrito() {

    $('#productosCarrito').load('ajax_load.php?script=interfaz/vista_usuario/mostrarCarrito.php');

}