"use strict";

// Innicializa el API
//window.addEventListener('load', inicializarTabla);

/**
 * 
 * @param {*} formulario recibo directamente el objeto formulario
 * @param {*} tabla El elemento HTML
 */
function inicializarTabla(formulario, tabla, url, plantillaTabla) {
    
    // Asigna los eventos
    formulario.addEventListener('submit', frmOnSubmit);
    
    // Guarda los objetos que necesita para llevar a cabo las operaciones
    // con la tabla
    formulario.tablaResultado = tabla;
    formulario.url = url;
    formulario.plantillaTabla = plantillaTabla;

    // Esto ya no funciona
    enviarFormulario(formulario, tabla);
}

function frmOnSubmit(evento) {
    
    // Cancela el handler por defecto
    evento.preventDefault();

    // Elementos
    const formulario = evento.target;
    const tabla = formulario.tablaResultado;

    // Envía un submit del form
    enviarFormulario(formulario, tabla);
}

function enviarFormulario(formulario, tabla) {

    // Calcula el filtro
    let filtroUsuario = formulario.filtro.value.trim();
    if(!filtroUsuario.trim().endsWith('%')) {
        filtroUsuario += '%';    
    }

    // Creo un objeto con los parámetros
    const parametros = {
        filtro: filtroUsuario
    };

    enviarFormularioFetch(formulario.url, parametros, tabla, formulario.plantillaTabla);
}

/**  
 * Recibe como argumento el objeto con los parámetros
 * envía petición JSON
 */
function enviarFormularioFetch(url, parametros, tabla, plantilla) {
  
    fetch(
      url, 
      {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(parametros), // data can be `string` or {object}!
        headers:{
            'Content-Type': 'application/json'
      }
    })
    .then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => mostrarResultado(response, tabla, plantilla));
}

/** 
 * Muestra el resultado en la página del clietne.
 */
function mostrarResultado(resultado, tabla, plantillaTabla) {    
    tabla.innerHTML = json2html.render(resultado, plantillaTabla);
}