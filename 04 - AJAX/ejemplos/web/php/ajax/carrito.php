<?php
    // Pasa la entrada a json
    $json = file_get_contents('php://input');

    // Aquí tenemos un array con los parámetros de entrada
    $objeto = json_decode($json, TRUE);

    // Obtiene los valores que vienen en el array asociativo
    $arg_id_producto = $objeto['id_producto'];
    $arg_unidades = $objeto['unidades'];

    // Obtenemos la información del producto
    $tProductos = Productos::singletonProductos();
    $p = $tProductos->getProductoPorIdProducto($arg_id_producto);
	  
    //--------------------
    // Hace una copia de la cesta deserializada
    $copiaCesta=array();
    $tamanioCesta = sizeof($_SESSION['cesta']);
    for ($i=0;$i < $tamanioCesta; $i++) {
        $pr=$_SESSION['cesta'][$i];
        array_push($copiaCesta,unserialize($pr));
    }

    // Toma los parámetros de entrada. Las unidades ya están asignadas
    $idProducto = $arg_id_producto;
    $unidades = $arg_unidades;  
    $descripcion = $p->getDescripcion();
    $pvp = $p->getPvp();
    $tipoIva = $p->getTipoIVA();
    $fotoProducto = $p->getRutaFoto();

    // Comprueba si el producto está en la cesta. En ese caso incrementa unidades
    $indice=0;
    $encontrado=false;
    while ($indice < $tamanioCesta && !$encontrado){
        
        // Coge el producto de la posición de la cesta indicada
        $pr=$copiaCesta[$indice];
        
        // Si el producto está en la cesta incremento las unidades
        if ($pr->getIdProducto() == $idProducto){
            $encontrado=true;
            $pr->setUnidades($pr->getUnidades() + $unidades);
            
            $copiaCesta[$indice]=$pr;
        }
        else{
            $indice++;
        }
    }

    // Si no lo ha encontrado, genera la línea de pedido y lo inserta en la cesta
    // y en el array de fotos
    if (!$encontrado) {
        
        $detalleProducto = new LineaPedido(0, 0, $idProducto, $unidades, $descripcion, $pvp, $tipoIva, 1);

        array_push($copiaCesta, $detalleProducto);
        array_push($_SESSION['fotos'], $fotoProducto);
    }
    
    // Vuelca el contenido de la copia de la cesta de nuevo a la cesta
    $_SESSION['cesta'] = array();
    foreach ($copiaCesta as $pr) { 

        // Filtro los pedidos que se han quedado a 0 unidades o negativo
        if($pr->getUnidades() > 0) {
            $detalleProducto = serialize($pr);
            array_push($_SESSION['cesta'],$detalleProducto);
        } 
    }
    //--------------------

    // Elimina el cliente y guarda el resutlado
    $respuesta = [];
    $respuesta['resultado'] = 1;

    // Tenemos que convertir el objeto en un json
    $respuesta_json = json_encode($respuesta);

    // Prepara la respuesta HTTP
    header('Content-Type:application/json');  
    echo($respuesta_json);
?>



