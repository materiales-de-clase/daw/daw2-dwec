<?php
     // Pasa la entrada a json
    $json = file_get_contents('php://input');

     // Aquí tenemos un array con los parámetros de entrada
    $objeto = json_decode($json, TRUE);

    // Obtiene los valores que vienen en el array asociativo
    $provincia = $objeto['parametro'];

    // Pasamos a obtener las poblaciones de la base de datos
    $tPoblaciones = Poblaciones::singletonPoblaciones();
    $respuesta = $tPoblaciones->getPoblacionesTodasFA($provincia);

    // Tenemos que convertir el objeto en un json
    $respuesta_json = json_encode($respuesta);

    header('Content-Type:application/json');  
    echo $respuesta_json;	

?>