<?php

    $tPedidos = Pedidos::singletonPedidos();
    $dataPoints = $tPedidos->getPedidosPorMes();

    $respuesta_json = json_encode($dataPoints);

    //Preparar la respuesta Http
    header('Content-Type:applicacion/json');
    echo($respuesta_json);

?>