<?php
    
     // Pasamos a obtener las provincias de la base de datos
     $tProvincias = Provincias::singletonProvincias();

     $respuesta = $tProvincias->getProvinciasTodasFA();

     // Tenemos que convertir el objeto en un json
     $respuesta_json = json_encode($respuesta);
 
     // Prepara la respuesta HTTP
     header('Content-Type:application/json');  
     echo($respuesta_json);

?>