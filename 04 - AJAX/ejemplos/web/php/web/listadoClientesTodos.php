
<form id="configuracionTabla" name="configuracionTabla">
	<input name="filtro" />
</form>

<table class="table table-bordered table-striped text-center table-light table-hover align-items-center">
  <thead class="table text-white bg-success">
    <tr>
      <th scope="col">Id_Cliente</th>
      <th scope="col">id_Usuario</th>
      <th scope="col">Nombre</th>
      <th scope="col">Primer Apellido</th>
      <th scope="col">Segundo Apellido</th>
      <th scope="col">NIF</th>
      <th scope="col">Código postal</th>
      <th scope="col">Nº cuenta</th>
      <th scope="col">Como nos conocio</th>
      <th scope="col">Activo</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody id='resultado'>
  </tbody>
</table>

<script src="js/tablas_ajax.js" defer></script> 
<script src="js/crud_ajax.js" defer></script> 
<script src="js/modal.js" defer></script> 

<script>
	window.addEventListener('load', function () {
		
		inicializarTabla(
			// Filtro de entrada
			document.getElementById('configuracionTabla'),
			
			// Tabla con el resultado
			document.getElementById('resultado'),
			
			// URL para descargar los datos
			'ajax.php?script=getTablaClientesPorNombre',
			
			// Plantilla para generar la tabla
			{'<>': 'tr','html': [
        		{'<>': 'td','html': '${id_cliente}'},
        		{'<>': 'td','html': '${id_usuario}'},
        		{'<>': 'td','html': '${nombre}'},
        		{'<>': 'td','html': '${apellido1}'},
        		{'<>': 'td','html': '${apellido2}'},
        		{'<>': 'td','html': '${nif}'},
        		{'<>': 'td','html': '${cod_postal}'},
        		{'<>': 'td','html': '${numcta}'},
        		{'<>': 'td','html': '${como_nos_conocio}'},
        		{'<>': 'td','html': '${activo}'},
        		{'<>':'td','html':'<button class="botonEliminar btn btn-danger bi bi-trash-fill" value="${id}:${nombre}"></button>'}
    		]}
		);

		// Inicializa botones AJAX con acciones sobre la tabla
		//caInicializarBotonesEliminar('resultado', 'ajax.php?script=crudDeleteCliente');
		$('#resultado').on('click', '.botonEliminar', (evento) => {

			// Ontengo el valor en el botón
			let value = evento.target.value;
			const valores = value.split(':');
			const id = valores[0];
			const nombre = valores[1];
			const mensaje = `
				<p>¿Está seguro de que quiere eliminar el registro? : 
			`+id+'nombre : '+nombre;

			mostrarModalEliminar(mensaje, () => {

				// Eliminar el registro
				caEliminarRegistroAjax('ajax.php?script=crudDeleteCliente', id, 

					() => {
						// Hay que eliminar el registro de la tabla
						$(evento.target).parents('#resultado tr').remove();
					},
				
					() => {					
						// Si hay errores, mostrar un alert con el error
						mostrarAlert('Ha ocurrido un error eliminando el registro');
					}
				);
			});
		});	

	});

</script>
