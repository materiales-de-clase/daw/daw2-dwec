<script src="js/graficos/pedidosPorMes.js" defer></script>
<script>
	window.addEventListener('load', () => {
		inicializarPedidosPorMes('chartContainer1');
		inicializarPedidosPorMes('chartContainer2');
		inicializarPedidosPorMes('chartContainer3');
		inicializarPedidosPorMes('chartContainer4');
	});
</script>

<div class="container border">
	<div class="row d-flex justify-content-evenly">
		<div id="chartContainer1" class="grafico col-lg-4 m-1"></div>
		<div id="chartContainer2" class="grafico col-lg-4 m-1"></div>
		<div id="chartContainer3" class="grafico col-lg-4 m-1"></div>
		<div id="chartContainer4" class="grafico col-lg-4 m-1"></div>
	</div>
<div>