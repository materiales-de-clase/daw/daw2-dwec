"use strict";

// Constantes

// Valores para niveles FACIL, NORMAL y FACIL
const _TAMAÑO_INICIAL = [ 100, 80, 50 ];
const _TIEMPO_INICIAL = [ 100, 80, 50 ];
const _DECREMENTO_CUADRADO = [ 1, 4, 8 ];
const _DECREMENTO_TIEMPO = [ 1, 10, 100 ];

// Objetos utilizados para el renderizado del juego
const pantalla = document.getElementById('juego');
const CTX2D = pantalla.getContext('2d');
let intervalId;

// Variables de estado del juego
let nivel_dificultad = 0;
let tamaño = _TAMAÑO_INICIAL[nivel_dificultad];
let tiempo = _TIEMPO_INICIAL[nivel_dificultad];
let xCuadrado = 150;
let yCuadrado = 150;
let puntuacion = 0;
let aciertos = 0;


// Inicialiación del juego
window.addEventListener('load', () => {    

    // Asigna los gestores de eventos
    document.getElementById('facil').addEventListener('click', 
            iniciarPartidaFacil );
    document.getElementById('normal').addEventListener('click', 
            () => iniciarPartida(1));
    document.getElementById('dificil').addEventListener('click', 
            () => iniciarPartida(2));

});

function iniciarPartidaFacil(evento) {
    iniciarPartida(0);
}

function iniciarPartida(nivelDificultad) {
    
    // Inicializamos la partida
    nivel_dificultad = nivelDificultad;
    tamaño = _TAMAÑO_INICIAL[nivel_dificultad];
    tiempo = _TIEMPO_INICIAL[nivel_dificultad];
    puntuacion = 0;
    aciertos = 0;

    calcularPosiciónCuadrado();


    // Inicializo los eventos
    pantalla.addEventListener('click', onJuegoClick);
    intervalId = window.setInterval(refrescarPantalla, 1);

    // Desactivar los controles
    $('button').attr('disabled', true);
}

function refrescarPantalla() {    
    borrarPantalla();
    
    dibujarMarcador();
    dibujarTiempo();
    dibujarCuadrado();

    tick();
}

function borrarPantalla() {
    CTX2D.clearRect(0 , 0, pantalla.width, pantalla.height);
}

function dibujarMarcador() {
    CTX2D.save();
        CTX2D.font = '20px Verdana';
        CTX2D.fillText(puntuacion,20, 30);
    CTX2D.restore();
}

function dibujarTiempo() {
    CTX2D.save();
        CTX2D.font = '20px Verdana';        
        CTX2D.fillText(tiempo,300, 30);
    CTX2D.restore();
}

function dibujarCuadrado() {
    CTX2D.save();
        CTX2D.fillStyle = 'blue';
        CTX2D.fillRect(xCuadrado, yCuadrado, tamaño, tamaño);
    CTX2D.restore();
}

function crearBufferPantalla(ancho, alto) {
    const offScreenCanvas = document.createElement('canvas');
    offScreenCanvas.width = ancho;
    offScreenCanvas.height = alto;
  
    return offScreenCanvas;
}

function tick() {
    tiempo--;

    if(tiempo < 0) {
        finalizarPartida('Has perdido !!');
    }
}

function finalizarPartida(mensaje) {
    CTX2D.save();
        CTX2D.font = '40px Verdana';
        CTX2D.fillStyle = "red";
        CTX2D.fillText(mensaje,10, 100);
    CTX2D.restore();
    
    // Limpia los eventos.
    window.clearInterval(intervalId);
    window.removeEventListener('click', onJuegoClick);

    // Activa los controles
    $('button').attr('disabled', false);
}

function onJuegoClick(evento) {

    // Obtiene la posición en que se ha pulsado
    const x = evento.offsetX;
    const y = evento.offsetY;

    if(isAcierto(x, y)) {
        puntuacion += tiempo;
        aciertos++;
        tamaño = _TAMAÑO_INICIAL[nivel_dificultad] - (_DECREMENTO_CUADRADO[nivel_dificultad] * aciertos);
        tiempo = _TIEMPO_INICIAL[nivel_dificultad] - (_DECREMENTO_TIEMPO[nivel_dificultad] * aciertos);
       
        calcularPosiciónCuadrado();
    }
}

function calcularPosiciónCuadrado() {
    xCuadrado = Math.random() * (pantalla.width - tamaño);
    yCuadrado = Math.random() * (pantalla.height - tamaño);
}

function isAcierto(x, y) {
    return (x >= xCuadrado && x <= xCuadrado+tamaño) && (y >= yCuadrado && y <= yCuadrado+tamaño);   
}