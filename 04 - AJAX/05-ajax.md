# AJAX
Lo podemos considerar una técnica cuya finalidad es que nuestras páginas puedan enviar peticiones a un servidor de forma asíncrona para, por ejemplo, refrescar partes de la página web.

## Objeto XmlHttpRequest
Objeto diseñado inicialmente por Microsoft y que finalmente se adoptó como standard.

- **Descripción**: https://developer.mozilla.org/es/docs/Web/API/XMLHttpRequest
- **Utilización**: https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/Using_XMLHttpRequest

### Ejemplo sencillo
```javascript
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "pagina.php");
    xhr.onload = function () {

        // Con esto puedo obtener el testo de la respuesta
        postMessage(xhr.responseText);
    };
    xhr.send();
```

### Enviar un formulario
```javascript
  // https://developer.mozilla.org/es/docs/Web/API/FormData
  let formData = new FormData(document.forms.nombre_formulario);

  // https://developer.mozilla.org/en-US/docs/Web/API/FormData/append
  formData.append("middle", "Lee");

  let xhr = new XMLHttpRequest();

  // https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/open
  xhr.open("POST", "/article/xmlhttprequest/post/user");

  // Función a la que se va a llamar cuando termine la descarga
  xhr.onload = () => alert(xhr.response);

  // Envía la petición
  // https://developer.mozilla.org/es/docs/Web/API/XMLHttpRequest#send()
  xhr.send(formData);
```

### Enviar un JSON
```javascript
    let xhr = new XMLHttpRequest();

    // Convierte en objeto a una cadena que representa JSON
    let json = JSON.stringify({
        nombre: "Paco",
        apellidos: "García"
    });

    // Inicializa la petición
    // https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/open
    xhr.open("POST", '/submit');

    // Asigna una cabecera HTTP
    // https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/setRequestHeader
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');

    // Envía nuestro JSON
    xhr.send(json);
```


## API Fetch
- Uso de Fetch https://developer.mozilla.org/es/docs/Web/API/Fetch_API/Using_Fetch

Interfaces que debemos conocer

- [fetch](https://devdocs.io/dom/fetch): el método que se utiliza para cargar el recurso.
- [Headers](https://developer.mozilla.org/en-US/docs/Web/API/Headers): Permite llevar a cabo acciones con las cabeceras enpetición y respuesta.
- [Request](https://developer.mozilla.org/en-US/docs/Web/API/Request): Objeto con el que podemos hacer peticiones personalizadas
- [Response](https://developer.mozilla.org/es/docs/Web/API/Response): Objeto que representa una respuesta


### Ejemplos
#### Petición sencilla
Carga un saludo y lo mete en la página

```javascript
fetch('http://texto')
  .then(response => response.text())        // Obtiene una promesa de que obtendrá el texto
  .then(data => console.log(data));         // Recibe el texto y lo procesa
```

#### Paso de parámetros
```javascript
// Crea la url
let url = new URL('https://pagina.php')

// Crea on objeto con los parámetros
let params = {x:10, y:1} 
url.search = new URLSearchParams(params).toString();

// Carga la url
fetch(url)
    .then(response => response.text())  // Obtiene el texto
    .then(texto => console.log(texto)); // Muestra el texto en consola
```

#### Ejemplo que procesa los errores
```javascript
// Crea la url
let url = new URL('https://pagina.php')

// Crea on objeto con los parámetros
let params = {x:10, y:1} 
url.search = new URLSearchParams(params).toString();

let error = false;

// Carga la url
fetch(url)
    .then(response => {
        if(response.ok == false) { // Guarda la información del error
            error = true;
        } 
        
        // Esto lo hago si o si por tener que devolver una promesa para que
        // funcione el siguiente then. Para un procesamiento más elaborado
        // de errores, necesitaría dar otro tratamiento
        return response.text();
    })  // Obtiene el texto
    .then(texto => {  // Muestra el texto en consola si no hay error
        if(!error) 
            console.log(texto)
    }); 
```

#### Envía un JSON vía POST
```javascript
  let url = 'https://example.com/profile';
  let data = {username: 'example'};

  fetch(url, {
    method: 'POST', // or 'PUT'
    body: JSON.stringify(data), // data can be `string` or {object}!
    headers:{
      'Content-Type': 'application/json'
    }
  })
  .then(res => res.json())
  .catch(error => console.error('Error:', error))
  .then(response => console.log('Success:', response));
```

#### Ejemplo fetch con post, async y await
Este ejemplo enviaría una petición vía post al servidor. Enviará un JSON. Modifica las cabeceras acorde
al tipo de petición que estamos haciendo. 

```javascript
// Define y ejecuta la función immediaramente. Utilizar async define la función
// como asínrona. Esto implica que la función al ejecutarse retorna un objeto
// Promise
(async () => {
  // Ejecutar fetch con await hace que se ejecute de forma síncrona.   
  const rawResponse = await fetch('http://miurldestino', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({a: 1, b: 'Textual content'})
  });

  // Al hacerse ejecutado de forma asíncrona, podemos directamente procesar la respuesta.
  const content = await rawResponse.json();

   // Registra el contenido
  console.log(content);
})();
```

## Referencias
- [Ejemplos de fetch](https://pablomonteserin.com/curso/javascript/ejemplos-api-fetch/)

