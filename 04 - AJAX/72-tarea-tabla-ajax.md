```
Tarea parte de desarrollo paso a paso de aplicación de gestión de tareas
```
# Tablas - AJAX
Hasta ahora, las tablas las has generado en el servidor. Para hacerlo, cargas la página web entera incluyendo tanto la tabla como el resto de información. En las aplicaciones web modernas, es habitual que tablas y otros elementos no requieran cargar la página completa. En esta actividad, vamos a implementar la carga de nuestras tablas de forma asíncrona en todas las páginas de nuestra gestión de contactos.

## Consideraciones previas
Antes de comenzar, debemos tener en cuenta los siguientes aspectos:

- Deberemos implementar esta funcionalidad como una biblioteca que utilizaremos del mismo modo en toda nuestra aplicación. Esto implica que deberemos definir previamente cómo va a funcionar una tabla.
- Lo primero, será diseñar cómo van a ser nuestras tablas en todos los aspectos. Por ejemplo:
  - Definir si la tabla ha de cargar siempre completa o vamos a tener algún tipo de filtro. 
  - Definir si es posible seleccionar una o varias filas para llevar a cabo algún tipo de acción.
  - Definir si vamos a incluir una o varias acciones directas en la tabla. Por ejemplo, incluir un icono para eliminar una determinada fila. En caso de poder hacerlo, tendremos que establecer cómo se va a implementar esta funcionalidad. 
  - Diseñar visualmente nuestra tabla. Si vamos a utilizar bootstrap o no, estilos, etc. En el diseño, deberemos acomodar todas estas funcionalidades. Este diseño lo incluiremos en nuestra guía de estilos (pondré una tarea para ello).
  - Deberemos tener en cuenta en el diseño si vamos a hacer un diseño responsive o no, ya que desde el inicio deberemos implementar este tipo de diseño.
- Para analizar las acciones a incluir, revisar todas las tareas.

Por supuesto, tendremos que definir también cómo vamos a implementar esta funcionalidad en nuestra aplicación. Deberíamos al menos tener una idea clara qué cómo va a funcionar, los eventos que necesitamos gestionar y cómo vamos a implementar las diferentes operaciones. En la implementación vamos a establecer las siguiente restricciones:

- La biblioteca de gestión de tablas genérica debería encontrarse en uno o varios archivos js que sean importados en las páginas web de nuestra aplicación. La biblioteca no debería ser modificada para adaptarse a cada una de las páginas, sino que debería ofrecer un API que permita adaptarla a las diferentes vistas.
- Deberás definir de algún modo cómo se van a mapear las filas a las acciones como eliminar, etc. Aunque escapa al objetivo de este ejercicio implementar estas partes. En el ejercicio no vamos a pasar de mostrar la tabla.
- No debes incluir JavaScript en las páginas HTML más allá del estrictamente necesario. 
- Debe haber unos pasos claros para utilizar la biblioteca.
- Posiblemente ahora, las tablas deberían cargar siempre de forma asíncrona desde el inicio para evitar duplicidad de código.

## Enunciado
La tarea consistirá en:

- Desarrollar una biblioteca para nuestra aplicación que permita cargar las tablas de forma asíncrona. Que implemente todas las funciones que hemos tratado en clase.
- Utilizar dicha biblioteca para renderizar todas las tablas en la aplicación.

## Entrega
La entrega se hará por repositorio y en un archivo comprimido. La tarea debes resolverla en el directorio:

- AJAX/tareas/taskman

El archivo con la entrega contendrá lo siguiente:

- Escribe un pequeño documento pdf (a parte de los comentarios) donde:
  a. enumeres los archivos js implicados y su función.
  b. expliques brevemente cómo funciona tu implementación y cuáles son las principales funciones/gestores de eventos implicados en el proceso.
- Graba un video breve con audio donde se vea tu aplicación en acción y explica cómo funcionan de cara al usuario todos los diferentes detalles que has implementado. Formato mkv/mp4


