# Proyecto
Deberás implementar un proyecto que cubra todas las tareas anteriores. Si lo hiciste bien, los javascript que has creado como biblioteca no necesitarán ser modificados.

Sugerencias:

- Gestor de contactos
- Gestor de contraseñas
- Lista de la compra

## Enunciado
Propón un proyecto que incluya todo lo que hemos hecho en tareas anteriores.

## Entrega
La entrega se hará por repositorio y en un archivo comprimido. La tarea debes resolverla en el directorio:

- AJAX/tareas/miproyecto

El archivo con la entrega contendrá lo siguiente:

- Proyecto comprimido y descarga de la base de datos.
- PDF donde se incluyan los ficheros implicados y el funcionamiento de cada una de las funcionalidades explicado brevemente.
- Video explicativo donde se vea el proyecto en acción y todo lo que se ha implementado.
