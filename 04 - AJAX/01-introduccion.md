# Introducción
## Mecanismos de comunicación asíncrona
Vamos a estudiar a lo largo del tema los siguientes conceptos

- [Promises](https://devdocs.io/javascript/global_objects/promise): Objetos utilizados para llevar a cabo operaciones asíncronas.
- [AJAX](https://developer.mozilla.org/es/docs/Web/Guide/AJAX/Getting_Started) (Asinchronous JavaScript and XML): Permite crear páginas dinámicas donde no es necesario refrescar todo el contenido de la página. Se intercambian datos en diferentes formatos como JSON, XML y otros formatos. Se pueden utilizar fiferentes APIs.
- [Web workers](https://devdocs.io/dom/web_workers_api/using_web_workers): son procesos en JavaScript que se ejecutan en segundo plano.
- [AJAX y jQuery](https://api.jquery.com/category/ajax/): Utilizar jQuery permite también realizar este tipo de peticiones utilizando esta biblioteca.

## Conceptos relacioandos
- [JSON](https://www.json.org/json-es.html) (JavaScript Object Notation): es una notación que permite describir objetos JavaScript en una cadena de texto. Lo vamos a utilizar para intercambiar información entre cliente y servidor.

