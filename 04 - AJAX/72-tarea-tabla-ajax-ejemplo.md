# Tabla AJAX - jQuery/Fetch
En algunos ejemplos en clase, hemos intercambiado datos en formato JSON entre cliente y servidor. En esta tarea, vamos a ejecutar la acción completa y vamos a generar una tabla antes de implementarlo en nuestra aplicación. Necesitaremos:

- Servidor apache con soporte PHP. Trabajaremos con el servidor que tenéis configurado.
- Una tabla de la base de datos. La escogeremos en la clase.

La tabla a generar va a ser la de tareas.

## Apartado 1. HTML
En este apartado vamos a crear el código HTML que vamos a necesitar. Para ello tendremos que tener una página HTML al menos con los siguientes elementos:

- **Formulario**: para permitir filtrar los datos de la tabla. Podemos, por ejemplo, permitir al usuario introducir un nombre para mostrar solo los registros que coinciden con dicho campo.
- **Tabla**: La tabla donde vamos a motrar los registros seleccionados. El contenido de la tabla lo vamos a generar de forma dinámica.

### Formulario
Para recoger los datos vamos a necesitar un formulario. El objetivo es hacer, por ejemplo, una búsqueda por algún atributo que permita obtener más de una fila de la tabla. De modo que vamos a crear un formulario que tenga los siguientes elementos:

- Input donde el usuario introduzca su filtro. Por ejemplo un nombre.
- Un select que permita seleccionar entre varios campos. De modo que el texto que escribamos en el input de búsqueda permita buscar por diferentes criterios.
- Select con las opciones Fetch / jQuery. Por defecto, estará seleccionada la opción XmlHttpRequest. Tendremos que implementar las tres formas.
- Select con las opciones para generar la tabla. JavaScript/json2html/jQuery. Para jQuery utilizaremos el método append que permite añadir contenido a un elemento pero la generación la haremos utilizando JavaScript.
- Submit

### Tabla
El objetivo es enviar la consulta, obtener el resultado en formato JSON, y generar una tabla HTML. Para hacerlo, recuerda que el documento HTML tendrá que tener un sitio reservado para los datos. Luego utilizaremos uno de estos métodos:

- **innerHTML**: Para insertar el código de la tabla directamente en este atributo.
- **appendChild**: Puede ser algo más complicada de implementación.

De cualquier modo, el objetivo de este apartado sería, que establezcas donde irá la tabla y que formato va a tener. Es decir, en qué punto del documento has de insertar el código HTML.

## Apartado 2. PHP
Hay que crear un script PHP que reciba vía JSON el parámetro de entrada y retorne un archivo JSON con el resultado. Ten en cuenta que:

- Controla los errores o el resultado vacío. De modo que no falle el programa cliente en caso de que no haya datos o haya errores. Debes controlar las situaciones especiales. Por ejemplo, retornando una variable de estado en el JSON.
- Retorna la salida en formato JSON.

## Apartado 3. Cliente 
En este apartado tendrás que implementar la parte cliente que se encarga de :

- Enviar los datos al servidor
- Recibir la respuesta. 
  - En caso de error, mostrar el error de algún modo
  - En caso de que todo ok, generar la tabla y mostrarla.
- Generar la tabla según el método. El método actual va a ser generar el HTML utilizando el método seleccionado. 
- El código HTML debe generarse de forma asíncrona.

## Entrega
El archivo con la entrega contendrá lo siguiente:

- Archivo php
- Archivo html
- Archivo/s mjs
- Archivo css (si lo hubiere)
- Captura de pantalla con la tabla que genera el código.
- Captura de pantalla con mensaje de error.













