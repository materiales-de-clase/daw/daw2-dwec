# Bloque de ejercicios 2
Bloque de ejercicios básicos para trabajar con las estructuras que hemos visto en JavaScript. Los ejercicios deben subirse al GIT del alumno en la carpeta javascript/ejercicios/sentencias. 

El nombre del archivo tendrá el siguiente formato:

- js0202.js

No incluyas el nombre descriptivo.

Recuerda que todos los programas deben incluir antes de cualquier código:

```javascript
    "use strict";
```

## js0201. Edad
Pide la edad y si es mayor de 18 años indica que ya puede conducir.

## js0202. Calificación
Pide una nota (número). Muestra la calificación según la nota.

## js0203. Cadenas
Realiza un script que pida cadenas de texto  hasta que se pulse “cancelar”. Al salir con “cancelar” deben mostrarse todas las cadenas concatenadas con un guión -.

## js0204. DNI
Realizar una página con un script que calcule el valor de la letra de un número de DNI (Documento nacional de indentidad). 

## js0205. Pirámide
Realiza un script que escriba una pirámide del 1 al 30 de la siguiente forma :
1
22
333
4444
55555
666666

## js0206. Pirámide inversa
Haz un script que escriba una pirámide inversa de los números del 1 al número que indique el usuario de la siguiente forma : (suponiendo que indica 30).
333
22
1

## js0207. Otra pirámide
Crea script para generar pirámide siguiente con los números del 1 al número que indique el usuario (no mayor de 50) :
1
12
123
1234
12345
123456

## js0208. ¿Quién es mayor?
Realiza un script que pida por teclado 3 edades y 3 nombres e indique el nombre del mayor.

## js0209. Número aleatorio
Realiza un script que genere un número aleatorio entre 1 y 99.

## js0210. Tres números aleatorios
Genera 3 números aleatorios entre 1 y 99 pero que no se repita ninguno.

## js0211. Quiniela
Realiza un script que imprima 14 resultados aleatorios de una quiniela 1 X 2. Ejemplo de resultado:
Resultado 1: 1
Resultado 2: X
Resultado 3: 2
…..
Resultado 14: 2

## js0212. Busca vocal (con bucles)
Realiza un script que muestre la posición de la primera vocal de un texto introducido por teclado.

## js0213. Busca vocal (con includes)
Realiza el mismo ejercicio anterior pero utilizando el método includes. 

## js0214. Contar 
Haz un programa que lea un número mayor que cero por pantalla. Si el número es incorrecto, deberá mostrar el error. En caso contrario, imprimirá todos los números de 0 hasta el número indicado.

## js0215. Factorial for
Pide un número y calcula el factorial de forma iterativa. Utilizando for.

## js0216. Factorial while
Pide un número y calcula el factorial de forma iterativa. Utilizando while.

## js0217. Adivina el número
Crea el programa adivina el número que:

- Genere un número aleatorio entre 1 y 100. (Utiliza el objeto Math para hacerlo)
- Permita al usuario introducir números de modo que:
  - Si el número es menor mostrará un mensaje que lo indique
  - Si el número es mayor mostrará un mensaje que lo indique
  - Si es igual el usuario gana la partida
- El usuario tendrá un número máximo de intentos que tendrá que ser definido en una constante.
 
## js0218. Tipos de motor. if/else
Pide el tipo de motor al usuario (indicando que los valores posibles son 1, 2, 3, 4) y:

a) Si el tipo de motor es 0, mostrar un mensaje indicando “No hay establecido un valor definido para el tipo de bomba”.

b) Si el tipo de motor es 1, mostrar un mensaje indicando “La bomba es una bomba de agua”.

c) Si el tipo de motor es 2, mostrar un mensaje indicando “La bomba es una bomba de gasolina”.

d) Si el tipo de motor es 3, mostrar un mensaje indicando “La bomba es una bomba de hormigón”.

e) Si el tipo de motor es 4,mostrar un mensaje indicando “La bomba es una bomba de pasta alimenticia”.

f) Si no se cumple ninguno de los valores anteriores mostrar el mensaje “No existe un valor válido para tipo de bomba”.

## js0219. Tipos de motor. switch
Resuelve el ejercicio anterior utilizando la sentencia switch.

## js0220. do/while
Crea un programa que pida números al usuario hasta que el número introducido sea 0.

## js0221. Piedra papel tijera
Realiza un juego de Piedra Papel Tijera contra el Pc.

## js0222. Calculadora
Crea un programa que implemente una calculadora que cumpla lo siguiente: 

- Debe tener una pantalla que almacena siempre el valor actual. Inicialmente contiene un 0.
   - La pantalla se muestra siempre al inicio y después de cada operación.
   - Considera la pantalla un atributo que tiene un valor de solo lectura.
   - Todas las operaciones admiten dos operandos. El primero es la pantalla y el otro se pide al usuario. 
- Tendrá una memoria que permitirá almacenar un operando que se inicializará a 0.
- Debe soportar las operaciones: - +,-,*,/. 
- La operación C pone la pantalla a cero.
- La operación M guarda el valor en la pantalla en memoria.
- La operación R pone el valor en memoria en pantalla.
- Si cuando se está leyendo un operando se introduce R el operando será el valor en memoria.
- La operación q permite cerrar la calculadora. 
- La calculadora debe mostrar un error cuando se realicen operaciones no válidas.



