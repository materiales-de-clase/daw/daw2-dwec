Objetos
# Objetos
En este apartado vamos a profundizar en los objetos. Vamos a empezar viendo sus características:

- Un objeto es una lista no ordenada de propiedades. Cada propiedad tiene un nombre y un valor.
- El nombre de una propiedad es generalmente un identificador.
- Un objeto hereda las propiedades de otro objeto, conocido como su "prototype".
- Los objetos en JavaScript son dinámicos. Se pueden añadir y eliminar propiedades a demanda.
- Todo valor en JavaScript que no es String, Number, Symbol, true, false, null or undefined es un objeto.
- Aunque String, Number y booleanos no son objetos, se puede comportar como objetos immutables.

---

Las operaciones más comunes que se hacen con los objetos son:

- Crear los objetos
- Asignar, consultar, borrar, comprobar y enumerar su propiedades.

---
Sobre las propiedades, debemos tener en cuenta lo siguiente:

- Una propiedad tiene un nombre y un valor.
- El nombre de una propiedad puede ser una cadena o cualquier Symbol. 
- Un objeto no puede tener dos propiedades con el mismo nombre. 
- El valor de una propiedad puede ser cualquier valor válido en JavaScript o una función getter/setter.
- Es posible diferenciar entre las propiedades heredadas y las que son definidas en el objeto.
- Además de su nombre y valor, cada propiedad tiene tres atributos:
  - writable: indica cuando se puede escribir en la propiedad
  - enumerable: indica si la propiedad puede ser tratada en un bucle for/in
  - configurable: indica si puede ser eliminada y sus atributos modificados.

Por defecto, todas las propiedades de los objetos que creemos tienen las propiedades writable, enumerable y configurable a true. 

---
# El objeto Object
Todos los objetos en JavaScript heredan propiedades de Object.prototype salvo que se hayan creado sin prototipo. Por lo tango, tienen algunos métodos heredados de object.

- toString()
- toLocaleString()
- valueOf()
- toJSON()

---
# Creando objetos
Para crear objetos podemos utilizar:

- Literales de objeto
- El operador new
- La función Object.create()

---
## Literales de objeto
Un literal de objeto es una lista de pares nombre:valor separados por comas encerrados entre llaves. **Crea e inicializa un nuevo objeto cada vez que se evalúa**. 

```javascript
    let vacio = {};                 // Un objeto que no tiene propiedades
    
    let punto = { x: 0, y: 10 }    // Define un objeto punto con dos propiedades.

    let libro = {                   // Define un libro.
        titulo: "JavaScript",       // El título

        autor: {                    // La propiedad autor es un objeto
            nombre: "Paco",
            apellidos: "García"
        }

    };
```
--- 

## Creando objetos con new
Este operador permite instanciar un nuevo objeto. Se deben cumplir los siguientes puntos:

- Este operador crea e inicializa un nuevo objeto. 
- El operador new debe ir seguido de una invocación de función. 
- La función usada de este modo es llamada constructor y sirve para inicializar el objeto creado.

En JavaScript disponemos de constructores para los objetos incluidos en el lenguaje. Por ejemplo:

```javascript
    let o = new Object();
    let a = new Array();
    let d = new Date();
    let r = new Map();
```

Podemos también crear funciones constructoras que inicialicen nuestros objetos. Por ejemplo:

```javascript
function Car(make, model, year) {
  this.make = make;
  this.model = model;
  this.year = year;
}

var mycar = new Car('Eagle', 'Talon TSi', 1993);
```

--- 
## Creando objetos con Object.create()
Este método crea un nuevo objeto utilizando el primer argumento como el prototipo. Es decir, nuestro nuevo objeto heredará las propiedades de este objeto.

```javascript
  let objeto = Object.create({ x:1, y: 2 });    // objeto hereda las propiedades del literal

  let vacio = Object.create(null);              // Utilizar null como prototipo hace que el 
                                                // objeto no tenga método como toString()
  let normal = Object.create(Object.prototype); // Esto es equivalente a new o []

```

---
# Asignar y leer propiedades
Para obtener o asignar el valor de una propiedad podemos utilizar el '.' o [].

Para obtener el valor de una propiedad:

```javascript
    let nombreAutor = libro.nombreAutor;    // Obtengo el nombre del autor utilizando las 
    let nombreAutor = libro["nombreAutor"]; // dos formas
```

Para **crear** o asignar un valor a una propiedad se puede usar también la misma sintaxis.
```javascript
    let libro.nombreAutor = "Paco";    // Obtengo el nombre del autor utilizando las 
    let libro["nombreAutor"] = "Paco"; // dos formas
```

En la segunda forma, parece que estamos accediendo a un array utilizando una cadena en lugar de 
un número. Este tipo de array se llama array asociativo (hash, map o dicctionary). **Todos los objetos en JavaScript son arrays asociativos**. Esto implica ciertas ventajas. Por ejemplo, mientras que en Java las propiedades de un objeto tienen que ser definidas por adelantado, en JavaScript pueden ser definidas a medida que el programa se ejecuta. O incluso se pueden recorrer todas las propiedades con el bucle for/in.

---
# Herencia
Todo objeto en JavaScript

- Tiene su propio conjunto de propiedades.
- Hereda propiedades del objeto prototipo.

Por ejemplo

```javascript
    let punto = {};     // Literal vacío no tiene propiedades salvo las de Object.prototype

    o.x = 10;           // Ahora, además, tiene una propiedad x
    o.y = 20;           // Ahora, además, tiene una propiedad y


```

---

# Borrando propiedades
El operador delete permite eliminar una propiedad de un objeto.

```javascript
    delete punto.x;     // Elimina la propiedad x
```

---
# Comprobar si la propiedad existe
Como se ha dicho anteriormente, en JavaScript un objeto se puede ver como un conjunto de propiedades. Podemos comprobar un objeto tiene una propiedad con:

```javascript
    "nombre" in objeto                      // true si el objeto tiene propiedad "nombre".
    objeto.hasOwnProperty("nombre")         // true si el objeto tiene esa propiedad. 
    objeto.propertyIsEnumerable("nombre")   // true si existe y es enumerable.
    objeto.nombre !== undefined             // true si existe la propiedad. No distingue
                                            // propiedades que han sido asignadas a undefined

```
---
# Recorriendo las propiedades de un objeto
Podemos hacerlo con:

- Bucle for/in
- Obteniendo un array de propiedades con:
    - Object.keys()
    - Object.getOwnPropertyNames()
    - Object.getOwnPropertySymbols()
    - Reflect.ownKeys();

---

# Serializando objetos
La serialización consiste en convertir un objeto en una cadena que puede ser luego restaurada a un objeto. Se pueden utilizar los siguientes métodos:

- JSON.stringify(objeto) : Obtiene una representación en formato JSON del objeto.
- JSON.parse(cadena): Obtiene un objeto a partir de una representación JSON.

---
# Getters y Setters
JavaScript soporta, además de las que hemos visto hasta ahora, "accesor properties". Ests propiedades, en lugar de un valor, tienen métodos get y set. 

Ejemplo. 
```javascript
let objeto = {

    // Define una propiedad asignando un valor
    dataProp: valor,

    // Con esto defino la propiedad accesorProperty a partir de dos funciones
    // El nombre de los métodos es el nombre de la propiedad
    // Si no existe este método será una propiedad de solo escriotura
    get accesorProperty() { return this.dataProp; }

    // Si no hay set sería una propiedad de solo lectura
    set accesorProperty(valor) { this.dataProp = valor; }

};

```
Ojo al this. Este referencia al objeto con el que se ha invocado al método. 

En el ejemplo anterior daría un poco igual usar un tipo de propiedad o la otra. En otros casos, los métodos get/set pueden ser utilizados para calcular propiedades a partir de otras o para simplificar la asignación de ciertos valores.

---
# Definición de métodos
Un método es una función asociada a un objeto, es más, podemos considerar un método como una propiedad de un objeto que es una función.

Veamos algunos ejemplos:

```javascript

// Asigna la propiedad methodname del objeto a una función.
objectName.methodname = functionName;

// En este caso, dentro del literal de objeto asignamos nuestras funciones a propiedades.
var myObj = {
  myMethod: function(params) {
    // ...hacer algo
  }

  myOtherMethod(params) {
    // ...hacer algo más
  }
};
```
También se puede utilizar un constructor para inicializar el objeto. Por ejemplo.

```javascript
// Función Car. Inicializa un objeto con las propiedades y métodos que necesitamos.
function Car(make, model, year, owner) {

  this.make = make;
  this.model = model;
  this.year = year;
  this.owner = owner;

  this.displayCar = displayCar;
}


// Creamos un objeto y lo inicializamos
let coche = new Car('marca', 'modelo', 2222, 'yo');
```



Para invocar a un método haríamos como en otros lenguajes.

```javascript
// Invoco al método.
Objeto.metodo(argumentos);
```

Para acceder a las propiedades del objeto, se puede hacer como se haría aquí utilizando this.

```javascript
function displayCar() {
  var result = `Un hermoso ${this.year} ${this.make} ${this.model}`;
  pretty_print(result);
}
```

