"use strict";

class ObservableString {

    // El valor almacenado
    _valor;

    // Una función a invocar cuando el valor cambia
    listener;

    constructor(s) {
        this._valor = s;
    }

    get valor() {
        return this._valor;
    }

    set valor(v) {
        this._valor = v;

        if(this.listener) {
            this.listener(v);
        }
    }
}

let os = new ObservableString('hola');
os.listener = (v) => {
    console.log('El valor se ha cambiado a : '+v);
}

os.valor = 'hola';
os.valor = 'adios';

