
import { clase2 } from './clase2.mjs';

class clase1 {

    instancia;

    constructor(i) {
        this.instancia = i;
    }

}

let c2 = new clase2();
c2.nombre = 'hola';
let c1 = new clase1(c2);

c1.instancia.mostrarNombre();