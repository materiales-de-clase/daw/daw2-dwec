// Define la clase libro
export { Libro };

class Libro {    
    
    titulo;
    descripcion;
    genero;

    constructor (titulo, descripcion, genero) {
        
        // Valida que los parámetros sean correctos
        if(titulo == null) {
            throw "El título es null";
        } 

        if(descripcion == null) {
            throw "La descripción es incorrecta"
        }

        // Si los parámetros son correctos
        // Carga los valores en los atributos
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.genero = genero;
    }

    /**
     * Getter que ontiene una descrpción que contiene 
     * titulo, descripción y género
     * 
     * Define una propiedad que se calcula en el momento de llamar a la función
     */
    get descripcionCompleta() {
        return this.titulo+this.descripcion+this.genero;
    }

};