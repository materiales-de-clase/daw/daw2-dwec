// Módulo que representa una biblioteca
import * as ModuloLibro from './libro.mjs';
export { Biblioteca };

class Biblioteca {

    // Libros en la biblioteca. Es una lista
    libros = [];

    // Libros por género. Es un map. Permite obtener el libro pasado el génerp
    librosPorTitulo = new Map();

    registrarLibro(libro) {
        
        // Guarda el libro en la lista de libros
        this.libros.push(libro);

        // Guardo el libro en el map para búsquedas por titulo
        this.librosPorTitulo.set(libro.titulo, libro);
    }

    obtenerLibros() {
        return this.libros;
    }

    buscarPorTitulo(filtro) {
        const resultado = []

        // Recorro los libros
        for(let l of this.libros) {

            // Si el título del libro empieza por la cadena enviada como filtro
            // lo guarda en el array resultado
            if(l.titulo.startsWith(filtro)) {
                resultado.push(l);
            }
        }

        // Devuelve los libros que cumplen el criterio
        return resultado;
    }

    buscarPorTituloExacto(titulo) {
        return this.librosPorTitulo.get(titulo);
    }

    borrarTodos() {
        this.libros.splice(0, libros.length );
        this.librosPorTitulo.clear();
    }
}

