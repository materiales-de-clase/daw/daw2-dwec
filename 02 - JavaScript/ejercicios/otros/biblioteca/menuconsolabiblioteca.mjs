// Define el programa biblioteca.
import { Biblioteca } from './biblioteca.mjs';
import { Libro } from './libro.mjs';
import promptSync from 'prompt-sync';

export { MenuConsolaBiblioteca }

const prompt = promptSync();

/**
 * Implementa un menú de consola para gestionar una biblioteca
 */
class MenuConsolaBiblioteca {

    // Atributo con la biblioteca que se va a gestionar
    biblioteca;

    // Inicializa la biblioteca
    constructor() {
        this.biblioteca = new Biblioteca();
    }

    // Ejecuta el menú
    ejecutarMenu() {
        let fin = false;

        while(!fin) {

            // Muestra las opciones disponibles
            this.mostrarOpcionesDisponibles();

            // Recibimos el comando del usuario
            let comando = prompt("Inserta opción: ");
            switch(comando) {
                case 'A': 
                    this.añadirLibro();
                    break;
                case 'L': 
                    this.listarLibros();
                    break;
                case 'Q':
                    fin = true;
                    break;
            }
        }
    }

    mostrarOpcionesDisponibles() {
        console.log("A - Añadir libro");
        console.log("L - Listar libros");
        console.log("B - Buscar por título");
        console.log("G - Obtener por título")
        console.log("D - Borrar todos");
    }

    añadirLibro() {
        // Leer los datos el libro
        let titulo = prompt("Título: ");
        let descripcion = prompt("Descripción: ");
        let genero = prompt("Genero: ");

        // Crear una insdtancia del libro
        let libro = new Libro(titulo, descripcion, genero);

        // Almacenar la instancia en la biblioteca
        this.biblioteca.registrarLibro(libro);
    }

    listarLibros() {
        for(let l of this.biblioteca.obtenerLibros()) {
            console.log(l.descripcionCompleta);
        }
    }
}