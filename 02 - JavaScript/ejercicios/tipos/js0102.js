"use strict";

const prompt = require('prompt-sync')();

// Leo una cadena de la consola
let cadena = prompt('Introduce una cadena de texto : ');
let cadenaSalida = "";

for(let n = 0;n < cadena.length;n++) {
    cadenaSalida += cadena[n];
    if(n < cadena.length - 1) {
        cadenaSalida += '-';
    }    
}

console.log(cadenaSalida);