// Cuenta las vocales en un texto
"use strict";

const prompt = require('prompt-sync')();

let tiempoAntes = Date.now();

let cadena = prompt('Introduce tu nombre : ');

let tiempoDespues = Date.now();

let tiempoRespuesta = tiempoDespues - tiempoAntes;
console.log(tiempoRespuesta/1000);