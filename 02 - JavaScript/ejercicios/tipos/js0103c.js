// Cuenta las vocales en un texto
"use strict";

const prompt = require('prompt-sync')();

// Lee una cadena de la entrada 
let cadena = prompt('Introduce una cadena de texto : ');

// Recorre la cadena y cuenta las vocales
let contadorVocales = 0;

for(let ch of cadena) {

    if(ch == 'a' || ch == 'e') {
        
            contadorVocales++;
    }        
}

console.log(contadorVocales);