// Cuenta las vocales en un texto
"use strict";

const DIAS_SEMANA = [
    'D','lunes', 'martes', 'miércoles','J','V','S'
];

let fecha = new Date();
let fechaCadena = 'Hoy es ';

fechaCadena += DIAS_SEMANA[fecha.getDay()];
fechaCadena += ', ';
fechaCadena += fecha.getDate();

console.log(fechaCadena);
