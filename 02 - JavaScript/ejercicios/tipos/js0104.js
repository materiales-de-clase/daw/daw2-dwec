// Cuenta las vocales en un texto
"use strict";

const prompt = require('prompt-sync')();

// Lee una cadena de la entrada 
let cadena = prompt('Introduce una cadena de texto : ');
let cadenaInvertida = '';

for(let n = cadena.length-1;n >= 0;n--) {
    cadenaInvertida += cadena[n];
}

console.log(cadenaInvertida);