// Cuenta las vocales en un texto
"use strict";

const prompt = require('prompt-sync')();

// Lee una cadena de la entrada 
let cadena = prompt('Introduce una cadena de texto : ');

// Recorre la cadena y cuenta las vocales
let contadorVocales = 0;
const  vocales = [ 'a', 'e', 'i', 'o', 'u' ];

for(let ch of cadena) {
    // ch es un caracter de la cadena
    let encontrado = false;
    
    for(let n = 0;n < vocales.length && !encontrado;n++) {
        if(vocales[n] === ch) {
            encontrado = true;
        }
    }

    if(encontrado) {
        contadorVocales++;
    }
}

console.log(contadorVocales);