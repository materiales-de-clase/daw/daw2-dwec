"use strict";

const prompt = require('prompt-sync')();

// Variables 
let cadena;
let cadenaMayusculas;

// Leo una cadena de la consola
cadena = prompt('Introduce una cadena de texto : ');

// Convierte la cadena 
cadenaMayusculas = '';
for(let ch of cadena) {
    if(ch >= 'a' && ch <= 'z') {
        cadenaMayusculas += ch.toUpperCase();
    } else {
        cadenaMayusculas += ch;
    }
}

// Muestra la cadena
console.log(cadenaMayusculas);

