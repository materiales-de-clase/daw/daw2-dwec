// Ordenar palabras
"use strict";

const prompt = require('prompt-sync')();





/**
 * Inseta una cadena en el array en orden alfabétco
 * 
 * @param {*} elementos array de cadenas
 * @param {*} cadena cadena
 */
function insertarElementoArray(elementos, cadena) {

    let indice = 0;
    let destino = [];

    // Copia ahora los que son menores a destino
    while(indice < elementos.length && elementos[indice] < cadena) {
        destino.push(elementos[indice]);
        indice++;
    }

    // Copia el elemento
    destino.push(cadena);
    
    // Copia ahora el resto de elementos
    while(indice < elementos.length) {
        destino.push(elementos[indice]);
        indice++;
    }

    return destino;
}

/**
 * Inseta una cadena en el array en orden alfabétco
 * 
 * @param {*} elementos array de cadenas
 * @param {*} cadena cadena
 */
function insertarElementoArraySplice(elementos, cadena) {
    let indice = 0;

    // Copia ahora los que son menores a destino
    while(indice < elementos.length && elementos[indice] < cadena) {
        indice++;
    }

    // Inserta el elemento con splice
    elementos.splice(indice, 0, cadena);

    // devuelve el array
    return elementos;
}

let r = insertarElementoArray([ 'a', 'b', 'c', 'n', 'f'], 'e');
let r2 = insertarElementoArraySplice([ 'a', 'b', 'c', 'n', 'f'], 'e');

// Muestra el destino
console.log(r);
console.log(r2);