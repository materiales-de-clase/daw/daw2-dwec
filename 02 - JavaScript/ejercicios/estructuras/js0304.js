// Ordenar palabras
"use strict";

const prompt = require('prompt-sync')();

// Crea un set
let articulos = Array();

// Lee artículos
let fin = false;
do {
    // Leer un artículo
    let a = prompt('Introduce un artículo : ');

    // Si la cadena está vacía he terminado
    if(a == '') {
        fin = true;
    } else {
        // Si no está vacía tengo que comprobar si está
        if(buscarArticulo(articulos, a)) {
            console.log('YA EXISTE');
        } else {
            console.log('añadido');
            articulos = insertarArticulo(articulos, a);
        }
    }    

} while(!fin);

console.log('');

for(let a of articulos) {
    console.log(a);
}


function buscarArticulo(elementos, cadena) {
    let encontrado = false;
    let n = 0;
    
    while(!encontrado && n < elementos.length) {
        if(cadena == elementos[n++]) {
            encontrado = true;
        }
    }

    return encontrado;
}

/**
 * @param {*} elementos array de cadenas
 * @param {*} cadena cadena
 */
 function insertarArticulo(elementos, cadena) {

    let indice = 0;
    let destino = [];

    // Copia ahora los que son menores a destino
    while(indice < elementos.length && elementos[indice] < cadena) {
        destino.push(elementos[indice]);
        indice++;
    }

    // Copia el elemento
    destino.push(cadena);
    
    // Copia ahora el resto de elementos
    while(indice < elementos.length) {
        destino.push(elementos[indice]);
        indice++;
    }

    return destino;
}