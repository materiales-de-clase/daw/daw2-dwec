// Sumar elementos
"use strict";

const prompt = require('prompt-sync')();

// Creo un array vacío
let elementos = [];
let fin = false;
do {
    
    // Leo un número
    let cadena = prompt('Introduce número : ');
    if(cadena != '') {
        // Convertir en un número la cadena
        let numero = Number(cadena);

        // Añado el número al array
        elementos.push(numero);
    } else {
        fin = true;
    }

} while(!fin);

// Muestra la longitud del array
console.log('Longitud del array : '+elementos.length);

// Muestra todos los elementos en el orden de entrada
console.log('Mostrar los elementos en orden');
for(let e of elementos) {
    console.log(e);
}

// Muestra el contenido del array en orden inverso
console.log('Mostrar los elementos en orden inverso');
for(let n = elementos.length-1;n >= 0;n--) {
    console.log(elementos[n]);
}

// Calcula la suma
console.log('Muestra la suma');
let suma = 0;
for(let e of elementos) {
    suma += e;
}
console.log(suma);