// Ordenar palabras
"use strict";

const prompt = require('prompt-sync')();

// Leo la palabra que va a limitar las letras
const letrasProhibidas = prompt('Introduce la palabra prohibida : ');
const validas = [];
const novalidas = [];

// Leo cadenas de texto hasta que me introducen la cadena vacía
let fin = false;
do {
    // Leer una cadena de texto
    let cadena = prompt('Introduce cadena de texto : ');

    // Si la cadena está vacía he terminado
    if(cadena == '') {
        fin = true;
    } else {
        // Si no está vacía tengo que comprobar si contiene letras prohibidas
        if(contieneLetrasProhibidas(letrasProhibidas, cadena)) {
            console.log('Contiene letras prohibidas');
            novalidas.push(cadena);
        } else {
            validas.push(cadena);
        }
    }    

} while(!fin);

console.log('Palabras válidas');
for(let palabra of validas) {
    console.log(palabra);
}

console.log('Palabras no válidas');
for(let palabra of novalidas) {
    console.log(palabra);
}


//-------------------------------------------------------------------------------
// Comprueba las letras prohibidas
//-------------------------------------------------------------------------------
function contieneLetrasProhibidas(letrasProhibidas, cadena) {
    let resultado = false;

    // Recorrer todas las letras prohibidas
    for(let letra of letrasProhibidas) {
        if(contieneLetra(letra, cadena)) {
            resultado = true;
            break;
        }
    }

    return resultado;
}


function contieneLetra(letra, cadena) {
    // Ejemplo con indexOf
    //return cadena.indexOf(letra) >= 0;

    let contiene = false;

    for(let ch of cadena) {
        if(letra == ch) {
            contiene = true;
            break;
        }
    }

    return contiene;
}