// Calculadora I
"use strict";

const prompt = require('prompt-sync')();
let acumulador = 0;
let memoria = 0;
let operaciones = {
    '+': sumar,
    '-': restar
};

// Ejecuto el menú de la calculadora
menuCalculadora();



//--------------------------------------------------------------
// Menú Calculadora
//--------------------------------------------------------------
function menuCalculadora() {
    
    // Defino variable para controlar la finalización del peograma
    let fin = false;

    while(!fin) {
        console.log('Acumulador = '+acumulador);
        let operacion = prompt('Introduce la operación a realizar : ');
        let operando2 = 0;

        switch(operacion) {
            case 'fin' : 
            case 'Q' : 
            case 'q' : 
                fin = true;
                break;
            case '+': 
            case '-':
            case '*':
            case '/':
                // Código para dos operandos
                operando2 = Number(prompt('Introduce número : '));
                operaciones[operacion](operando2);
                break;
            default:                
                console.log('Operación no soportada : '+operacion);
        }        
    }
}

//--------------------------------------------------------------
// Calculadora
//--------------------------------------------------------------
function sumar(x) {
    acumulador += x;
}

function restar(x) {
    acumulador -= x;
}

function factorial() {
    function _factorial(x) {
        if(x > 0) {
            return x * factorial(x-1);
        } else {
            return 1;
        }   
    }
    acumulador = _factorial(acumulador);
}

function potencia(exp) {
    // Calcula acumulador elevado al exponente
}

function guardarMemoria() {
    memoria = acumulador;
}

function recuperarMemoria() {
    acumulador = memoria;
}

function cero() {
    acumulador = 0;
    memoria = 0;
}