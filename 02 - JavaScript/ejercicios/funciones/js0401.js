// Define la función factorial
// Factorial(X) = | SI X > 0 = X*FACTORIAL(X-1)
//                | SI X == 0 = 1
"use strict";

const prompt = require('prompt-sync')();
const factorialDeX = x => (x > 0)?x*factorialDeX(x-1):1;

// Leo un número de la consola
let x = prompt('Introduce un número : ');

console.log('factorial(x) = '+factorial(x));
console.log('factrialDeX(x) = '+factorialDeX(x));


//------------------------------------------------------------------
// Funciones
//------------------------------------------------------------------
//                  SI X > 0 = X*FACTORIAL(X-1)
//                | SI X == 0 = 1
function factorial(x) {

    if(x > 0) {
        return x * factorial(x-1);
    } else {
        return 1;
    }
    
}

