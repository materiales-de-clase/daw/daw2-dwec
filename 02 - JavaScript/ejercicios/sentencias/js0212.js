// Cuenta las vocales en un texto
"use strict";

const prompt = require('prompt-sync')();

let cadena = prompt('Introduce una cadena de texto : ');
let encontrado = false;
let indice = -1;
for(let n = 0;n < cadena.length && !encontrado;n++) {

    switch(cadena[n]) {
        case 'a','e','i','o','u':
            encontrado = true;
            indice = n;
            break;
        default:            
    }        
}

if(indice < 0) {
    console.log('No se han encontrado vocales');
} else {
    console.log('la primera vocal se encuentra en la posición : '+indice);
}