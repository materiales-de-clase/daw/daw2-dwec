// Cuenta las vocales en un texto
"use strict";

const prompt = require('prompt-sync')();
const LETRAS_NIF = 'TRWAGMYFPDXBNJZSQVHLCKE';

let dni = Number(prompt('Introduce el dni : '));

let letra = LETRAS_NIF[dni % 23];

console.log(letra);