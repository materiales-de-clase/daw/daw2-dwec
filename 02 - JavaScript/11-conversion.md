# Conversión
Tenemos dos tipos de conversión

## Implicita
La lleva a cabo JavaScript de forma automática. Dependiendo del objeto que necesitemos y el que suministremos tendremos un objeto resultante. 
### Operadores aritméticos
- El operador + produce la suma de operandos numéricos o concatenación de cadenas.
- Los operadores - * / convierten la cadena a Number de forma implícita y realizan la operación. 

### Conversión de booleanos

- true se convertirá en 1 en operaciones aritméticas.
- false se convertirá en 0 en operaciones aritméticas

Todos los valores en javascript se convierten en true con la excepción de:

- ""
- 0
- undefined
- null
- NaN
- false

Conversión de cualquier valor a booleano se utiliza !!.

```javscript
!!"" == false
!!NaN == false
!!null == false
```

## Explícita
Puede ser que queramos hacer una conversión de tipo explícita para tener un código más claro o tener un coltrol preciso de las operaciones que se pueden realizar. Para hacer una conversión explícita de tipos, JavaScriot permite usar las funciones:

- Number() 
- String()
- Boolean()

Estas reciben un valor y devuelven un valor del tipo que representan.

Ejemplo de conversión explícita

```javascript
    let cadena = String(5);   
    let numero = Number("5");
```

## Comparación estricta
Se puede utilizar el operador === para comparar valor y el tipo.

