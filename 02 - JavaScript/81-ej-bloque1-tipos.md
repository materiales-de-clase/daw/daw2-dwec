# Bloque de ejercicios 1
Bloque de ejercicios básicos para trabajar con las estructuras que hemos visto en JavaScript. Los ejercicios deben subirse al GIT del alumno en la carpeta javascript/ejercicios/tipos. 

El nombre del archivo tendrá el siguiente formato:

- js0102.js

No incluyas el nombre descriptivo.

Recuerda que todos los programas deben incluir antes de cualquier código:

```javascript
    "use strict";
```

## Ejercicios con String

### js0101. Mayúsculas
Realiza un script que pida un texto y lo muestre en mayúsculas.

### js0102. Guión
Realiza un script que pida una cadena de texto y la muestre poniendo el signo – entre cada carácter sin usar el método replace. Por ejemplo, si tecleo “hola qué tal”, deberá salir “h-o-l-a- -q-u-e- -t-a-l”.

### js0103. Vocales
Realiza un script que cuente el número de vocales que tiene un texto.

### js0104. Invertir cadena
Realiza un script que pida una cadena de texto y la devuelva al revés. Es decir, si tecleo “hola que tal” deberá mostrar “lat euq aloh”.

## Ejercicios con Date 
### js0105. Fecha y hora actual
Realiza un script que escriba en el documento la fecha y hora actual. La salida deberá tener elsiguiente formato:

Hoy es martes, 28 de Febrero de 2018 y son las 14:32 horas.

### js0106. Tiempo de entrada
Realiza un script que pida un nombre y dos apellidos e indique el tiempo que se tardó en introducir los datos. La salida sería algo así como : En introducir Luís Perez Alonso ha tardado 25 segundos.

### js0107. Calendario
Realiza un script pida un mes y año e imprima su calendario. No hace falta esmerarse en la presentación del calendario, el calendario simplificado puede ser del tipo:
OCTUBRE – 2014
1 (miercoles), 2 (jueves), ….. , 31 (viernes).

