# Configuración de VSCode y Node.js
Para ejecutar nuestros programas escritos en JavaScript, vamos a necesitar un entorno de ejecución. Si bien más adelante vamos a utilizar el navegador, con todo lo que ello implica, de momento vamos a centrarnos en los conceptos básicos del lenguaje. Para ello, vamos a recurror a node.js que es un entorno de ejecución construido con el moder JavaScript V8 de chrome.

# Instalar node.js
El primer paso será instalar node.js. Descargaremos la versión 16 desde el siguiente enlace.

- [https://nodejs.org/es/](https://nodejs.org/es/)

Ejecutaremos el instalador. Para nuestra instalación no va a ser necesario cambiar ninguna de las opciones (todo siguiente). Tras la instalación nos interesan los iconos

- **Node.js command prompt** : que ejecuta un interfaz de comandos configurado.
- **Node.js** : intérprete de JavaScript

Dichos iconos apuntan a los programas que vamos a utilizar en las sesiones siguientes.

# Instalación de paquetes para entrada/salida por consola
Vamos a instalar en node un paquete que vamos a utilizar posteriormente en nuestras aplicaciones para leer datos de forma síncrona en consola. El objetivo es que tengamos algo que se utiliza de forma similar a la clase Scanner de Java. Para ello seguiremos los siguientes pasos:

## 1. Abre *Node.js command prompt*
Con esto podemos instalar los paquetes.

## 2. Elige el directorio de instalación
Ahora cambia el directorio a un directorio que esté por encima de los directorios de trabajo de los repositorios. 
Por ejemplo en mi caso tengo 
```
    RepoRoot/
        daw2-dwec-alumnado
```
De modo que como las dependencias las voy a utilizar en proyectos dentro de daw2-dwec-alumnado, me sitúo en el directorio RepoRoot para hacer la instalación de los paquetes. Esto hará que node las encuentre a la hora de ejecutar los programas.

## 3. Ejecuta npm
Ejecuta el comando **npm install prompt-sync**. Esto debería crear un directorio node_modules con el paquete el directorio global por defecto.

Si todo ha salido bien, tendremos el paquete instalado en el directorio node_modules. Lo probaremos más tarde en algún ejemplo. 

El siguiente fragmento de código nos permitiría importar el módulo que hemos instalado y solicitar un dato por consola.

```javascript
    /** Carga el módulo. Función de node.js para cargar módulos  */
    const prompt = require('prompt-sync')();

    /** Lee un nombre desde la consola de forma síncrona */
    const nombre = prompt('¿Cómo te llamas?');
```

# Comprobar integración con VS Code
Si hemos seguido los pasos correctamente, ya podríamos ejecutar programas en JavaScript utilizando Node.js y Visual Studio Code. Para ello, vamos a crear el siguiente programa de prueba:

```javascript
    console.log('Hola mundo');
```

Tenemos varias opciones a la hora de ejecutar nuestro programa por consola.

## Utilizar un terminal
Abriremos el terminal integrado de VStudio Code en el menú **terminal/Nuevo terminal** y ejecutaremos la siguiente secuencia de comandos:

```cmd
    node holamundo.js
```

## Abrir un terminal en modo depuración.
Podemos hacerlo en el panel lateral. En la opción **Ejecución y depuración/JavaScript Debug Terminal**. Utilizando esta opción abriremos un terminal como el anterior pero con la depuración activada. Podemos ejecutar los programas del mismo modo, pero si pusiéramos un punto de ruptura, se dentedría el programa ya que en este caso tenemos la depuración activada.

## Ejecutar el script en modo depuración
Podemos ejecutar el script directamente en modo depuración utilizando el botón **Ejecución y depuración/Ejecutar y depurar**

```
Nota importante de la documentación de visual studio:

Node console

By default, Node.js debug sessions launch the target in the internal VS Code Debug Console. Since the Debug Console does not support programs that need to read input from the console, you can enable either an external terminal or use the VS Code Integrated Terminal by setting the console attribute in your launch configuration to externalTerminal or integratedTerminal respectively. The default is internalConsole.

If an external terminal is used, you can configure which terminal program to use via the terminal.external.windowsExec, terminal.external.osxExec, and terminal.external.linuxExec settings.
```

Por lo que para ejecutar utilizando este botón es necesario crear un launch.json y definir, por ejemplo, la opción console con valor integratedTerminal. Por ejemplo, podemos insertar una línea como:

```json
    "console": "integratedTerminal"
```