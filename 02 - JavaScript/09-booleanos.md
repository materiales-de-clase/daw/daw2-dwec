# Booleanos
Un booleano representa verdadero o falso y se utiliza para ello el objeto Boolean

# Valores que se evalúan como falso
Todo valor en JavaScript puede ser convertido a booleano. Los siguientes valores se convierten siempre a false:

- undefined
- null
- 0
- -0
- NaN
- ""

El resto de valores, incluyendo objetos y arrays se convierten en true.

Así por ejemplo las siguientes comparaciones serían equivalentes

```javascript
    if(object !== null) 
    if(object) 
```

Tenéis más información aquí
- [Referencia de Boolean](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Boolean)
