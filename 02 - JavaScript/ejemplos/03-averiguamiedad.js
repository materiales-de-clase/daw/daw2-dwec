"use strict";

//--------------------------------------------------------------------------
// Constantes
//--------------------------------------------------------------------------
const prompt = require('prompt-sync')();
const miedad = 20;

//--------------------------------------------------------------------------
// Programa principal
//--------------------------------------------------------------------------

while(!fin) {
    // 1. Escuchar una edad
    let edad = prompt('Introduce una edad:');

    // 2. Si es mi edad decirle que ha acertado
    if(edad == miedad) {
        fin = true;
        console.log('Has acertado');
    } else if(edad < miedad) {
        console.log('Soy más viejo');
    } else {
        console.log('Soy más joven');
    }
}

//--------------------------------------------------------------------------
// Funciones
//--------------------------------------------------------------------------