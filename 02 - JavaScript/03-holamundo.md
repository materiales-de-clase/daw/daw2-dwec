# Hola Mundo JavaScript
Como no podía ser de otro modo, vamos a crear nuestro primer programa en JavaScript como lo haríamos al más puro estilo de otros lenguajes de programación. 

# Hola Mundo!
Crea el archivo holamundo.js desde VisualStudio Code y escribe el siguiente fragmento de código.

```javascript
    console.log("Hola mundo!");
```

En el ejemplo se utiliza el objeto console para enviar un mensaje a la consola. Este objeto será muy útil cuando estéis haciendo desarrollo en el cliente para depurar errores. Podéis obtener más información sobre este objeto en la referencia de [DevDocs][console].

Para ejecutar el ejemplo, abre "Ejecutar/Ejecutar sin depuración" (CTRL+F5). Se abrirá un desplegable. Ahí debes seleccionar Node.js. Se mostrará el mensaje en la consola de depuración.

[console]: https://devdocs.io/dom/console
