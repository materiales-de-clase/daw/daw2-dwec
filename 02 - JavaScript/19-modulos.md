# Módulos
El uso de módulos JavaScript nativos depende de las declaraciones **import y export**. Puedes exportar **funciones, var, let, const y clases**. Deben ser elementos de nivel superior; no puedes usar export dentro de una función, por ejemplo.

Este apartado lo vamos a seguir por la web de mozilla en este apartado.

- [Módulos](https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Modules)

## Resumen
### 1. Exportar características
Creamos nuestros módulos de segundo nivel y exportamos las características

```javascript
    export const name = 'square';

    export function draw(ctx, length, x, y, color) {
    ctx.fillStyle = color;
    ctx.fillRect(x, y, length, length);

        return {
            length: length,
            x: x,
            y: y,
            color: color
        };
    }
```

### 2. Importar características
En nuestro script, importamos las características que nos interesen

```javascript
    import { name, draw, reportArea, reportPerimeter } from './modules/square.js';
```

### 3. Importar módulo
En la página se tiene que importar el módulo de primer nivel con la funcionalidad que se quiere utilizar en nuestra página.
```html
<script type="module" src="main.js"></script>

<script type="module">
  /* El código del módulo de JavaScript de primer nivel va aquí */
</script>
```
## A tener en cuenta
- Ejecutar HTML con módulos puede dar problemas si no se usa un servidor. Evitar el file:///.
- Los módulos siempre se ejecutan en **modo estricto**.
- No es necesario utilizar el atributo defer. Los módulos se cargan de forma diferida de forma automática.
- Los módulos solo se ejecutan una vez. Incluso si son referenciados varias veces.
- Las características del módulo se importan al alcance de un solo script — no están disponibles en el alcance global. Solo podrás acceder a las funciones importadas desde el módulo que las importa.


