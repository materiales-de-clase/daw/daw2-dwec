# Fecha
JavaScript define una clase Date para representar y manipular los números que representan fechas. Las fechas en JavaScript son objetos, pero también podemos tener una representación numérica como timestamp. Ejemplos

```javascript
    let timestamp = Date.now();     // Obtiene la fecha y hora actual en milisegundos
    let now = new Date();           // Obtiene la fecha y hora actual como un objeto    
```

# Trabajando con fechas
Para trabajar con fechas, se utilizarán los métodos en la clase Date para obtener fechas y hacer las conversiones necesarias.

Tenéis más información aquí
- [Referencia de Date](https://www.w3schools.com/jsref/jsref_obj_date.asp)
