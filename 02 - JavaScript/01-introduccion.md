# Introducción
En esta unidad de trabajo vamos a familiarizarnos con el lenguaje JavaScript. En la actualidad la gran mayoría de sitios y aplicaciones web utilizan este lenguaje de programación. Además, debemos de tener en cuenta que JavaScript es un lenguaje de programación más, de modo que, no dependemos del navegador para utilizarlo, sino que simplemente necesitamos de un entorno de ejecución o compilador que lo soporte. Como ejemplo, Node.js permite la ejecución de código JavaScript fuera del navegador. 

Entre las características de JavaScript tenemos las siguientes:

- Es un lenguaje de **alto nivel**.
- Es **interpretado**. 
- Es un lenguaje **no tipado**. Esto quiere decir que cuando declaramos una variable no es necesario especificar el tipo.

# Historia
A continuación una pequeña secuencia de eventos.

1. Creado en Netscape en los inicio de la web. Tecnicamente, JavaScript es una marca registrada de Sun Microsystems (ahora Oracle). Esta era usada con licencia por los productos de Netscapa (ahora Mozilla).

2. Netscape envió el lenguaje para estandarización al [ECMA (European Computer Manufacturer´s Association)](https://www.ecma-international.org/) y por problemas con la marca registrada JavaScript el nombre adoptado por el lenguaje fue **ECMAScript**.

3. A finales 2010 la versión 5 de ECMAScript era soportada por todos los navegadores. 

4. La versión 6 del estándar, liberada en 2015, supuso un paso muy importante añadiendo características como clases y módulos.

5. Desde la verión 6, ECMAScript libera una versión cada año y las versiones del lenguaje ahora son identificadas con el año en que son liberadas. Así, la versión liberada en 2020 será ECMAScript2020

6. Como ocurre en muchas ocasiones, cuando un producto evoluciona, tiene que convivir con los errores del pasado en favor de la compatibilidad. Para ECMAScript esto también ocurre y, aunque los desarrolladores intentan corregir algunos de los defectos de las versiones inciales del lenguaje, no pueden romper la compatibilidad. Es por eso que en la versión 5 y posteriores, los desarrolladores pueden optar por programar usando el *strict mode* que corrige algunos de los errores de las versiones iniciales han sido corregidos.

# Contenido de la especificación
Cuando un lenguaje de programación es especificado debemos distinguir entre dos partes claramente diferenciadas:

- Las estructuras o construcciones soportadas por el lenguaje así como el API mínima para trabajar con númneros, texto, arrays, conjuntos, mapas, etc.
- Las API para trabajar con entrada/salida, red, almacenamiento, gráficos, etc. Estas funcionalidades son responsabilidad del entorno de ejecución. 

Aunque anteriormente el único entorno de ejecución posible era el navegador, en la actualidad podemos encontrar otros. Por ejemplo Node.js da a ECMAScript acceso por completo al sistema operativo permitiendo así escribir archivos, enviar realizar otras operaciones como enviar o recibir datos por la red. Hemos de tener presente que aunque **el lenguaje es el mismo en el navegador y en Node, el API es diferente**. Por ejemplo, si ejecutamos nuestro script en Node.js no vamos a tener expuestos los interfaces del DOM para acceder a un supuesto documento cargado en el navegador.

# Versiones de la especificación
En la siguiente [tabla de versiones](https://es.wikipedia.org/wiki/ECMAScript) tenemos un resumen con las versiones que han ido siendo desarrrolladas junto con un resumen de las nuevas características incluidas en el lenguaje.

# Compatibilidad con navegadores
Cada nueva iteración del lenguaje incluye nuevas funcionalidades. Es evidente que, cuando implementamos una aplicación, queremos que esta se ejecute sin problemas en todos los sistemas. Si vamos a trabajar con funcionalidades nuevas, tenemos que tener presente que dichas funcionalidades podrían no estar implementadas todavía en todos navegadores o resto de entornos de ejecución. En la siguiente (tabla)[https://kangax.github.io/compat-table/es2016plus/] tenemos un listado con la compatibilidad de los diferentes navegadores y entornos de ejecución con las diferentes funcionalidades del lenguaje.



