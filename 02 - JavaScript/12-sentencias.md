
# Sentencias
Las sentencias van separadas por ';'.
Los bloques de sentencias van entre { }


# Condicionales

## if

```javascript
    if(expresion) {
        sentencia;
        sentencia;
        sentencia;
    }
```

## else if

```javascript
    if(expresion) {

        sentencia;
        sentencia;
        sentencia;
    } else if(expresion) {

        sentencia;
        sentencia;
        sentencia;
    } else if(expresion) {

        sentencia;
        sentencia;
        sentencia;
    } else {

        // Si no se cumple ninguna de las condiciones se ejecuta este bloque
        sentencia;
        sentencia;
        sentencia;        
    }
```

## switch

```javascript
    switch(expresión) {
        case expresión: 
                sentencia;
                sentencia;
                sentencia;
                break;
        case expresión:
                sentencia;
                sentencia;
                sentencia;
                break;
        default:
                sentencia;
                sentencia;
                sentencia;
                break;
    }
```

# Bucles

## while
```javascript
    while(expresión) {
        sentencia;
        sentencia;
        sentencia;
    }
```

## do/while

```javascript
    do {
    
        sentencia;
        sentencia;
        sentencia;

    } while(expresión);
```

# for

```javascript
    for(inicialización; prueba; incremento) {
        sentencia;
        sentencia;
        sentencia;
    }
```

Tenemos que tener en cuenta que inicialización, prueba e incremento son tres expresiones. El equivalente en el bucle while sería:

```javascript
    inicialización;
    while(prueba) {
        
        sentencia;
        sentencia;
        sentencia;

        incremento;
    }
```

Notar que se pueden declarar variables directamente en el for. También se puede dejar vacío cualquiera de las tres expresiones del bucle. En el incremento, si se quiere controlar más de un incremento, se deben separar por una ','.

Para mostrar los números de 0 a 9 podemos utoilizar el siguiente código. 

```javascript
    for(let n = 0;n < 10;n++) {
        console.log(n);
    }
```

Otros ejemplos válidos son:

```javascript
    for(let n = 0;n < 10;n++) {
        console.log(n);
    }
```

Los siguientes ejemplos serían válidos

```javascript

for(let n = 0, i = 9;n < 10;n++, i--) {
    console.log(n);
    console.log(i);
}

// Equivalente a while(true) {}
for(;;;) {

}
```

## for/of
Aunque utiliza la palabra reservada for, es un tipo de bucle completamente diferente. Este bucle funciona con objetos iterables. Por ejemplo, un array o una cadena son iterables.  Veamos un ejemplo:

```javascript
// Define un array y lo inicializa
let data = [ 1, 2, 3, 4, 5, 6, 7, 8, 9];

// Variable para guardar la suma
let suma = 0;

// Para cada elemento de data una iteración
for(let n of data) {
    suma += n;
}

console.log(suma);
```

Una cadena es iterable caracter a caracter de modo que se pueden recorrer los caracteres de una cadena del siguiente modo:

```javascript
for(let ch of "cadena") {
    console.log(ch);
}
```

## for/in
Este for se puede utilizar para recorrer todos los nombres de las propiedades de un objeto. Tiene la siguiente sintaxis:

```javascript
    for(variable in object) {
        sentencia;
        sentencia;
        sentencia;
    }
```

Por ejemplo, para mostrar todas las propiedades del objeto global, podríamos utilizar:

```javascript
    for(let propiedad in globalThis) {
        console.log(propiedad);
    }
```

# Saltos
En este apartado solo voy a comentar los que ya conocéis de Java. Tienen el mismo significado con pequeñas diferencias. 

- break;
- continue;
- return expression;
- throw expresion; : En este caso se puede devolver un valor de cualquier tipo. También hay clases en JavaScript que representan errores. Por ejemplo se puede lanzar "new Error("Mensaje");"

# Gestión de excepciones

## try/catch/finally
Funciona de modo similar a java pero con algunas diferencias. A continuación tenemos un ejemplo.


```javascript
try {
    // Puedo lanzar un valor de cualquier tipo. En este caso lanzo directamente una cadena
    throw "hola";
    
} catch(e) {
    // e es como un parámetro de una función. No tengo que declarar el tipo. 

    // Como es una cadena lo que se ha lanzado, lo puedo imprimir por pantalla.
    console.log(e);

} finally {

    // Esto se va a ejecutar si o si después del try y el catch.
    console.log("finally");
}
```

# Declaración de variables
En este apartado vamos a ver algunas de las características de las declaraciones de variables en JavaScript

## Variables no declaradas
En javascript podemos utilizar variables sin declararlas. Estas variables tienen las siguientes características:

- Asignar un valor a una variable no declarada implica crearla como variable global (se convierte en una propiedad del objeto global) cuando la asignación es ejecutada.
- Las variables declaradas se limitan al contexto de ejecución en el cual son declaradas. Las variables no declaradas siempre son globales.
- Las variables sin declarar no existen hasta que el código que las asigna es ejecutado.
- Las variables declaradas son una propiedad no-configurable de su contexto de ejecución (de función o global). Las variables sin declarar son configurables (p. ej. pueden borrarse).

Aquí tenemos algunos ejemplos

```javascript
    // Variable sin declarar dentro de la función puede ser referenciada fuera
    // ya que es global

    function x() {
        y = 1;   // Lanza un error de tipo "ReferenceError" 
                 // en modo estricto ('use strict')
        var z = 2;
    }

    x();

    console.log(y); // Imprime "1". Al no estar declarada, la variable es global
    console.log(z); // Lanza un error de tipo "ReferenceError": 
                    // z no está definida afuera de x
```


```javascript
    // La variable no declarada puede ser eliminada
    var a = 1;
    b = 2;
    delete this.a; // Falla ya que es una variable declarada
    delete this.b; 
```

```javascript
    console.log(a);               // La variable no ha sido declarada previamente 
                                  // por lo que esto falla.
```

## var
La sentencia var declara una variable, opcionalmente inicializándola con un valor.

Se deben tener en cuenta los siguientes aspectos:

- El ámbito será su contexto de ejecución en curso, que puede ser **la función que la contiene** o, para las variables declaradas afuera de cualquier función, un **ámbito global**. 
- Si re-declaras una variable Javascript, esta no perderá su valor.

```javascript
    // La variable a no pierde su valor al redeclararla

    var a = 10;
    var a;
    console.log(a); // mostrará 10
```

```javascript
    function x() {
        var z = 2;
    }

    x();

    console.log(z); // Falla. La variable está definida 
                    // dentro de la función x
```

## let
La instrucción let declara una variable de alcance local con ámbito de bloque(block scope), la cual, opcionalmente, puede ser inicializada con algún valor.

- Variables declaradas por let tienen por alcance el bloque en el que se han definido. Cuando usamos let dentro de un bloque, podemos limitar el alcance de la variable a dicho bloque. Notemos la diferencia con var, cuyo alcance reside dentro de la función donde ha sido declarada la variable.
- Una variable declarada con let no puede volver a declararse.
- En el nivel superior de un programa y funciones, let, a diferencia de var, no crea una propiedad en el objeto global.

```javascript
    function varTest() {
    var x = 31;
    if (true) {
        var x = 71;  // ¡misma variable!
        console.log(x);  // 71
    }
    console.log(x);  // 71
    }

    function letTest() {
    let x = 31;
    if (true) {
        let x = 71;  // variable diferente
        console.log(x);  // 71
    }
    console.log(x);  // 31
    }
```

```javascript
    var x = 'global';
    let y = 'global';
    console.log(this.x); // "global"
    console.log(this.y); // undefined
```

## const
Las variables constantes presentan un **ámbito de bloque** (block scope) tal y como lo hacen las variables definidas usando la instrucción **let**, con la particularidad de que el valor de una constante **no puede cambiarse a través de la reasignación**. Las constantes **no se pueden redeclarar**.

```javascript
    // Define una constante y la utiliza
    const a = 7;
    document.writeln("a es " + a + ".");
```

## Buenas prácticas

- En general, **se considera una buena práctica siempre declarar siempre las variables, sin importar si están en una función o un ámbito global**. 
  En caso de no hacerlo, declara la variable al menos antes de su utilización.
- se recomienda siempre declarar variables al inicio de su ámbito (la cima del código global y la cima del código de función) para que sea claro cuáles variables pertenecen al ámbito de función (local) y cuáles son resueltas en la cadena de ámbito. 
- Vamos a utilizar let, ya que tiene un comportamiento similar al que ya hemos estudiado previamente.
- Si el valor de una variable no va a cambiar nunca, **utiliza const** para declararla. Esto permitirá a JavaScript optimizar la ejecución de tu código gracias a esta información adicional.

## Más información
https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Statements/var
https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Statements/let
https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Statements/const


# Otras sentencias
## with
Permite acceder a las propiedades de un objeto como si fueran variables en un contexto determinado. La sintaxis es:

```javascript
    with(objeto) {
        sentencia;
        sentencia;
        sentencia;
    }
```

Permite por ejemplo 

```javascript
    // Tengo que utilizar la f para acceder a los atributos
    let f = document.forms[0]; 
    f.name.value = ""; 
    f.address.value = ""; 
    f.email.value = "";

    // Las propiedades del form son como variables locales
    with(document.forms[0]) {
        name.value = ""; 
        address.value = ""; 
        email.value = "";
    }
```
## use strict
En realidad es una directiva para el intérprete. Esta directiva puede aparecer úniamente al inicio de un script o al inicio del cuerpo de una función antes de la primera sentencia. Poner esta directiva indica que el script utiliza código estricto. Las funciones definidas dentro de un bloque de cófigo estricto se considera que usan también las mismas restricciones. Lo mismo aplica para el código pasado a la función eval();

Disponéis de más información del modo estricto en la [web de mozilla](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Strict_mode).

Ejemplo:

```javascript
"use strict";

a = 10; // No he declarado a. Debería lanzar un error
```