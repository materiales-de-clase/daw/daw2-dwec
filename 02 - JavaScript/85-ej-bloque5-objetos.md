# Bloque de ejercicios 5. Objetos.
Bloque de ejercicios básicos para trabajar con objetos. Los ejercicios deben subirse al GIT del alumno en la carpeta javascript/ejercicios/objetos. 

El nombre del archivo será el nombre del ejercicio.js. Por ejemplo, js0202.js

Recuerda que todos los programas deben incluir antes de cualquier código:

```javascript
    "use strict";
    const prompt = require('prompt-sync')();
```

## js0501. Calculadora. Literal de objeto.
Vamos a crear de nuevo nuestra calculadora, pero vamos a utilizar objetos. La aplicación deberá cumplir.

- El menú principal debe ser una función.
- La calculadora estará implementada como un objeto.
- Tendra una pantalla o acumulador. El valor inicial será 0.
- Implementará los siguientes operadores con 2 operandos. (pantalla más uno solicitado)
   - +,-,*,/,%
   - factorial
   - x elevado a y
- Implementará los siguientes operadores con 1 operando. (pantalla)
- El resultado de las operaciones se almacenará en el acumulador o pantalla.
- Tendrá una memoria que permitirá almacenar el contenido de la pantalla. El operador será 
  M
- Tendrá una operación que permitirá cargar el valor en memoria en la pantalla. R
- Tendrá una operación para poner a 0 tanto la pantalla como la memoria.
- **La calculadora no debe interactuar en ningún momento con el usuario.**
- Cada vez que se realiza una operación, la calculadora generará un evento invocando a una función con el resultado.

Será necesario también que exista un programa de prueba que realize al menos una vez cada una de las operaciones.

## js0502. Calculadora. Literal de objeto. II
Haz un diseño modular del ejercicio anterior

## js0503. Calculadora. new.
Repite el ejercicio pero usa new.

## js0504. Calculadora. Object.create
Repite el ejercicio utilizando Object.create.

## js0505. Cuenta
Crea una clase llamada Cuenta que tendrá los siguientes atributos: titular y cantidad (puede tener decimales). El titular será obligatorio y la cantidad es opcional. Crea dos constructores que cumpla lo anterior.

Crea sus métodos get, set y toString.

Tendrá dos métodos especiales:

- ingresar: se ingresa una cantidad a la cuenta, si la cantidad introducida es negativa, no se hará nada.
- retirar: se retira una cantidad a la cuenta, si restando la cantidad actual a la que nos pasan es negativa, la cantidad de la cuenta pasa a ser 0.


## js0506. Persona
Haz una clase llamada Persona que siga las siguientes condiciones:

Sus atributos son: nombre, edad, DNI, sexo (H hombre, M mujer), peso y altura. Por defecto, todos los atributos menos el DNI serán valores por defecto según su tipo (0 números, cadena vacía para String, etc.). Sexo sera hombre por defecto, usa una constante para ello.

- Se implantará un constructor que con los siguientes parámetros
  - nombre (obligatorio)
  - DNI (obligatorio)
  - sexo (obligatorio)
  - edad (opcional)
  - peso (opcional)
  - altura (opcional)

- Los métodos que se implementaran son:
  - calcularIMC(): calculara si la persona esta en su peso ideal (peso en kg/(altura^2  en m)), si esta fórmula devuelve un valor menor que 20, la función devuelve un -1, si devuelve un número entre 20 y 25 (incluidos), significa que esta por debajo de su peso ideal la función devuelve un 0  y si devuelve un valor mayor que 25 significa que tiene sobrepeso, la función devuelve un 1. Te recomiendo que uses constantes para devolver estos valores.
  - esMayorDeEdad(): indica si es mayor de edad, devuelve un booleano.
  - validaDNI(): Invocado en el constructor. Comprueba si el DNI es correcto. No se pueden asignar DNIs incorrectos.

- Crea ahora una clase menú que permita gestionar personas.

- Crea el programa principal que invoca al menú.

## js0507. Persona II
Sobre el mismo ejercicio, vamos a crear la clase PersonaObservable. Este será un objeto en el que las propiedades van a ser setters que:

- Asignarán el valor a un atributo. Los atributos tendrán ahora el prefijo_
- Llamarán a una función listener que se podrá asignar con un método.

Aquí tenéis un ejemplo de como podría quedar.
```javascript
"use strict";

const persona = {

    // Este será el atributo del objeto donde se guarda el nombre
    _nombre: '',

    // Esto se podrá asignar a una función que llevará a cabo
    // la acción que nos interese
    listener: undefined,

    // Setter para la propiedad nombre
    set nombre(n) {
        this._nombre = n;
        if(listener) this.listener('nombre', n);
    }    

    // getter Para la propiedad nombre
    get nombre() {
      return _nombre;
    }
};

// Asigno el listener a una función que muestra un mensaje
// Estoy usando la sintaxis de función flecha que es más compacta
// pero se podría utilizar otra sintaxis si os resulta más claro.
persona.listener = (propiedad, valor) => console.log(propiedad + ' = ' + valor);

// Al asignar la propiedad, se muestra por consola
persona.nombre = "Paco";
```

## js0508. ValorObservable
Ahora vamos a crear un objeto similar a lo que sería un ObservableString de java. El objetivo es crear un objeto que tiene un valor, un listener, un setter y un getter de modo que cuando se cambia el valor se llama al listener con el nuevo valor. Los atributos serán:

- El valor. Lo guardaremos en un atributo con prefijo _
- setter/getter para obtener y asignar el valor
- Cuando se llama al setter, se asigna el valor y se llama al listener con el nuevo valor.
- función para asignar un listener. 
- Un atributo donde guardar el listener.

Crea el objeto como una constante y después crea un pequeño programa donde el listener muestre el nuevo valor por consola. Asigna varios valores y comproba que se muestra correctamente.

