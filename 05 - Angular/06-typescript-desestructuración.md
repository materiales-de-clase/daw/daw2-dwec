# Desestructuración
La sintaxis de desestructuración es una expresión de JavaScript que permite extraer valores de arrays o propiedades de objetos en distintas variables. Lo vamos a ver con algunos ejemplos.

## Objetos
Para el caso de objetos, vamos a partir del siguiente interfaz:

```ts
    interface Reproductor {
        volumen: number;
        segundo: number;
        cancion: string;
        detalles: Detalles;
    }

    interface Detalles {
        autor: string;
        año: number;
    }

    const reproductor: Reproductor = {
        volumen: 10,
        segundo: 24,
        cancion: 'El título',

        detalles: {
            autor: 'La Paca',
            año: 2022
        }
    }
```

Si queremos referenciar a varios de los atributos para hacer algo con ellos, nos encontramos con que tenemos que hacer un montón de referencias redundantes. Por ejemplo, en el siguiente ejemplo mostramos algunas de las propiedades en el objeto reproductor:

```ts
    console.log('El volumen actual : ', reproductor.volumen);
    console.log('El segundo actual : ', reproductor.segundo);
    console.log('La cancion actual : ', reproductor.cancion);
    console.log('El autor es: ', reproductor.detalles.autor);
```

La desestructuración nos permite definir nombres de variables más cortos que podemos utilizar para referirnos posteriormente a esos atributos. Para este caso, podríamos hacer lo siguiente:

```ts
    // Con esta línea puedo referneciar a los atributos más adelante sin la
    // referencia al objeto
    const { volumen, segundo, cancion, detalles } = reproductor;
    const { autor, año } = detalles;

    // Aquellas propiedades que he definido en la línea anterior las
    // puedo referenciar directamente sin el objeto
    console.log('El volumen actual : ', volumen);
    console.log('El segundo actual : ', segundo);
    console.log('La cancion actual : ', cancion);
    console.log('El autor es: ', autor);
```

## Arrays
Lo mismo que hemos hecho anteriormente para los objetos podemos hacerlo con un array. Es decir, podemos definir constantes para referenciar directamente a las posiciones de un array. Lo vamos a ver con un ejemplo.

Para referenciar a los valores en las posiciones de un array podemos utilizar la siguiente sintaxis

```ts
    const nombres: string[] = [ 'Paco', 'Manolo', 'Eva' ];

    console.log(nombres[1]);
    console.log(nombres[2]);
    console.log(nombres[3]);
```

Vamos a ver como quedaría si desestructuramos el array. De modo que podamos referenciar a cada posición con una constante.

```ts
    const nombres: string[] = [ 'Paco', 'Manolo', 'Eva' ];
    
    // La posición importa. n1 referencia al primer elemento
    // n2 Referencia al segundo elemento
    const [ n1, n2, n3 ] = nombres;

    // También sería perfectamente válido omitir nombres que no se van 
    // a utilizar del siguiente modo. En este caso, otro_n3 referencia
    // a la tercera posición
    const [ , , otro_n3 ] = nombres;

    console.log(n1);
    console.log(n2);
    console.log(n3);
```
## Argumentos de funciones
Esto es algo que podemos hacer en las funciones para simplificar el acceso a las propiedades de objetos que recibamos como argumento. Vamos a verlo con un ejemplo.

Aquí tenemos un objeto con dos atributos. Lo normal es que a esto le saquemos más partido con objetos que tengan muchos atributos, pero para el ejemplo va a ser perfecto.
```ts
    interface Producto {
        desc: string;
        precio: number;
    }
```
La siguiente función, permite calcular la suma total del precio de todos los productos. Para ello, recibe un array de productos.
```ts
    function calcularTotal( productos: Producto[]): number {
        
        let total = 0;

        productos.forEach( (producto: Producto) => {
            total += producto.precio;
        });

        return total;
    }
```

Finalmente, aquí tenemos un ejemplo de uso de dicha función.
```ts

    // Creo un par de objetos de tipo producto
    const telefono: Producto = {
        desc: 'Motorola', 
        precio: 10
    }

    const tableta: Producto = {
        desc: 'Vivo', 
        precio: 102
    }

    // Creo un array que voy a pasar como argumento con dichos objetos
    const articulos = [ telefono, tableta ];

    // Llamo a calcular el total
    const total = calcularTotal(articulos);

    // Lo muestro
    console.log('Total', total);

```
Pero hemos hablado de que íbamos a hacer una desestructuración de parámetros de modo que fuera más sencillo acceder a los mismos. El cambio lo vamos a hacer dentro de la función. La siguiente función sería equivalente

```ts
    function calcularTotal( productos: Producto[]): number {
        
        let total = 0;

        // OJO a las llaves. Defino constantes para acceder a los atributos 
        // de producto. El nombre de las constantes coincide con el nombre 
        // de las propiedades.
        productos.forEach( ({ precio }) => {
            
            // Ya no necesito referenciar a producto.precio. Puedo
            // directamente referenciar a precio
            total += precio;
        });

        return total;
    }
```
## Valores de retorno de funciones
Del mismo modo que podemos desestructurar los parámetros de entrada, podemos hacer lo mismo con los valores de retorno de funciones. Veámoslo con un ejemplo:

```ts

    // Esta función hace una división y retorna un array con el cociente
    // y el resto
    function dividir(dividendo: number, divisor: number): number[] {
        
        const cociente = dividendo;
        const resto = dividendo % divisor;

        // Retorna un array
        return [ cociente, resto ];
    }
```

Para acceder al resultado podemos utilizar el siguiente código:
```ts
    // El nombre da igual, ya que al ser un array lo que importa es la posición
    const [ cociente, resto ] = dividir(10, 3);

    // Muestra el resultado
    console.log('Cociente: ', cociente);
    console.log('Resto: ', resto);
```

