# Modulos
Como se ha dicho anteriormente, los módulos agrupan entidades que están relacionadas entre sí. Un a aplicación va a tener siempre como mínimo un módulo que estará definido en el fichero app-module.ts.

Los módulos permiten:

- **Encapsulación**. Ya que cada módulo va a exportar únicamente los recursos que puedan ser utilizados desde otros módulos. 
- **Facilitan la carga perezosa**. Solo se cargan los módulos cuando se utilizan, es decir, podemos cargar los módulos bajo demanda en lugar de tener toda nuestra aplicación cargada en memoria desde el inicio.

## app.module.ts
Vamos ahora a echar un vistazo a la estructura de un módulo a partir del módulo que ha sido generado por Angular al crear nuestro proyecto. Lo primero que se quiere destacar es que **un módulo es una clase TypeScript con un Decorador**. 

```ts
// Estos imports son similares a los que se hacen en JavaScript. Aquí se importan 
// los recursos externos. Vamos a intentar ordenarlos siempre del siguiente modo
// 1. En primer lugar los módulos de Angular
// 2. Los módulos de terceros
// 3. Nuestros módulos.
// Además, los ordenaremos alfabéticamente y separaremos estas secciones con espacios. 
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Este decorador de la clase es lo que hace que sea un módulo. Describe las 
// características de nuestro módulo
@NgModule({
  declarations: [   // En este array van a aparecer los COMPONENTES que se
                    // declaran en este módulo. Esto no implica exportación
    AppComponent
  ],
  exports: [        // En este array aparecen los COMPONENTES que se exportan
                    // desde este módulo. Estos componentes podrán ser utilizados
                    // en otros módulos.
  ],
  imports: [        // Aquí se indican los MODULOS que se importan en este módulo
                    // para su utilización. 
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],            // Proveedores de servicio. Hablaremos sobre esto más adelante.
  bootstrap: [AppComponent] // Es el componente raíz que Angular crea e inserta en 
                            // el index.html. Por defecto nos crea el AppComponent
})
export class AppModule { }
```

## Organización
Se recomienda organizar la información en directorios. Para nosotros, **cada módulo estará en un directorio independiente** y contendrá todos los recursos que pertenecen a dicho módulo. 

## Crear un módulo
Podemos crear los módulos manualmente o utilizando el AngularCLI. Por simplificar la tarea, vamos a crearlos siempre utilizando AngularCLI y vamos luego a analizar lo que ha pasado. Volviendo a nuestra aplicación, la vamos a dividir en los siguientes módulos (pongo nombres en inglés para que quede más compacto):

- auth: Contendrá todo lo que esté relacionado con la autenticación de usuarios. Por ejemplo, la página de login.
- dashboard: Nuestro dashboard
- shared: En este módulo vamos a poner aquellos componentes, servicios, etc que vayan a ser comunes a toda la aplicación. 
- tasks: Este va a ser el módulo de gestión de tareas.
- tasks-state: Modulo de gestión de estados de tareas. (Este módulo podría estar integrado en las tareas pero lo vamos a separar para ejercitar más los módulos)
- tasks-type: Modulo de gestión de estados de tipos de tareas. (Este módulo podría estar integrado en las tareas pero lo vamos a separar para ejercitar más los módulos)
- users: Módulo de gestión de usuarios

Vamos a distinguir dos tipos de módulos:

- Los que van a contener páginas. Estos módulos necesitarán configurar el enrutamiento.
- Los que van a contener únicamente componentes. Para estos no hace falta enrutamiento.

Para crear los módulos que necesitan enruramiento vamos a utilizar una secuencia de comandos como la siguiente:

```cmd
C:\taskmanApp> ng generate module auth --routing
CREATE src/app/auth/auth-routing.module.ts (247 bytes)
CREATE src/app/auth/auth.module.ts (272 bytes)
```
El parámetro --routing hace referencia a que cree el archivo de configuración de rutas. Veremos más adelante qué es esto. Si nos fijamos, veremos que este archivo solo se crea en aquellos módulos que contienen páginas.

El único módulo que no tendría páginas sería shared. Este módulo lo vamos a crear del siguiente modo:
```cmd
C:\taskmanApp> ng generate module shared
CREATE src/app/shared/shared.module.ts (192 bytes)
```
#### Ejercicio - Crear los módulos
Crea ahora el resto de módulos de la aplicación basándote en la información de este apartado.


## Importar un módulo
Para importar un módulo, tendríamos que incluirlo en el import del *.module.ts desde el que queramos referenciarlo. Por ejemplo, para importar el módulo shared a nuestro módulo tasks tendríamos que modificar el archivo tasks.module.ts para que luciera como sigue:

```ts
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TasksRoutingModule } from './tasks-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [],
  
  // Los imports los haremos en el siguiente orden
  // 1. Los de Angular
  // 2. Los de terceros
  // 3. Los de nuestra aplicación
  // ! siempre en orden alfabético
  imports: [

    // Este módulo ofrece algunas funcionalidades para trabajar con plantillas HTML
    CommonModule,

    // Aquí se define el enrutamiento de este módulo
    TasksRoutingModule,

    // Módulo Shared
    SharedModule
  ]
})
export class TasksModule { }
```

En nuestro módulo raíz también necesitaríamos importar aquellos módulos que queramos referenciar desde los componentes dentro de dicho módulo. De momento no vamos a incluir ninguno, ya que vamos a explicar directamente como hacer la carga perezosa y lo vamos a a ver en ese momento.

# Estructura de directorios
Dentro de cada módulo de los que hemos creado, vamos a crear los siguientes directorios:

- components: Componentes. Pueden ser partes de páginas reutilizables. Por ejemplo una tabla o un combo.
- interfaces: Aquí pondremos los interfaces que el módulo necesite para funcionar.
- pages: Los componentes que representan páginas de la aplicación.
- pipes: Si creáramos algún pipe lo pondríamos aquí. 
- services: Si creamos algún servicio iría en este directorio.
- guards: Para poner los guards implementados en nuestra app.

Si de alguno de ellos no hay nada, no es necesario que lo creemos, pero de momento vamos a hacer el ejercicio de crearlos en el módulo tasks. Esta es una forma de tener el código fuente organizado por tipo de componente y hace más fácil 
identificar la información en nuestra aplicación.
