# Angular Material
Un poco de interfaz gráfico con Angular Material

https://material.angular.io/

### Instalar Angular Material (A partir de aquí heroesApp)
Para instalar angular material vamos a utilizar npm

Proceso de instalación aquó
https://material.angular.io/guide/getting-started

```bash
ng add @angular/material
i Using package manager: npm
√ Found compatible package version: @angular/material@14.2.5.
√ Package information loaded.

The package @angular/material@14.2.5 will be installed and executed.
Would you like to proceed? Yes
√ Packages successfully installed.
? Choose a prebuilt theme name, or "custom" for a custom theme: Indigo/Pink        [ Preview:
https://material.angular.io?theme=indigo-pink ]
? Set up global Angular Material typography styles? Yes
? Include the Angular animations module? Include and enable animations
UPDATE package.json (1107 bytes)
√ Packages installed successfully.
UPDATE src/app/app.module.ts (423 bytes)
UPDATE angular.json (3102 bytes)
UPDATE src/index.html (577 bytes)
UPDATE src/styles.css (181 bytes)
```

A partir de este momento vamos a utilizar Angular Material

Se crea un módulo material al tener que trabajar con muchos módulos o componentes de material. El objetivo es que nuestro app.module.ts sea más fácil de mantener. crece solo lo que es necesario y todos los componentes que necesitamos están centralizados en el módulo material que es el que importará los componentes. Es decir Material incluye los componentes de  material App Module incluye el módulo de Material.


### Instalación de angular flex-layout
Para organizar información en pantalla
Es una alternativa al sistema de grid de Angular Material
de modo que se puede utilizar el Grid de Angular o se puede utilizar flex-layout

https://github.com/angular/flex-layout

// Instala el módulo flex-layout
npm install @angular/flex-layout @angular/cdk

up to date, audited 915 packages in 1s

121 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities

