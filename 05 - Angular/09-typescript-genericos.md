# Genéricos
En el siguiente apartado vamos a hablar de los genéricos en TypeScript. Estos están disponibles en otros lenguajes de programación como Java y permiten utilizar el mismo código para trabajar con diferentes tipos de datos.

En el siguiente ejemplo definimos una función genérica que será implementada para diferentes tipos de datos. La sintaxis es similar a la que se usa en Java.

```ts

    // Aquí tenemos una declaración de una función que 
    // recibe dos parámetros de tipo T y devuelve un valor de tipo T
    function suma<T>(arg1: T, arg2: T): T {
        return arg1 + arg2;
    }
```
Para invocar a la función podemos ahora utilizar un código como el siguiente. Este código permite invocar a una versión concreta.
```ts
    // Invocamos a la función para sumar números de tipo number
    const rnumber = suma<number>(10, 20);

    // Aquí tenemos la misma función, pero funcionando con bigint
    // Vemos como cambia lo que hay entre <> y los literales
    //  han de ser bigint también
    const rbigint = suma<bigint>(10n, 20n);
```