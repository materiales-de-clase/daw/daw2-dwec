# Importaciones y exportaciones
Del mismo modo que en JavaScript podemos exportar e importar funciones, constantes, clases, etc. en TS podemos también hacer exportación e importanción de nuestros objetos. Veamos el siguiente ejemplo:

Aquí tenemos un archivo que hace una exportación
```ts
    // Contenido del archivo alumno.ts
    export interface Alumno {
        nombre: string;
        nota: number;
    }
   
    const clase: Alumno:[] = [];

    export function calcularNotaMedia(alumnos: Alumno[]): number {

        // El total
        let total = 0;

        // Para cada alumno. Desestructura el objeto que recibe como parámetro
        alumnos.forEach(({ nota }) => {
            total += nota;
        });

        // Retorna la media
        return total/alumnos.length;
    }

```
Aquí importamos y ejecutamos
```ts
    // Contenido de main.ts
    import { Alumno, calcularNotaMedia } from './alumno';

    // Ahora puedo hacer uso el interfaz
    const alumnos: Alumno:[] = [
        {
            nombre: 'Paco',
            nota: 10
        },
        {
            nombre: 'Manolo',
            nota: 5
        }
    ];

    // Calcula la nota media
    const media = calcularNotaMedia(alumnos);

    // Muestra la nota media
    console.log("Media:", media);
```