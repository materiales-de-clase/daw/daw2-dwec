# Componentes
Cuando hablamos de componentes, nos referimos a una clase que representa:

- Una página en nuestra aplicación
- Un componente que puede ser insertado en una página

## Crear las páginas en nuestra aplicación
En este apartado nos vamos a centrar en crear lo que serían las páginas de nuestra aplicación. Más adelante crearemos algún componente para ver cómo se crearía un componente para posteriormente utilizarlo en otras páginas.

Las páginas que vamos a crear son:

- auth/pages/login
- dashboard/pages/dashboard
- tasks/pages/listado
- tasks/pages/editar
- tasks/pages/ver


Para crear una página tendríamos que ejecutar la siguiente secuencia de comandos:

```cmd
C:\MIPROYECTO\> ng generate component auth/pages/Login --skip-tests --style="none"
```

Recuerda utilizar camelcase a la hora de escribir el nombre del componente/página. Esto ayudará a Angular a asignar el nombre correctamente.

En el comando anterior :
- auth/pages/login: indica el directorio donde vamos a crear el componente, que en este caso es una página. Si el directorio está por debajo de un módulo, se añadirá la página recien creada al módulo.
- --skip-tests: Si no pasamos este argumento se va a crear un archivo TypeScript relacionado con las pruebas. Nosotros no vamos a trabajar con esto, de modo que podemos omitirlo.
- --style="none": Esto indica que no vamos a querer que cree una hoja de estilos para esta página. Nuestra página va a coger los estilos de la aplicación

Si el comando se ejecuta correctamente se habrán creado 2 ficheros:

- login.component.html
- login.component.ts

Al tratarse de un componente, siempre lleva esa extensión component. Esto es una convención de typescript que además ayuda a distinguir el tipo de contenido de cada fichero. Además, separa en este caso el código HTML de la implementación.

## Estructura de un componente
A continuación tenemos el código de lo que podría ser una página de login. Como vemos, contiene únicamente código HTML. Más adelante entraremos en los detalles.

### AppComponent
#### appcomponent.ts
Contenido del archivo appcomponent.ts:

```ts
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',     // Este atributo indica el elemento, el 
                            // selector que identifica este componente.
                            // Se puede utilizar esto en la página para 
                            // insertarlo.
  templateUrl: './app.component.html',  // ruta relativa a la plantilla a 
                                        // emplear para el componente. 
                                        // Contiene el HTML que se va a 
                                        // insertar en el punto
                                        // donde está el componente.
                                        // También podríamos utilizar el 
                                        // atributo template para pasar el 
                                        // código html como una cadena de 
                                        // texto
  styleUrls: ['./app.component.css']    // EL CSS que se va a aplicar a 
                                        // este componente.
})
export class AppComponent {
  title: string = 'miproyecto';     // Este será el título de mi aplicación
                                    // Aquí defino las propiedades de mi 
                                    // componente. Puedo añadir, cambiar 
                                    // lo que sea necesario 
                                    // para representar el estado de mi
                                    // componente


numero: number = 0;                 // Este atributo lo vamos a utilizar 
                                    // en un ejemplo

sumar(v : number) {                 // Un par de métodos de ejemplo
this.numero++;
}

restar(v: number) {
this.numero--;
}                                    
}
```

#### appcomponent.html
Representa la plantilla HTML. En esta plantilla tenemos el HTML que se va a mostrar.

```html
<h1>{{ titulo }}</h1>

<button (click)="numero = numero + 1">+1</button>
<span>{{ numero }} </span>
<button (click)="numero = numero - 1">-1</button>
```

Aquí vemos:
- Estas llaves permiten mostrar el valor de una propiedad. Entre llaves tenemos una expresión que se evalúa y se muestra el resultado. {{ expresión }}
- (click)="numero = numero + 1" : De este modo podemos asignar un gestor de eventos en línea dentro de la plantilla. 

Del mismo modo que he referenciado a un atributo, podría referenciar a un método. 

```html
<h1>{{ titulo }}</h1>

<button (click)="sumar(1)">+1</button>
<span>{{ numero }} </span>
<button (click)="restar(1)">-1</button>
```



## index.html
Ahora veamos el archivo src/**index.html** de nuestra aplicación.

```html
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Bases</title>
  <base href="/">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
</head>
<body>
  <!-- 
    Vemos como aquí se ha incluido el elemento app-root. 
    Este coincide con el nombre del selector que hemos puesto a nuestro componente. De modo que, nuestro componente se va a insertar aquí 
  -->
  <app-root></app-root>

</body>
</html>
```

### Login
Vamos a ver ahora un ejemplo para la pantalla de login
#### login.component.html
```html
<div class="container mt-5">
    <div class="d-flex justify-content-center h-100">
        <div>
            <div class="d-flex justify-content-center">
                <div class="brand_logo_container mb-2">
                    <img src="assets/logo.png" alt="Logo">
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <!-- Inicializa el modelo de Angular -->
                <form method="post" (ngSubmit)="login()">
                    <div class="input-group mb-2">
                        <span class="input-group-text rounded-0 bg-grey"><i class="bi bi-person"></i></span>
                        <input [(ngModel)]="credenciales.login" name="usuario" class="form-control input-lg rounded-0" placeholder="Usuario">
                    </div>

                    <div class="input-group mb-2">
                        <span class="input-group-text rounded-0 bg-grey"><i class="bi bi-key"></i></span>
                        <input [(ngModel)]="credenciales.pass" type="password" name="password" class="form-control input-lg rounded-0" placeholder="Contraseña">
                    </div>
                    <div class="mb-2">
                        <span class="text-danger" *ngIf="errorInicioSesion">No se ha podido iniciar sesión</span>
                    </div>
                    <!-- TODO Mensaje de error -->
                    <div class="d-flex justify-content-center mt-3">            
                        <button type="submit" name="submit" class="btn bg-primary text-white">Entrar</button>
                    </div>
                </form>
            </div>
        </div>       
    </div>
</div>
```

#### login.component.ts
El código TypeScript estará en el fichero TypeScript. En este caso incluyo el código con la implementación del componente. Aunque lo comentaremos con más detalles posteriormente.

```ts
import { Component, OnInit } from '@angular/core';
import { AutenticacionService } from '../../services/autenticacion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  // Credenciales en la página
  credenciales = {

    login: '',
    pass: ''

  };

  errorInicioSesion: boolean = false;

  constructor(
    
    private router: Router,

    private autenticacionService: AutenticacionService

  ) { }

  ngOnInit(): void {
  }

  login() {

    this.autenticacionService.iniciarSesion(this.credenciales.login, this.credenciales.pass)
    .subscribe(resultado => {
      if(resultado) {
        this.router.navigate([ '/dashboard' ]);
      } else {
        this.errorInicioSesion = true;
      }
    });
  }
}
```

## Buenas prácticas
Esto lo podemos aplicar sobre todo si creamos nuestros componentes manualmente:

- Vamos a utilizar app- como prefijo para indicar que es un componente personalizado de la aplicación
- El nombre tendrá el siguiente formato: nombre.component.ts. Component indica que es un componente.
- Si hay varias palabras se separan con guión. nombre-segundo.component.ts
- Utilizar directorios para organizar la información relacionada


### Ejercicio 
Hasta aquí hemos creado la página de login. Crea ahora el resto de páginas de la aplicación.

- dashboard/pages/dashboard
- tasks/pages/listado
- tasks/pages/editar
- tasks/pages/ver


