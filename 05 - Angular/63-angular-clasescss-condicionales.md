# Clases CSS condicionales
Manipulación de clases css 


## Utiliando class

```html
<!--
    En este caso, se comprueba si la región coincide con la región activa. Si se cumple la condición
    se añade la clase btn-primary. Permite resaltar un objeto o cambiar su estilo gráfico en función del estado del modelo
    -->
<div class="row">
    <div class="col">
        <button *ngFor="let region of regiones" 
                class="btn btn-outline-primary" 
                [class.btn-primary]="region === regionActiva"
                (click)="activarRegion(region)">
            {{ region }}
        </button>
    </div>
</div>
```

## Utilizando ngClass
Permite especificar pares clase condición. La clase se añade al componente si y solo si se cumple la condición.

```html
<div class="row">
    <div class="col">
        <button *ngFor="let region of regiones" 
                class="btn btn-outline-primary" 
                [ngClass]="{
                    'btn-primary': regionActiva === region,
                    'btn-outline-primary': regionActiva !== region
                }"
                (click)="activarRegion(region)">
            {{ region }}
        </button>
    </div>
</div>
```

## Clases CSS condicionales
En este ejemplo lo hacemos con código JavaScript

```html
<hr>
<h5>Seleccione la región</h5>
<div class="row">
    <div class="col">
        <button *ngFor="let region of regiones" 
                [class]="(region === regionActiva)?'btn btn-primary': 'btn btn-outline-primary'" 
                (click)="activarRegion(region)">
            {{ region }}
        </button>
    </div>
</div>
```

Esto se puede hacer también definiendo el código en una función a la que le pasemos como parámetro la region



