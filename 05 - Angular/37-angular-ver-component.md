# Ver component
En esta sección, vamos a analizar como sería la implementación de ver una tarea.


## html
Este código HTML es solo un ejemplo. Muestra como sacar información del modelo.

```html
<div class="row">
    <span>Titulo: {{ tarea.titulo }}</span>
    <span>Informador: {{ tarea.informador }}</span>
    <span>Asignado: {{ tarea.asignado }}</span>
</div>

<div class="row ">
    <span class="col-9"></span>
    <button class="btn btn-primary col-2 m-3" routerLink="/tareas">Volver</button>    
</div>
```

## TypeScript
A continuación el TypeScript necesario para ver una tarea.

```ts
  tarea! : Tarea;  

//-------------------------------------------------------------------------------------
// Inicialización
//-------------------------------------------------------------------------------------

constructor(

  private activatedRoute    : ActivatedRoute,
  private router            : Router,

  private tareasService     : TareasService

) { }

/**
 * Inicialización de la página
 */
ngOnInit(): void {

  // Carga la tarea
  this.cargarTarea();

}


//-------------------------------------------------------------------------------------
// Funciones de persistencia. Permiten guardar y recuperar tareas
//-------------------------------------------------------------------------------------

/**
 * Cuando estamos editando, este método carga la tarea que estamos editando en el formulario
 */
 cargarTarea() {
    
  // Si estamos en modo edición, obtiene los parámeros
  // y carga los datos
  this.activatedRoute.params
    
    // Usamos switchMap, que permite cambiar el id (el parámetro de entrada)
    // por la tarea
    .pipe(

        switchMap( ({id}) => this.tareasService.getTareaPorId(id) ),
        
        // Este pipe muestra lo que viene
        tap(console.log)
    )
    // Finalmente, este subscribe recibe el resultado, que será el objeto
    .subscribe(respuesta => {
      
      if(respuesta.ok) {

        // Carga los datos
        this.tarea = respuesta.datos;

      } else {
        this.router.navigate([ '/tareas/listado' ]);
      }
    });
  }

```