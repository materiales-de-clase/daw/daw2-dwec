# Decoradores
Los decoradores podemos considerarlos una forma de aportar información adicional a nuestras clases, métodos, etc. Sirven para expandir o añadir funcionalidades. Son similares en la forma de utilización a las anotaciones de Java. En el siguiente enlace tenemos información adicional sobre los decoradores:

- https://www.typescriptlang.org/docs/handbook/decorators.html#class-decorators

Además, se trata de una característica experimental, por lo que en nuestro fichero tsconfig.json tendremos que descomentar el siguiente parámetro de configuración:

```json
"experimentalDecorators": true
```

En principio no vamos a necesitar profundizar en los decoradores ni en crear nuestros propios decoradores ya que nos vamos a limitar a utilizar aquellos que necesitemos durante la creación de nuestras aplicaciones en Angular.

## Ejemplo de decorador tomado de la web

```ts
function reportableClassDecorator<T extends { new (...args: any[]): {} }>(constructor: T) {
    return class extends constructor {
        reportingURL = "http://www...";
    };
}

@reportableClassDecorator
class BugReport {
    type = "report";
    title: string;

    constructor(t: string) {
        this.title = t;
    }
}

const bug = new BugReport("Needs dark mode");
console.log(bug.title); // Prints "Needs dark mode"
console.log(bug.type); // Prints "report"
 
// Note that the decorator _does not_ change the TypeScript type
// and so the new property `reportingURL` is not known
// to the type system:
bug.reportingURL;
```