# Crear un proyecto
En este apartado vamos a crear nuestro proyecto. 

## Antes de comenzar
Antes de comenzar, vamos a verificar que tenemos instalado nuestro cliente de Angular utilizando el siguiente comando.

```batch
C:\MIDIR\> ng version

     _                      _                 ____ _     ___
    / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
   / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
  / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
 /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
                |___/


Angular CLI: 14.2.6
Node: 16.17.0
Package Manager: npm 8.15.0
OS: win32 x64

Angular:
...

Package                      Version
------------------------------------------------------
@angular-devkit/architect    0.1402.6 (cli-only)
@angular-devkit/core         14.2.6 (cli-only)
@angular-devkit/schematics   14.2.6 (cli-only)
@schematics/angular          14.2.6 (cli-only)
```

En nuestro caso, podemos ver que vamos a trabajar con la versión 14 o superior de Angular JS que es la que está disponible en el momento de escribir esta sección. 

## Crea el proyecto
Para crear el proyecto, sitúate en el directorio donde quieres hubicar el código fuente del proyecto y escribe la siguiente secuencia:

```batch
C:\MIDIR\> ng new taskmanApp
? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? CSS
CREATE taskmanApp/angular.json (2943 bytes)
CREATE taskmanApp/package.json (1042 bytes)
CREATE taskmanApp/README.md (1064 bytes)
CREATE taskmanApp/tsconfig.json (863 bytes)
CREATE taskmanApp/.editorconfig (274 bytes)
CREATE taskmanApp/.gitignore (548 bytes)
CREATE taskmanApp/.browserslistrc (600 bytes)
CREATE taskmanApp/karma.conf.js (1428 bytes)
CREATE taskmanApp/tsconfig.app.json (287 bytes)
CREATE taskmanApp/tsconfig.spec.json (333 bytes)
CREATE taskmanApp/.vscode/extensions.json (130 bytes)
CREATE taskmanApp/.vscode/launch.json (474 bytes)
CREATE taskmanApp/.vscode/tasks.json (938 bytes)
CREATE taskmanApp/src/favicon.ico (948 bytes)
CREATE taskmanApp/src/index.html (296 bytes)
CREATE taskmanApp/src/main.ts (372 bytes)
CREATE taskmanApp/src/polyfills.ts (2338 bytes)
CREATE taskmanApp/src/styles.css (80 bytes)
CREATE taskmanApp/src/test.ts (749 bytes)
CREATE taskmanApp/src/assets/.gitkeep (0 bytes)
CREATE taskmanApp/src/environments/environment.prod.ts (51 bytes)
CREATE taskmanApp/src/environments/environment.ts (658 bytes)
CREATE taskmanApp/src/app/app-routing.module.ts (245 bytes)
CREATE taskmanApp/src/app/app.module.ts (393 bytes)
CREATE taskmanApp/src/app/app.component.html (23115 bytes)
CREATE taskmanApp/src/app/app.component.spec.ts (1085 bytes)
CREATE taskmanApp/src/app/app.component.ts (214 bytes)
CREATE taskmanApp/src/app/app.component.css (0 bytes)
√ Packages installed successfully.
    Directory is already under version control. Skipping initialization of git.
```

Durante la instalación se han planteado las siguientes cuestiones:

- **Would you like to add Angular routing?** : Contestamos que sí. Aunque es algo que podemos hacer nosotros manualmente, vamos a estudiar como se hace basándonos en el fichero que se genera de forma automática.
- **Which stylesheet format would you like to use?** : Da varias opciones. Nosotros vamos a trabajar directamente CSS. Después instalaremos bootstrap o algún otro framework para utilizarlo para desarrollar la aplicación.

Una vez que termine todo el proceso, monta el directorio de la aplicación en tu editor de código.

## Levantar la aplicación
Antes de continuar, es bueno comprobar que nuestra aplicación funciona y somos capaces de acceder a ella a través de la URL. Desde el directorio recién creado vamos a ejecutar el siguiente comando:

```batch
C:\MIDIR\taskmanApp\> ng serve -o 

√ Browser application bundle generation complete.

Initial Chunk Files   | Names         |  Raw Size
vendor.js             | vendor        |   2.10 MB |
polyfills.js          | polyfills     | 318.06 kB |
styles.css, styles.js | styles        | 210.09 kB |
main.js               | main          |  50.17 kB |
runtime.js            | runtime       |   6.52 kB |

                      | Initial Total |   2.67 MB

Build at: 2022-10-30T17:19:34.213Z - Hash: 97da590718630904 - Time: 9342ms

** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **


√ Compiled successfully.
```

Este comando ha llevado a cabo las siguientes tareas:

- Traduce el código TS a JS
- Monta un servidor mediante webpack
- Si se indica el -o lo abre en cuanto está disponible utilizando el navegador por defecto

Cuando hagamos cambios a nuestra aplicación se actualizará automáticamente de modo que cuando vayamos a trabajar con angular **tendremos siempre el servidor levantado**.

## Notas sobre el proyecto
### El archivo gitignore
El proyecto se genera con un archivo gitignore que viene configurado para que en caso de que usemos control de versiones con git no subamos archivos que se generan o que son dependencias de nuestro proyecto descargadas de internet. No se recomendaría eliminar nada salvo que se esté seguro de qué se está haciendo, pero sí se puede añadir algún recurso a ignorar si se considera necesario, aunque en nuestro caso no vamos a trabajar con este archivo. 

### El directorio node_modules
Contendrá todos los paquetes instalados con npm de forma automática al crear el proyecto o posteriormente por nosotros si lo hemos necesitado. Este directorio no se sube al repositorio git, por lo tanto, cuando hagamos un clone del repositorio necesitaremos instalar las dependencias en la nueva réplica. Para descargar las dependencias podemos ejecutar el siguiente comando:

```
$ npm install
```

Al ejecutarlo desde el directorio del proyecto se instalarán en ese equipo todas las dependencias necesarias para hacer funcionar nuestro proyecto.

### La caché de Angular
Angular puede cachear ciertos recursos. Para limpiar la cache de nuestro proyecto podemos utilizar el comando.

```
$ ng cache clean
```