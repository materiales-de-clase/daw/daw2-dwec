# Arrays
Vamos a ver ahora como se tratarían los arrays en TypeScript

## Declaración de un array
La declaración de un array se haría del siguiente modo:

```ts
    // Este array sería similar al que declaramos en JavaScript. No hay restricciones de tipo. Puedo poner absolutamente cualquier cosa.
    let cosas: any[] = [];

    // En este ejemplo además lo inicializo
    let cosas = [ 1, 'cadena', 1.3, true ];

    // En TypeScript podemos indicar que el array es de un tipo determinado. En el siguiente ejemplo estoy definiendo un array de tipo string
    let colores: string[] = [];

    // Aquí hago una inicialización del array con varios valores de tipo cadena
    let colores: string[] = [ 'rojo', 'verde' ];

    // Puedo añadir si así lo deseo algún color al array del mismo modo que 
    // haría en JavaScript
    colores.push('azul');
    colores.push(10);       // Este código en JavaScript sería válido. Aquí falla.
                            // por ser un array de cadenas
```

