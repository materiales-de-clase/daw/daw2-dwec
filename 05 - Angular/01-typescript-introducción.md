# Introducción a TypeScript
En este apartado vamos a hacer una breve introducción a TypeScript

## ¿Qué es TypeScript?
Es un **superset del estándar de ECMAScript**. Podemos afirmar que todo el código JavaScript es TypeScript, pero no al contrario. Algunas de las características son:

- Por debajo, es el mismo JavaScript
- Extiende características del lenguaje JavaScript
- Añade características que no incluye JavaScript
- El lenguaje TypeScript es mantenido por Microsoft
- En los archivos TypeScript se utiliza la extensión de archivo ts

## Características de TypeScript
A continuación tenemos algunas características de TypeScript

- Superset de JavaScript
- Tipado estricto y flexible
- Evita errores y facilita el mantenimiento
- Mejora la asistencia de los editores al tener las variables un tipo asociado
- Permite usar características modernas incluidas en otros lenguajes de programación

### Ejemplo tipado 
En el siguiente ejemplo se calcula el importe total utilizando JavaScript

```javascript
function calcularTotal(productos) {

    let total = 0;

    for(let p of productos) {
        total += p.precio;
    }

    return total;
}
```

Este es el mismo código pero utilizando TypeScript

```typescript
// La función incluye información sobre el interfaz de la función
// Indica los tipos de los parámetros de entrada y los de salida
function calcularTotal(productos: Producto[]): number {

    let total = 0;

    for(let p of productos) {
        total += p.precio;
    }

    return total;
}
```

Aquí tenemos un ejemplo de la típica funcion sumar implementada en TypeScript

```typescript
    function sumar(x: number, y: number): number {
        return x + y;
    }
```

## ¿Por qué Angular usa TypeScript?

- Para beneficiarse de las ventajas del uso de TypeScript
- Ayudas a la programación más precisas. Por ejemplo autocompletado. En el caso de JavaScript es imposible al no existir tipado de objetos. No se sabe el tipo de objeto que está en una variable hasta que no se ejecuta el programa.
- Podemos detectar errores en el momento de la escritura ya que al aportar más información el programador, esta información se puede utilizar para detectar estos errores.
- Nos permite aplicar inyección de dependencias.
