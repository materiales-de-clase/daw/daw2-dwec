# Tareas service
Hemos visto hasta ahora como crear una tabla tareas pero utilizando un array local. En este apartado vamos a descargar los datos desde el servidor.

## Crear el servicio
Para crear el servicio.

```cmd
C:\MIPROYECTO> ng generate service tasks/services/Tareas --skip-tests
```

## Tareas.interface.ts
En TypeScript, para facilitar trabajar con las peticiones y respuestas a servicios sin cometer errores, vamos a definir interfaces que describan estos interfaces. Para hacerlo vamos a recurrir a la web:

- [quicktype.io](https://quicktype.io/) 

Este sitio web lo vamos a utilizar para generar el interfaz que vamos a necesitar para trabajar con la URL del servicop getTareasPorTitulo. Hay que tener en cuenta, que siempre es posible que sea necesario adaptar el resultado a nuestras necesidades.

Permite evitar errores y facilitar que podamos trabajar con las peticiones de una forma uniforme a lo largo de toda la aplicación. Lo único que vamos a necesitar va a ser el JSON que queremos convertir, en nuestro caso, en interfaces TypeScript que nos van a permitir tratar las respuestas de forma clara y evitando errores. Tendremos que seguir los siguientes pasos:

1. Pegar el JSON completo para el que queremos crear el interface
2. Asignar el nombre que queramos. Este será el tipo del objeto de respuesta. El resto de los nombres de los interfaces se calcularán a partir de los atributos en el JSON.
3. Seleccionar el lenguaje TypeScript
4. Marcar la opción interfaces only

Existen extensiones de Quicktype para Visual Studio Code, pero en estos momentos solo vamos a trabajar con la web.

```ts
export interface TaskmanListaTareasResponse {
    ok:      number;
    mensaje: string;
    datos:   Tarea[];
}

export interface Tarea {
    id_tarea?:         number;
    titulo:            string;
    id_informador:     number;
    informador?:       string;
    id_asignado:       number;
    asignado?:         string;
    id_tipo_tarea:     number;
    tipo?:             string;
    id_estado:         number;
    estado?:           string;
    descripcion:       string;
    fecha_alta:        string;
    fecha_vencimiento: string;
    hora_vencimiento:  string;
}

```

## Importar HttpClient
El módulo HttpClient es el que ofrece la funcionalidad que necesitamos para hacer peticiones HTTP. Para importarlo:

```ts
import { HttpClient, HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
  ],
  
  imports: [
    // Servicio Http. Se requiere para llamar al servidor
    HttpClientModule,
  ]
})
export class TasksModule { }
```


## Método getTareasPorTítulo
Vamos ahora a definir este método en nuestro servicio.

```ts
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { Tarea, TaskmanListaTareasResponse} from '../interfaces/tarea.interface';

@Injectable({
  providedIn: 'root'
})
export class TareasService {

  // Ruta base para todas las llamadas al servicio
  // se toma de environment
  private taskmanBaseUrl = environment.taskmanBaseUrl;
  private debug = environment.debug;

  constructor(
    // Necesitamos este objeto para hacer peticiones. 
    private httpClient: HttpClient
  ) {}

  // Genera la url dado el nombre del script 
  private generarUrl(script: string) : string {
    return `${this.taskmanBaseUrl}/ajax.php?s=${script}${this.debug?"&__debug":""}`;
  }

  /**
   *  Dado el filtro, retorna las tareas que coinciden con el criterio
   */ 
  getTareasPorTitulo(filtro: string = '%'): Observable<TaskmanListaTareasResponse> {
    
    // Inicializa el objeto con la petición
    const argumentos = {
      filtro: (filtro == '%')?filtro:filtro+'%'
    };

    // Retorna un observable
    return this.httpClient.post<TaskmanListaTareasResponse>(this.generarUrl("_getTareasPorTitulo"), argumentos);
  }

  /**
   * Borra una tarea pasada la tarea
   */
  borrarTarea(tarea : Tarea): Observable<TaskmanTareaResponse> {
    
    // Inicializa el objeto con los argumentos de la petición
    const argumentos = {
      id: tarea.id_tarea
    };

    // Llama a eliminar la tarea
    return this.httpClient.post<TaskmanTareaResponse>(this.generarUrl("_deleteTarea"), argumentos);
  }
```

# Configuración del servidor
Será necesario editar el archivo .htaccess de nuestro sitio web. En dicho archivo incluiremos las siguientes líneas. Si no lo hacemos obtendremos errores al tratar de ejecutar llamadas a dicho servicio.

```shell
# Permite acceso al servicio desde estos orígenes
Header set Access-Control-Allow-Origin http://localhost:4200

# Con este atributo del encabezado se permiten todas las cabeceras
# En caso contrario no es posible, por ejemplo, recibir JSON
Header set Access-Control-Allow-Headers *
```

## Conclusiones
Hasta este punto, tenemos un servicio con un método que permite obtener la tabla de tareas por título. 




