# Crear un componente
En el siguiente ejemplo, vamos a crear un componente sencillo que permitirá incrementar y decrementar un contador.

## Archivos fuente del componente

### contador.component.html
```html
<h1>{{ titulo }}</h1>
<h3>La base es: <strong>{{ base }}</strong></h3>

<button (click)="sumar(base)">+ {{base}}</button>
<span>{{ numero }} </span>
<button (click)="restar(base)">- {{base}}</button>
```

### contador.component.ts
```ts
import { Component } from '@angular/core'

@Component({
    selector: 'app-contador',
    templateUrl: './contador.component.html',
})
export class ContadorComponent {
    titulo: string = 'Contador TS';
    numero: number = 10;
    base: number = 5;
  
    sumar(base: number) {
      this.numero += base;
    }
  
    restar(base: number) {
      this.numero -= base;
    }  
}
```

## Utilizar el componente

### app.module.ts
```ts
@NgModule({
  declarations: [
    AppComponent,       
    ContadorComponent   // Referencio aquí mi módulo. 
                        // Aún no he creado un módulo. 
                        // Pero lo declaro dentro de este mismo módulo
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

### app.component.html
Puedo incluir todas las referencias que sea necesario al componente en mi plantilla.

```html
<app-contador></app-contador>
<app-contador></app-contador>
<app-contador></app-contador>
```

