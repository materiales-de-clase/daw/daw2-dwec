# Directivas
En nuestras páginas podemos utilizar diverss directivas para generar el código HTML desde el cliente a partir de la información en nuestro modelo de datos.

## ngFor
Permite retir un código HTML a partir de una lista de datos

```html
<ul>
    <!-- item será un array en el componente -->
    <li *ngFor="let item of items">{{ item }}</li>    
</ul>

<ul>
    <!-- Por ejemplo, podríamos mostrar la lista de títulos de las tareas con esta línea -->
    <li *ngFor="let t of tareas; let i = index">{{ t.titulo }} {{ i }}</li>
</ul>    
```

## ngIf
Permite que un elemento se muestre únicamente si se cumple la condición

```html
<p *ngIf="saludar == true">
    Hola mundo
</p>
```

## ngIf-else + ng-Template
Permite que un elemento se muestre únicamente si se cumple la condición

```html
<p *ngIf="hola == true; else adios">
    Hola mundo
</p>
<ng-template #adios>
    Adios mundo
<ng-template>
```
