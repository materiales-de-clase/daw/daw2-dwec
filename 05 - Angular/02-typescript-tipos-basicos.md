# Tipos Básicos
En este apartado, vamos a hablar sobre los tipos básicos de TypeScript. Hay que tener presente, que aunque TypeScript es tipado, por debajo no deja de ser JavaScript y va a ser traducido a JavaScript antes de ejecutarse en el navegador, de modo que los tipos van a ser los mismos a los mismos que ya teníamos disponibles. La diferencia principal, será que ahora tenemos que declarar las variables de un tipo determinado. 

## Declaración de variables
A continuación tenemos agunos ejemplos de declaraciones de variables utilizando TypeScript

```ts    
    // Declaro variables de un tipo y les asigno un valor
    let nombre: string = "Paco";
    let edad: number = 10;
    let fin: boolean = false;
```

Puedo tener variables que guarden más de un tipo de valor. En el siguiente ejemplo la variable puede aceptar valores de tipo numérico o string
```ts
    let variable: number | string; 
    variable = 10;      // Esto es correcto
    variable = 'hola';  // Esto es correcto
```

## Declaración de constantes
Con las constantes en cambio no tenemos que indicar el tipo de dato, ya que una constante va a tener únicamente el valor que se asigna durante la declaración.

```ts
    // La declaración de constantes se hace del mismo modo que en JavaScript
    const nombre = "PACO";
```