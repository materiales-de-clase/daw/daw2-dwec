# ViewChild 
Es otro método que se puede utilizar para mapear elementos de la vista con el modelo de modo que se pueda acceder a los datos en la vista desde el modelo. En esta opción, tenemos acceso al elemento HTML en la página web. En lo que se hace con el ngModel solo tenemos acceso al valor, pero no es posible manipular los elementos de la vista. De este modo, tenemos acceso a todos los atributos de la vista y podríamos hacer los cambios que fueran oportunos.

Más información en

https://angular.io/api/core/ViewChild 

