# Tareas por estado
Este gráfico se va a implementar utilizando CanvasJS

## Instalación
#### Descargar desde aquí
En primer lugar, tendremos que obtener la biblioteca desde aquí y descomprimir el archivo.

https://canvasjs.com/download-html5-charting-graphing-library/

#### Copiar
Dentro del directorio src/lib de nuestro proyecto tendremos que poner los siguientes archivos:

- canvasjs.angular.component.ts
- canvasjs.min.js

#### Importar a nuestro módulo
Ahora vamos a importar al módulo dashboard

```ts
import * as CanvasJSAngularChart from '../../lib/canvasjs.angular.component';
var CanvasJSChart = CanvasJSAngularChart.CanvasJSChart;

@NgModule({
  declarations: [
    CanvasJSChart,
    DashboardComponent,    
    TareasPorEstadoComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
```

## Crear el componente
Vamos ahora a crear el componente tareas por estado.

#### html
```html
<!-- Documentación aquí
    https://canvasjs.com/docs/charts/integration/angular/
-->
<div>
    <canvasjs-chart [options]="chartOptions" [styles]="{height: '300px', width: '500px'}"></canvasjs-chart>
</div>
```

#### typescript
```ts
// Documentación aquí
//    https://canvasjs.com/docs/charts/integration/angular/

import { AfterViewInit, Component } from '@angular/core';
import { TareasService } from '../../../tasks/services/tareas.service';

@Component({
  selector: 'app-tareas-por-estado',
  templateUrl: './tareas-por-estado.component.html'
})
export class TareasPorEstadoComponent implements AfterViewInit {

  // Define la variable que va a almacenar los puntos en el gráfico
  dataPoints: any[] = [];
  
  // Puntero a la gráfica
  chart:any;

  // Opciones del gráfico 
  chartOptions: any = {};    

  constructor(

    private tareasService: TareasService

  ) { }

  ngAfterViewInit(): void {

    // Carga los datos del gráfico
    this.tareasService.getResumenTareasPorEstado().subscribe(respuesta => {

      // Obtiene la respuesta
      let data = respuesta.datos;

      for (var i = 0; i < data.length; i++) {

        // Crea el punto
        const punto = {
          name: data[i].estado,
          y: Number(data[i].contador)
        }

        // Añade el punto
        this.dataPoints.push(punto);
      }      

      // Si no asigna aquí las opciones
      // no se refresca el gráfico
      this.chartOptions = {    
        exportEnabled: false,
        animationEnabled: true,
        title:{
            text: "Tareas por estado"
        },
        legend:{
            horizontalAlign: "right",
            verticalAlign: "center"
        },
        data: [{
            type: "pie",
            showInLegend: true,
            toolTipContent: "<b>{name}</b>: ${y} (#percent%)",
            indexLabel: "{name}",
            legendText: "{name} (#percent%)",
            indexLabelPlacement: "outside",
            dataPoints: this.dataPoints      
        }]
      };
    });    
  }

}
```


