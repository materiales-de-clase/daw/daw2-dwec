# Introducción
En este apartado vamos a hacer una pequeña introducción al desarrollo de aplicaciones con Angular. Hay que tener presente que Angular lo vamos a utilizar para desarrollar el frontend, pero no nos obliga a utilizar una tecnología en particular para implementar nuestro backend. Normalmente, nuestro front intercambiará peticiones con el back y mostrará los resultados.

Si deseas obtener más información puedes acceder a la página web de Angular en http://angular.io

## Características
Algunas de las características de Angular son:

- El framework de Angular es mantenido por Google
- Se pueden desarrollar aplicaciones para todas las plataformas
- Provee un marco de trabajo estandarizado
- Viene todo lo que vas a necesitar para trabajar
- Las aplicaciones son modulares

## Bloques fundamentales
Vamos a ver aquí los bloques fundamentales que compondrían una aplicación en Angular. Además, vamos a organizarlos de forma jerarquica.

- **Aplicación** Lo que sería el bloque principal. Representa nuestra aplicación. Contendrá bloques de los siguientes tipos.

  - **Módulos**: Los módulos los podemos considerar unidades organizativas que contienen todas las entidadas que están relacionadas entre sí. Por ejemplo, podemos tener un módulo de productos con todo aquello que está relacionado con los productos. Cuando reutilizamos partes de terceros, lo que vamos a instalar y referenciar son módulos. Dentro de nuestros módulos podemos encontrarnos:
    - **Componentes**: Se pueden ver como un bloque de código HTML con una contraparte de typescript. Se busca que los componentes sean bloques pequeños de código y lo más sencillos que sea posible. Pueden ir desde una página web hasta una pequeña parte reutilizable. Por ejemplo, la página principal de nuestra web será un componente. 
    - **Servicios**: Son singletons que permiten a la aplicación trabajar con información centralizada en algún lugar entre otras cosas.
    - **Directivas**: Hay tres tipos
      - **Directivas de componentes**: permiten intertar código html
      - **Directivas estructurales**: permiten modificar el dom añadiendo o eliminando elementos.
      - **Directivas de atributos**: cambian la apariencia o el comportamiento de los elementos, otro componente o una directiva
      - **Rutas**: son diferentes componentes que se pueden mostrar en función de la url en que se encuentra el cliente
      - **Pipes**: Son otras clases que permiten pasar información a través de ellas y alterarla.

# SPA (Single Page Application)
Técnicamente, una SPA es un sitio donde existe un único punto de entrada, generalmente el archivo index.html (u otro). En la aplicación no hay ningún otro archivo HTML al que se pueda acceder de manera separada y que nos muestre un contenido o parte de la aplicación, toda la acción se produce dentro del mismo index.html. 

