# Encadenamiento opcional
El encadenamiento opcional permite evitar ciertos errores de programación que van a darse en tiempo de ejecución. Utilizarlos va a permitir evitar estos errores o que nuestro programa falle.

## Ejemplo de encadenamiento opcional
En primer lugar, vamos a definir una clase con un atributo opcional.

```ts
interface Persona {
    nombre: string;
    hijos?: string[];   // Este atributo es opcional
                        // Podemos tener instancias de Persona que no
                        // tengan este atributo
}
```

Ahora vamos a crear un par de instancias de esta clase. En este ejemplo, paco no tiene el atributo hijos, pero ana tiene dos.
```ts
const paco: Persona = {
    nombre: "Paco";
}

const ana: Persona = {
    nombre: "Ana";
    hijos: [ 'Pablo', 'Natalia' ];
}
```

Vamos ahora a implementar varias versiones de una función que va a mostrar el número de hijos para explicar el problema.

```ts
function mostrarNumeroHijosFalla(p: Persona) : void {

    // Esta línea de código falla si hijos no está definido. Esto
    // puede ocurrir, ya que hemos puesto que es una propiedad opcional.
    // Ontendremos un error en tiempo de ejecución.
    const numeroHijos = p.hijos.length;

    console.log(numeroHijos);
}

function mostrarNumeroHijosUndefined(p: Persona) : void {

    // Ahora utilizamos encadenamiento opcional. Ya no falla esta línea.
    // La interrogación hace que si la propiedad 
    // no exis no continúe evaluando, pero en este caso numeroHijos será 
    // undefined si la persona no tiene hijos. Por ejemplo en el caso de paco
    const numeroHijos = p.hijos?.length;

    console.log(numeroHijos);
}

function mostrarNumeroHijos(p: Persona) : void {

    // Con la siguiente sentencia dejamos el problema resuelto.
    // Ahora, en caso de que no tenga hijos, en lugar de undefined, 
    // va a mostrar el valor 0.
    const numeroHijos = p.hijos?.length || 0;

    console.log(numeroHijos);
}

```
Finalmente, usamos la función definitiva
```ts
mostrarNumeroHijos(paco); // No tiene hijos, luego muestra 0
mostrarNumeroHijos(ana);  // Muestra 2
```