# Componente Menú de aplicación
Vamos ahora a desarrollar un componente que va a ser nuestro menú de aplicación.

## Crear el componente
Con la siguiente secuencia creamos el componente

```cmd
C:\MIPROYECTO> ng generate component shared/menu/Menu --skip-tests --style="none"
```

## HTML

```html
<!-- Configura la barra y asigna el color --> 	
<nav class="navbar navbar-expand-md navbar-dark bg-primary mb-4">

    <!-- Enlace al dashboard -->
    <a class="navbar-brand m-1" routerLink="dashboard"><img src="assets/logo-pequeño.png" alt="Logo"></a>

    <!-- Menú desplegable -->
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav mr-auto">

            <!-- Cuadro de mandos -->
            <li class="nav-item active">
                <a routerLink="dashboard" routerLinkActive="text-warning" class="nav-link" >Dashboard</a>
            </li>

            <!-- Tareas -->
            <li class="nav-item active">
                <a routerLink="tareas" routerLinkActive="text-warning" class="nav-link">Tareas</a>
            </li>

            <!-- Configuración -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarConfiguracion" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Configuración</a>
                
                <ul class="dropdown-menu" aria-labelledby="navbarConfiguracion">
                    <li>
                        <a routerLink="usuarios" routerLinkActive="text-warning" class="dropdown-item">Usuarios</a>
                    </li> 
                    <li>
                        <a routerLink="tipostarea" routerLinkActive="text-warning" class="dropdown-item">Tipos de tarea</a>
                    </li> 
                    <li>
                        <a routerLink="estados" routerLinkActive="text-warning" class="dropdown-item">Estados</a>
                    </li>
                    <li>
                        <a routerLink="etiquetas" routerLinkActive="text-warning" class="dropdown-item">Etiquetas</a>
                    </li>
                </ul>
            </li>            
        </ul>
    </div>

    <!-- Cerrar sesión -->
    <!-- TODO Cerrar la sesión -->
    <a class="navbar-brand m-2"><i class="bi bi-box-arrow-right" style="font-size:30px;" (click)="cerrarSesion()"></i></a>
</nav>
```

## TypeScript

```ts
import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent {

  @Output() onCerrarSesion : EventEmitter<any> = new EventEmitter();

  cerrarSesion() {
    // Genera evento de cerrar sesión
    this.onCerrarSesion.emit();
  }

}
```
