# Instalaciones
A lo largo de la unidad de trabajo serán necesarias las herramientas que se indican en las siguientes secciones. Algunas de ellas serán obligatorias y otras opcionales. En todo caso, es imrescincible asegurarse de que se tienen todas las herramientas necesarias para trabajar instaladas antes de pasar a la siguiente sección.

## Programas 
Los siguientes programas van a ser imprescindibles para seguir las clases.

- [Postman](https://www.postman.com/): Programa que vamos a utilizar para hacer pruebas de llamadas a nuestros endpoints.
- [Node.js](https://nodejs.org/es/): Vamos a utilizar node para trabajar con el cli de Angular.

## Extensiones VS Code
Las siguientes extensiones de VS Code son de instalación recomendada para trabajar con Angular aunque es totalmente opcional y no las vamos a probar.

- [Angular 10 Snippets](https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode)
- [Angular Language Service](https://marketplace.visualstudio.com/items?itemName=TypeScriptTeam.AngularLanguageService)
- [angular2-inline](https://marketplace.visualstudio.com/items?itemName=natewallace.angular2-inline)
- [Autor Close Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag)
- [Type Script Importer](https://marketplace.visualstudio.com/items?itemName=pmneo.tsimporter)

## Instalación de Angular CLI
Para trabajar con angular, será imprescindible instalar ancular CLI, ya que es lo que vamos a utilizar para gestionar nuestros proyectos en angular. Para insdtgalarlo sigue los siguientes pasos:

1. **Abre un terminal como administrador**. En linux puedes ejecutar los comandos con sudo
2. **Ejecuta el siguiente comando**

```batch
> npm install -g @angular/cli
added 209 packages, and audited 210 packages in 11s

26 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```
3. Para **comprobar si está correctamente instalado**, podemos ejecutar la siguiente secuencia:

```batch
C:\> ng version
? Would you like to share pseudonymous usage data about this project with the Angular Team
at Google under Google's Privacy Policy at https://policies.google.com/privacy. For more
details and how to change this setting, see https://angular.io/analytics. No
Global setting: disabled
Local setting: No local workspace configuration file.
Effective status: disabled

     _                      _                 ____ _     ___
    / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
   / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
  / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
 /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
                |___/


Angular CLI: 14.2.6
Node: 16.17.0
Package Manager: npm 8.15.0
OS: win32 x64

Angular: undefined
...

Package                      Version
------------------------------------------------------
@angular-devkit/architect    0.1402.6 (cli-only)
@angular-devkit/core         14.2.6 (cli-only)
@angular-devkit/schematics   14.2.6 (cli-only)
@schematics/angular          14.2.6 (cli-only)
```

## Navegador recomendado
Se puede utilizar Firefox o Chrome, sin embargo, en esta etapa vamos a pasar a utilizar chrome, ya que en algunas circunstancias ofrece más información
que Firefox y eso nos puede ayudar a depurar problemas y obtener más información de las pruebas que hagamos a lo largo del desarrollo del proyecto.

## Otros recursos
Para las primeras etapas del curso, en lugar de usar un proyecto de Angular, podemos utilizar un editor de TypeScript en línea. Esto puede servir para hacer algunas pruebas sin tener que pasar a la parte de Angular y tratar el lenguaje por separado. En el siguiente enlace tenemos disponible uno de estos intérpretes.

- https://stackblitz.com/edit/typescript-vxnz8z?file=index.ts

