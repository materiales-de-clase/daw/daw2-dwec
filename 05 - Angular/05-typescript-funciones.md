# Funciones
En este apartado vamos a describir como son las funciones en TypeScript

## Declaración de funciones
Vamos a ver las diferencias entre definir una función en JavaScript y TypeScript. A continuación tenemos la típica función suma escrita en JavaScript

```js
    function sumar(x, y) {
        return x+y;
    }

    // Las siguientes llamadas a sumar no van a actuar como se esperaba pero no 
    // tenemos forma de verlo hasta que no ejecutamos nuestro programa.
    sumar('Paco');  // Fallará en tiempo de ejecución. 
    sumar(10);      // Fallará en tiempo de ejecución
```

A continuación tenemos el mismo ejemplo pero utilizando TypeScript
```ts
    // En esta función, indico el tipo de los parámetros de entrada y de salida
    function sumar(x: number, y: number): number {
        return x+y;
    }

    // En este caso, el siguiente código es correcto
    sumar(x , y);

    // En cambio los siguientes son erroneos. Esto lo sabremos mientras
    // etidamos nuestro código.
    sumar('paco');
    sumar(10);
```
## Funciones que no retornan nada
Aunque en TypeScript no es necesario ya que TS trata de inferirlo, indicar que una función no retorna nada aporta información al programador de cuál es el uso que se debe hacer de esa función. En algunos lenguajes, se utiliza void para indicar que una función no retorna ningún valor. En TS podemos hacerlo también de este modo. A continuación tenemos un ejemplo:

```ts
    // Esta función no retorna nada. Indicando que retorna void aporto
    // esta información al analizador de TS y al programador que vaya
    // a utilizar esta función.
    function saludar(): void {
        console.out('HOLA !!!');
    }

    // El siguiente ejemplo va a fallar, ya que estoy tratando de retornar algo
    function saludar(): void {
        console.out('HOLA !!!');

        return 1;   // Falla. Va en contra de void
    }

```

## Valores por defecto
Al igual que en JavaScript, podemos especificar un valor por defecto para los parámetros. En la siguiente función, si omito el segundo parámetro tendrá el valor 1.

```ts
    // Si no paso el valor de y, se multiplica por 1.
    function multiplicar(x: number, y: number = 1): number {
        return x+y;
    }

    // El resultado es x * 1
    multiplicar(x) == x * 1;
```

## Parámetros opcionales
En JavaScript, se pueden pasar los parámetros que queramos en la invocación a una función. Aquí se nos obliga a pasar los argumentos que se han incluido en la declaración. En este caso, podemos seguir teniendo parámetros opcionales, pero debemos indicar explícitamente que son opcionales. Lo vemos en el siguiente ejemplo volviendo a la multiplicación:

```ts
    // La ? indica que el parámetro y es opcional
    // Esto hace que TS no nos obligue a pasar el parámetro
    // pero dentro de la función deberíamos controlarlo.
    function multiplicar(x: number, y?: number): number {
        return x+y;
    }

    // El resultado es NaN
    multiplicar(x) == x * undefined;    
```

Ten en cuenta que los parámetros opcionales deben empezar siempre por la derecha para evitar ambiguedades. En caso contrario obtendrás un error.

## Funciones que reciben objetos como argumentos
Del mismo modo que hacemos con los tipos básicos, es recomendable que cuando una función va a recibir un objeto, indiquemos el tipo de objeto que va a recibir cómo argumento. En este caso, vamos a recurrir de nuevo a los interfaces de los que hemos hablado previamente.

Así por ejemplo podemos tener el siguiente interfaz:

```ts
    interface Persona {
        nombre: string;
        apellidos?: string;
        edad: number;
    }
```

Y luego podemos tener funciones que reciban objetos de estos tipos como argumento y lleven a cabo una operación. Por ejemplo:
```ts
    function mostrarQuienEsMasViejo(p1: Persona, p2: Persona): void {
        if(p1.edad > p2.edad) {
            console.out(p1.nombre+" es más viejo que "+p2.nombre);
        } else if(p1.edad == p2.edad) {
            console.out(p1.nombre+" es de la misma edad que "+p2.nombre);
        } else {
            console.out(p1.nombre+" es más joven que "+p2.nombre);
        }
    }
```

Aquí tendríamos un ejemplo de uso de la función
```ts
    const paco : Persona = {
        nombre: 'Paco',
        edad: 10
    };

    const manolo : Persona = {
        nombre: 'Manolo',
        edad: 20
    };

    mostrarQuienEsMasViejo(p1, p2);
```

# Métodos en interfaces
Anteriormente hemos hablado de los interfaces, pero nos hemos limitado a añadir propiedades. En este apartado vamos a ver como podemos añadir una función/método a un interfaz en TypeScript. Para ello vamos a partir de nuestro interfaz Persona

```ts
    interface Persona {
        nombre: string;
        apellidos?: string;

        edad: number;

        // No se da una implementación. Solo se describe la entrada y salida
        // de la función
        mostrarNombreCompleto: () => void;
    };
```

Una implementación válida de ese interfaz sería
```ts
    const paco : Persona = {
        nombre: 'Paco',
        apellidos: 'García',
        edad: 10,      

        mostrarNombreCompleto() {
            console.log(nombre+apellidos);
        }
    };
```

## Buenas prácticas

- Indica siempre el tipo de los parámetros y el valor de retorno de las funciones
