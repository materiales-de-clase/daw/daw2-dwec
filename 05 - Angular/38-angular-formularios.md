# Formularios
Formas de trabajar con los formularios

- Formularios por template o aproximación por template
- driven template o formularios reactivos

## Formularios por template
Toda la pantalla se encuentra en el HTML y la mayor parte de la lógica se encuentra en el HTML. Vamos a poner atributos en el HTML y Angular se encargará de inferir la mayor parte de las cosas.

Son más fáciles de utilizar, pero se tiene menos control lo que afecta que pudiera ser más complicado llevar a cabo ciertas cosas.

En tipo este:
- Angular se encarga de toda la gestión del formulario. 
- Mi lógica se encuentra en la parte del template html
- Ideal para formularios sencillos
- Más difícil de depurar

## Formularios reactivos / model driven 
Dan más control y es más fácil implementar validaciones que en el caso de los formularios por template.

- Procura que el HTML sea lo más ligero posible. La plantilla es mucho más ligera y fácil de leer
- La mayor parte de la lógica se encuentra directamente en el código del componente
- Permite desarrollos más complejos donde hace falta tener mucho control

## Validaciones en servicios

Aprovechando que los servicios son singleton, es posible hacer validaciones en los servicios.

## Carga de datos. Por ejemplo para editar
La carga de datos de un formulario se hará generalmente en el onInit


## Referencias
https://angular.io/guide/reactive-forms