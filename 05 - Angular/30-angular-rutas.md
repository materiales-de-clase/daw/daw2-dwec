# Rutas
Angular nos permite crear aplicaciones SPA (Single Page Application). En estas aplicaciones solo tenemos un index.html que va a hacer de punto de entrada a nuestra aplicación pero, como sabemos, cualquier aplicación compleja está compuesta por muchas páginas. En Angular toda la gestión de la página que se muestra en cada momento se hace desde el cliente. Esto significa que Angular debe proveer de algún mecanismo para determinar que página (componente) se muestra en pantalla en cada momento. Esta funcionalidad la provee el enrutamiento. 

A lo largo de esta sección vamos a hablar de los siguientes conceptos:

- Definir qué vamos a usar para enrutar
- Configurar el enrutamiento de nivel superior o raíz. Este enrutamiento lo definiremos en el módulo principal de nuestra aplicación.
- Configurar el enrutamiento anidado, que definiremos en cada uno de los módulos de nuestra aplicación.
- Cómo mostrar la página que corresponda en función de la URL.
- Paso de parámetros a una página. 
- Como enlazar a una ruta desde cualquier página de la aplicación

## Definición de ruta
Ya que estamos en una aplicación web, las rutas vienen dadas por la url. Angular se encargará de capturar la url completa del navegador y mostrar el componente que corresponda. Por ejemplo, para nuestra aplicación podríamos considerar válidas las siguientes rutas:

- http://localhost/
- http://localhost/auth/login
- http://localhost/tareas/listado
- http://localhost/tareas/agregar
- http://localhost/tareas/editar/:id
- http://localhost/tareas/:id

Si nos fijamos, decidimos la página en que nos encontramos añadiendo elementos a la ruta. Además, en el caso de editar, se permite especificar que un elemento de la ruta es un parámetro. Podría ser por ejemplo un ID. Cuando hagamos la implementación del editar ya vemos como capturar ese parámetro de entrada. 

## Configurar el enrutamiento raíz
Este enrutamiento es el que hacemos en la aplicación principal. Nosotros lo vamos a configurar para enrutar con el primer componente de la ruta. En este caso sería, por ejemplo, auth o tareas. 

Vamos a echar un vistazo al fichero *app-routing-module.ts*. Si cuando creaste el proyecto le dijiste que configurara enrutamiento el archivo ya existirá. En caso contrario, lo puedes crear manualmente.

```ts
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Defino las rutas raíz. Estoy utilizando carga perezosa de acuerdo con
// la documentación de Angular
// https://angular.io/guide/lazy-loading-ngmodules
//
// Es un array de objetos de tipo Route. Tienes más información sobre
// dicho objeto aquí.
// https://angular.io/api/router/Route
const routes: Routes = [

  {    
    // Módulo de autenticación
    path: 'auth',
    // La ruta que indico es la ruta del módulo a importar
    // la función flecha siempre va a ser así. 
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),    
  },

  {
    // Cuadro de mandos
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),    
  },

  {
    // Gestión de tareas.
    path: 'tareas',
    loadChildren: () => import('./tasks/tasks.module').then(m => m.TasksModule),
  },

  {
    // La ruta por defecto. Va a entrar aquí si solicitamos
    // una ruta no definida.
    path: '**',
    redirectTo: 'dashboard'
  }
];

@NgModule({

  imports: [

    // Esto importa las rutas configuradas. Es importante incluir este
    // import para configurar el enrutamiento
    RouterModule.forRoot(routes)
  ],
  
  // Al exportar esto aquí, cuando importe este módulo se importarán
  // también las rutas.
  exports: [RouterModule]
})
export class AppRoutingModule { }
```

## Rutas hijas
En el anterior fichero se han configurado las rutas de primer nivel. Estas rutas se utilizan para entrar en los diferentes módulos. Luego las rutas hijas se utilizarán para reenviar al componente adecuado dentro del módulo.

Aquí tenemos un ejemplo de configuración de rutas hijas que vamos a utilizar para explicarlas basado en el archivo de rutas del módulo táreas:

```ts
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EditarComponent } from './pages/editar/editar.component';
import { ListadoComponent } from './pages/listado/listado.component';
import { VerComponent } from './pages/ver/ver.component';

const routes: Routes = [
  {
    // No es necesario especificar nada en este atributo
    // Ya que estamos definiendo rutas hijas.
    path: '',
    children: [
      {
        // tareas/listado
        path: 'listado',
        component: ListadoComponent
      },
      {
        // tareas/agregar
        path: 'agregar',
        component: EditarComponent
      },
      {
        // editar/XXXX donde XXXX es el id de tarea
        // Desde el componente se puede capturar ese ID
        path: 'editar/:id',
        component: EditarComponent
      },
      {
        // Hace match para /XXXX donde XXXX es el id de tarea
        // desde el componente se captura y se puede mostrar
        // la tarea
        path: ':id',
        component: VerComponent
      },
      {
        // Por defecto, envía al listado.
        path: '**',
        redirectTo: 'listado'
      }
    ]
  }
];

@NgModule({
  imports: [

    // Es imprescindible hacer esta importación. 
    RouterModule.forChild(routes)
  ],
  exports: [
    // Por convención, se exporta. De este modo solo hay que importar 
    // este módulo para tener las rutas.
    RouterModule
  ]
})
export class TasksRoutingModule { }
```

## Mostrar un componente en función de la ruta
Para hacer esto recurrimos al elemento router-outlet- En el siguiente fragmento de código html que se encontrará en **app.component.html** vemos como se incluye el elemento router-outlet. Donde se encuentra se renderizará el componente que la ruta seleccione.

```html
    <!-- Aquí tendría que escribir el HTML de mi menú-->
    
    <!-- Este componente se sitúa en el centro de la página -->
    <div class="col">    
        <router-outlet></router-outlet>
    </div>
```

### Ejercicio
Prueba a cambiar de una ruta a otra en el navegador y verifica que se te muestra una u otra página en función de la URL.

## RouterLink
De esta forma podemos crear un enlace para redirigir a una ruta determinada. El objetivo es mostrar otro componente pero sin refrescar la página.

A continuación tenemos un ejemplo de una lista que puede ser utilizada para navegar entre varias páginas.

```html
<div class="row container mt-4">
    <div class="col-3">
        <h2>Búsquedas</h2>
        <hr>
        <ul class="list-group">
            <!--    
                RouterLink indica que el elemento es un enlace a una parte de nuestra aplicación                 
                
                RouterLinkActive indica la clase css a añadir en caso de que estemos en esa ruta, conveniente para marcar la opción
                del menú que está seleccionada.

                La RUTA VACIA contiene a las demás, de modo que si queremos que solo se marque cuando esté exactamnete en la ruta vacia se puede utilizar el parámetro routerLinkActiveOptions. Esto permite enviar parámetros. En este caso, que sea coincidencia exacta
            -->
            <li routerLink=""                   
                routerLinkActive="active"
                [routerLinkActiveOptions]="{ exact:true }"
                class="list-group-item">
                Buscar tarea
            </li>
            <li routerLink="informador"     
                routerLinkActive="active"
                class="list-group-item">
                Tareas por informador
            </li>
            <li routerLink="asignado"    
                routerLinkActive="active"
                class="list-group-item">
                Tareas por asignado
            </li>
        </ul>
    </div>
```

## Dependencias
Para utilizar rutas hay diversos módulos que son necesarios. En la documentación de cada elemento o atributo utilizado se hará referencia a los módulos requeridos. Por ejemplo, para usar RouterLink no debemos olvidar importar el módulo **RouterModule** en el módulo donde vayamos a crear enlaces. 