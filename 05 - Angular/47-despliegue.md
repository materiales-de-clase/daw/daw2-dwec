# Despliegue
En el despliegue hay únicamente aquello que la aplicación necesita. Esto quiere decir que para hacer el despliegue es necesario hacer la construcción de nuestra aplicación para despliegue en producción. Una vez que tengamos nuestra build podemos desplegarlo en cualquier sistema. Básicamente vamos a obtener una aplicación web de modo que podemos desplegarlo en cualquier servidor de internet donde tengamos acceso ftp.

## Generar build de producción
Antes de generar el build a producción, debemos verificar que funciona. Esto lo podemos comprobar por ejemplo con el comando *ng serve -o*. 

```
Debemos ser cuidadosos y eliminar referencias y elementos que nuestras páginas no estén utilizando. Evita subir código muerto a producción.
```

Una vez tengamos nuestra aplicación lista, para generar nuestro build para producción podemos utilizar la siguiente secuencia de comandos:

```cmd
C:/MIPROYECTO> ng build --configuration production
√ Browser application bundle generation complete.
√ Copying assets complete.
√ Index html generation complete.

Initial Chunk Files           | Names         |  Raw Size | Estimated Transfer Size
main.560323f6b7fe4039.js      | main          | 144.68 kB |                40.81 kB
polyfills.918b4fc788bd4dea.js | polyfills     |  33.06 kB |                10.69 kB
runtime.d15e317f76fa3172.js   | runtime       |   1.04 kB |               600 bytes
styles.dc3e7ac02ccd86e8.css   | styles        | 464 bytes |               218 bytes

                              | Initial Total | 179.23 kB |                52.30 kB

Build at: 2022-10-20T12:56:51.124Z - Hash: f5df17ad2dacf2ae - Time: 10337ms
```

El parámetro --configuration permite escoger la configuración de construcción a utilizar. Tiene que ser una entrada en la sección *configurations* del archivo *angular.json*. En la versión actual, la configuración production viene por defecto en el proyecto generado.

Vemos que se han generado varios archivos. Estos archivos se van a encontrar en el directorio dist dentro de nuestro proyecto. Dentro de dicho directorio aparecen los archivos:

- index.html: el punto de entrada a nuestra aplicación
- 3rdpartylicenses.txt: Las licencias de componentes que hayamos utilizado
- favicon.ico: El icono de aplicación
- main.js: 
- polyfills.js: Ayuda a la compatibilidad con navegadores antiguos
- runtime.js
- styles.css: El archivo css de la aplicación

Hay archivos que tienen una hash en el nombre. Esto lo hace angular para evitar que los navegarores puedan guardar en caché archivos actualizados. De este modo, ante una nueva versión en que uno de los archivos ha cambiado, se obliga a que el archivo de la nueva versión se cargue de nuevo.

## Desplegarlo
```
Va a ser imprescindible desplegar en un servidor para poder probar 
la aplicación. Si abrimos directamente el index.html no va a funcionar nuestra aplicación.
```
El despliegue de la aplicación va a consistir generalmente en tomar la carpeta dist de nuestra aplicación, donde se encontrarán los archivos HTML, CSS y JS y desplegarla mediante FTP a un servidor de internet. 

Debemos tener en cuenta que lo que estamos desplegando es un frontend de modo que si nuestra aplicación necesitara de un backend tendríamos que desplegar también el backend.

## Proveedores
Lista de proveedores que pueden ser utilizados para desplegar nuestros frontends desarrollados en Angular.

- https://www.netlify.com/




