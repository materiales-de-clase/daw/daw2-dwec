# Validaciones asíncronas
Son aquellas que requieren, por ejemplo, una llamada al servidor para completar la validación. Las vamos a implementar como un tipo especial de servicio.

## Crear el servicio
Primero tendremos que ejecutar la siguiente secuencia de comandos:

```cmd
C:\MIPROYECTO> ng generate service tasks/validators/ValidacionTitulo --skip-tests
```
## Editar el archivo
Tendremos que editarlo e implementar el interfaz AsyncValidator. A continuaciónb tenemos un ejemplo de validación que comprueba si el título está duplicado.

```ts
export class ValidacionTituloService implements AsyncValidator {

  // Ruta base para todas las llamadas al servicio
  private taskmanBaseUrl = environment.taskmanBaseUrl;
  private debug = environment.debug;

  constructor(

    private validacionService   : ValidacionService,
    private tareasService       : TareasService

  ) { 

    validacionService.registrarMensajeError("tituloDuplicado", "Ya existe una tarea con ese título");

  }

  validate(control: AbstractControl<any, any>): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {

    // Obtengo el titulo como argumento
    const titulo = control.value;

    // Tendré que usar un pipe que evalue y me retorne el objeto que necesito para generar validation errors
    // Utilizo el operador map para cambiar el objeto que recibo por el objeto con el error
    return this.tareasService.getTareasPorTitulo(titulo)
      .pipe(
        
        // Esta pausa me permite comprobar como cambia el estado del formulario de INVALID a PENDING a VALID
        //delay(5000),

        map( respuesta => {
          
          if(respuesta.ok == 1 && respuesta.datos[0]?.titulo == titulo) {
            return { tituloDuplicado: true }
          } else {
            return null;
          }            
        })  
      );
  }  
}
```
