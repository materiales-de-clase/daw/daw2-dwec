# Importar bootstrap
En este proyecto vamos a utilizar bootstrap como framework CSS para trabajar la parte visual. En este documento os indico los pasos necesarios para importarlo y utilizarlo en nuesta aplicación Angular.

## Instalación de boostrap
Lo primero que vamos a hacer es utilizar el npm para instalar los paquetes necesarios. En nuestro caso, vamos a utilizar también la parte de JavaScript de modo que tendremos que instalar varios paquetes.

```bash
$ npm install  bootstrap@5.2.2 jquery @popperjs/core bootstrap-icons@1.9.1

added 4 packages, removed 1 package, and audited 914 packages in 2s

123 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```

Comprobamos que están instalados:

```bash
$ npm list
+-- @angular-devkit/build-angular@14.2.6
+-- @angular/animations@14.2.7
+-- @angular/cli@14.2.6
+-- @angular/common@14.2.7
+-- @angular/compiler-cli@14.2.7
+-- @angular/compiler@14.2.7
+-- @angular/core@14.2.7
+-- @angular/forms@14.2.7
+-- @angular/platform-browser-dynamic@14.2.7
+-- @angular/platform-browser@14.2.7
+-- @angular/router@14.2.7
+-- @popperjs/core@2.11.6
+-- @types/jasmine@4.0.3
+-- bootstrap@5.2.2
+-- jasmine-core@4.3.0
+-- jquery@3.6.1
+-- karma-chrome-launcher@3.1.1
+-- karma-coverage@2.2.0
+-- karma-jasmine-html-reporter@2.0.0
+-- karma-jasmine@5.1.0
+-- karma@6.4.1
+-- rxjs@7.5.7
+-- tslib@2.4.0
+-- typescript@4.7.4
```

## Ahora los referenciamos en angular.json
En el siguiente paso, vamos a hacer que estos scripts que hemos instalado y que estarán en node_modules se incluyan junto con la aplicación. En el archivo **angular.json** tendremos que incluir una referencia a los productos instalados en las siguientes rutas:

- projects->architect->build->styles
- projects->architect->build->scripts

Nos quedaría algo así dependiendo de los estilos y scripts que tengamos previamente instalados en nuestra aplicación:

```json
"styles": [
  "node_modules/bootstrap/dist/css/bootstrap.min.css",
  "node_modules/bootstrap-icons/font/bootstrap-icons.css",
  "src/styles.scss"
],
"scripts": [
  "node_modules/jquery/dist/jquery.min.js",
  "node_modules/@popperjs/core/dist/umd/popper.min.js",
  "node_modules/bootstrap/dist/js/bootstrap.min.js"
]
```

## Importando jQuery
En este punto, tenemos instalados tanto los archivos js como los css dentro de nuestra aplicación. Todos los archivos css se referenciarán de forma automática. Sin embargo, si queremos hacer referencia, por ejemplo, a la función $ de jquery, será necesario un paso adicional. Esto mismo podría ser necesario para otras bibliotecas importadas del mismo modo. Los pasos que tendremos que dar serían los siguientes: 

1. En nuestro proyecto, crear el archivo **src/typing.d.ts**
2. Dentro de dicho archivo podemos insertar una línea como la siguiente

```ts
declare var $: any;
```

Después de esto, debería ser posible invocar a jQuery desde dentro de nuestros componentes Angular.