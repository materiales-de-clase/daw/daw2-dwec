# FormsModule
Es un módulo que incluye Angular para permitir trabajar con formularios. 

## Importar el módulo
En nuestro xxx.module.ts tendremos que añadirlo al array de import. Podría quedar del siguiente modo:

```ts
import { CommonModule } from '@angular/common';
import { MainPageComponent } from './main-page/main-page.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [  
    MainPageComponent
  ],
  exports: [
    MainPageComponent
  ],
  imports: [
    CommonModule,
    FormsModule     // Lo tengo que importar aquí
  ]
})
export class MiModuloModule { }
```

## Más información

https://angular.io/guide/forms-overview
https://angular.io/api/forms/FormsModule


## Términos

- ngSubmit