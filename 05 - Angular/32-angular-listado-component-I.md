# Lista de tareas
En este apartado vamos a implementar el listado de tareas. En este guión expongo la línea que vamos a ir siguiendo para implementarlo. Lo vamos a hacer sin componentes. Lo que sería incluyendo todo el código dentro de la página.

## Interface
Antes de empezar a trabajar, vamos a definir el interfaz que describe una tarea

Archivo tasks/interfaces/tarea.interface.ts
```ts
export interface Tarea {
    id_tarea?:         number;
    titulo:            string;
    id_informador:     number;
    informador?:       string;
    id_asignado:       number;
    asignado?:         string;
    id_tipo_tarea:     number;
    tipo?:             string;
    id_estado:         number;
    estado?:           string;
    descripcion:       string;
    fecha_alta:        string;
    fecha_vencimiento: string;
    hora_vencimiento:  string;
}
```

## Array de tareas
En nuestro componente, las tareas las vamos a guardar como un array. Definiremos el siguiente atributo:

```ts
tareas: Tarea[] = [];
```

## Tabla de tareas
Ahora vamos con el código HTML que podríamos utilizar para mostrar el contenido del array.

```html
<!-- Aquí está la tabla donde se van a insetar los resultados -->
<table class="table table-bordered table-striped text-center table-light table-hover align-items-center">

    <!-- Encabezado de la tabla -->
    <thead class="table text-white bg-primary">
        <tr>
            <th scope="col">id</th>
            <th scope="col">Título</th>
            <th scope="col">Tipo</th>
            <th scope="col">Estado</th>
            <th scope="col">Fecha Vencimiento</th>
            <th scope="col">Hora Vencimiento</th>
            <th scope="col"></th> <!-- Mostrar detalles -->
            <th scope="col"></th> <!-- Editar -->
            <th scope="col"></th> <!-- Eliminar tarea -->
        </tr>
    </thead>
    
    <!-- 
        Cuerpo de la tabla. Aquí es donde se van a insertar las tareas
     -->
    <tbody>
        <tr *ngFor="let tarea of tareas; let i = index">
            <td>{{ tarea.id_tarea }}</td>
            <td>{{ tarea.titulo }}</td>
            <td>{{ tarea.tipo }}</td>
            <td>{{ tarea.estado }}</td>
            <td>{{ tarea.fecha_vencimiento | date }}</td>
            <td>{{ tarea.hora_vencimiento }}</td>
            <td>
                <!-- Enlace a ver la tarea -->
                <i [routerLink]="['../ver', tarea.id_tarea ]" class="btn btn-success bi bi-eye"></i>
            </td>                
            <td>
                <!-- Enlace a la edición de la tarea -->                                
                <i [routerLink]="['../editar', tarea.id_tarea ]" class="btn btn-warning bi bi-pencil"></i>
            </td>    
            <td>
                <!-- Eliminar la tarea -->
                <i (click)="borrarTarea(i)" class="btn btn-danger bi bi-trash"></i>
            </td>
        </tr>        
    </tbody>
</table>
```

## Buscador
Ahora vamos a ver el código HTML para crear un buscador.

```html
<form (ngSubmit)="buscar()" autocomplete="off">
    <input type="text" 
           name="termino" 
           class="form-control" 
           placeholder="Introduce título a buscar" 
           [(ngModel)]="termino" >
</form>                                                              
```

Del código anterior:

- (ngSubmit): Evita que se haga un submit y permite especificar lo que queremos que se haga.
- [(ngModel)]: Permite hacer un enlace bidireccional entre el input y el atributo que hemos indicado. 

El enlace del ngModel lo podemos hacer de dos formas:

- **[ngModel]** : en una sola dirección. Lo que escribamos en la propiedad de la clase pasa al input.
- **[(ngModel)]**: En ambas direcciones. Lo que escribamos en el input irá al modelo actualizando la propiedad.

Ahora veamos lo que vamos a poner en la implementación del buscar:

```ts
// Vamos a hacer que al buscar inserte una tarea en el array
buscar() {
    let tarea1: Tarea = {
        id_tarea            : 1;
        titulo              : 'Tarea 1';
        id_informador       : 1;
        informador?         : 'Admin';
        id_asignado         : 1;
        asignado?           : 'Admin';
        id_tipo_tarea       : 1;
        tipo?               : 'Tarea';
        id_estado           : 1;
        estado?             : 'Abierta';
        descripcion         : 'Tarea de prueba';
        fecha_alta          : '00-00-00';
        fecha_vencimiento   : '00-00-00';
        hora_vencimiento    : '00:00' 
    };

    tareas.push(tarea1);
}
```

## Conclusión
En este punto deberías tener la página de tareas con un buscador de modo que al buscar muestra una tarea en la tabla.
