# Editar Component I
Vamos a implementar el editar como un formulario reactivo. Aunque pego todo el código desde el inicio, vamos a irlo haciendo todo paso a paso.

## Crear el componente

```cmd
C:\MIPROYECTO> ng generate component tasks/pages/Editar --skip-tests -style="none"
```

## Importar formularios reactivos
Será necesario importar el módulo ReactiveForms

```ts
@NgModule({
  declarations: [
  ],
  
  imports: [


    // Para los formularios reactivos
    ReactiveFormsModule,
  ]
})
export class TasksModule { }
```


## html

```html
<form (ngSubmit)="guardar()" [formGroup]="formulario" class="container" autocomplete="off">

    <div class="mx-auto">        

        <!-- Identificador de la tarea -->
        <input id="tareaId" name="tareaId" type="hidden" value="-1">

        <!-- Título de la incidencia. Es texto -->
        <div class="row">

            <div class="col-md-12 mb-3">
                <label for="titulo" class="form-label">Título</label>

                <!-- El título -->
                <input formControlName="titulo" type="text" class="form-control" [readonly]="actualizando">
                <span class="form-text text-danger" *ngIf="esCampoNoValido('titulo')">{{ mensajeErrorCampo('titulo') }}</span>
            </div>

        </div>
          
        <div class="tab-pane fade show active" id="datos" role="tabpanel" aria-labelledby="datos-tab">
            
            <!-- CAMPOS EN EL FORMULARIO -->
            <div class="row">

                <!-- Informador. BUSQUEDA -->
                <label for="informadorDatalist" class="form-label">Informador</label>
                
                <input 
                    id="informadorDatalist"
                    [formControl]="nombreInformador" 
                    name="informador" 
                    campo_validacion="informadorId" 
                    class="form-control" 
                    list="informadorDatalistOptions" 
                    placeholder="Type to search..."
                    
                    (input)="nombreInformadorInput($event)"
                    (change)="nombreInformadorChange($event)"
                    >
                <input formControlName="id_informador" type="hidden">
                <div class="invalid-feedback"></div>                
                <datalist id="informadorDatalistOptions">
                    <option *ngFor="let e of selectInformador">{{e.texto}}</option>
                </datalist>            
            </div>        
    
            <div class="row">

                <!-- Asignado. BUSQUEDA en tabla por nombre que muestre opciones -->
                <div class="col-md-5 mb-1">
                    <label for="asignado" class="form-label">Asignado</label>                                        
                    <select id="asignado" formControlName="id_asignado" class="form-select">
                        <option *ngFor="let u of selectAsignado" [value]="u.id">{{ u.texto }}</option>
                    </select>                                    
                    <span class="form-text text-danger" *ngIf="esCampoNoValido('id_asignado')">El usuario asignado no puede ser el mismo que el informador</span>
                </div>
            </div>        
    
            <div class="row">
                <!-- Tipo. Combo AJAX -->
                <div class="col-md-5 mb-1">

                    <label for="tipo" class="form-label">Tipo</label>
                    <select id="tipo" formControlName="id_tipo_tarea" class="form-select">
                        <option *ngFor="let tt of selectTiposTarea" [value]="tt.id">{{ tt.texto }}</option>
                    </select>                
                    <div class="invalid-feedback"></div>
    
                </div>

                <!-- Estado. Combo AJAX. Depende del tipo -->
                <div class="col-md-5">

                    <label for="estado" class="form-label">Estado</label>
                    <select id="estado" formControlName="id_estado" class="form-select">
                        <option *ngFor="let e of selectEstadosTarea" [value]="e.id">{{ e.texto }}</option>                        
                    </select>                
                    <div class="invalid-feedback"></div>
                </div>
            </div>        
    
            <div class="row">
                <!-- Fecha de alta. Por omisión. El día en que se da de alta -->
                <div class="col-md-3 mb-1">
                    <label for="fechaalta" class="form-label">Fecha alta</label>
                    <input formControlName="fecha_alta" type="date" class="form-control">
                </div>
    
                <!-- Fecha vencimiento. La escoge el usuario -->
                <div class="col-md-3">
                    <label for="fechavencimiento" class="form-label">Fecha vencimiento</label>
                    <input formControlName="fecha_vencimiento" type="date" class="form-control">
                </div>
    
                <!-- Hora vencimiento. La escoge el usuario -->
                <div class="col-md-4">
                    <label for="horavencimiento" class="form-label">Hora vencimiento</label>
                    <input formControlName="hora_vencimiento" type="time" class="form-control">
                </div>
            </div>        
            
            <!-- Descripción -->
            <div class="row">
                <div class="col-md-12 mb-1">
                    <label for="descripcion" class="form-label">Descripción</label>
                    <textarea formControlName="descripcion" class="form-control" rows="2"></textarea>
                    <span class="form-text text-danger" *ngIf="esCampoNoValido('descripcion')">{{ mensajeErrorCampo('descripcion') }}</span>
                </div>
            </div>        

        </div> 

    </div>

</form>    

<hr>

<!-- Botones de navegación -->
<div class="row ">
    <span class="col-7"></span>
    <button class="btn btn-primary col-2 m-3" routerLink="/tareas">Volver</button>
    
    <!-- Desabilita el guardar si hay una operación en curso -->
    <button class="btn btn-warning col-2 m-3" (click)="guardar()" [disabled]="formulario.pending">Guardar</button>
</div>
<!--
<div>
{{ formulario.valid }}
<br>
{{ formulario.errors| json }}
<br>
{{ formulario.controls['titulo'].errors | json }}
<br>
{{ formulario.getRawValue()| json }}
</div>
-->
```

## TypeScript

```ts
  // Defino el formulario
  // En esta definición incluyo
  // - Nombres de los campos. Deben coincidir con los del objeto
  // - Opciones de los campos
  // - Validaciones locales
  // - Validaciones asíncronas
  formulario: FormGroup = this.fb.group({
      id_tarea          : [-1],

      titulo            : [ '', 
                            [ Validators.required, this.validacionService.validarEmpiezaMayuscula ],
                            [ this.validacionTituloService ]
                          ],

      id_informador     : ['', [ Validators.required] ],
      id_asignado       : ['', [ Validators.required] ],

      id_tipo_tarea     : ['', [ Validators.required] ],

      id_estado         : [ {
                              value: -1, 
                              disabled: true
                            }, 
                            [ Validators.required] 
                          ],

      fecha_alta        : [''],
      fecha_vencimiento : [''],
      hora_vencimiento  : [''],

      descripcion       : ['', [ Validators.required] ],

  }, {  
    // 008 Este segundo argumento que puedo enviar al formgroup permite por ejemplo ejecutar
    // validadores sincronos y asíncronos. Son validaciones al formgroup
    validators: [ this.validacionService.camposNoIguales('id_informador', 'id_asignado') ]
  });

  // Defino campos sueltos auxiliares que voy a utilizar
  // En este caso utilizo este para el datalist aunque en este caso
  // lo podría meter dentro del formulario ya que no va a afectar al funcionamiento.
  nombreInformador    : FormControl = this.fb.control('', Validators.required);
  
  // Estos arrays contendrán los elementos que voy a cargar en los selects
  selectInformador    : EntradaSelect[] = [];
  selectAsignado      : EntradaSelect[] = [];
  selectTiposTarea    : EntradaSelect[] = [];
  selectEstadosTarea  : EntradaSelect[] = [];

  // Indica si la tarea se está actualizando
  actualizando: boolean = false;

  //-------------------------------------------------------------------------------------
  // Inicialización
  //-------------------------------------------------------------------------------------

  constructor(

    private activatedRoute    : ActivatedRoute,
    private fb                : FormBuilder,
    private router            : Router,

    private dialogService     : DialogService,
    
    private estadosService    : EstadosService,
    private tareasService     : TareasService,
    private tiposTareaService : TiposTareaService,
    private usuariosService   : UsuariosService,

    private validacionService       : ValidacionService,
    private validacionTareasService : ValidacionTareasService,
    private validacionTituloService : ValidacionTituloService

  ) { }

  /**
   * Inicialización de la página
   */
  ngOnInit(): void {

    // Si no estamos en modo edición, sale de aquí
    if(this.router.url.includes('editar')) {    
      this.cargarTarea();
      this.actualizando = true;

      // Se carga la validación asíncrona en caso de edición
      this.formulario.get('titulo')?.clearAsyncValidators();
    }

    // Carga el contenido de los selects desde la base de datos
    this.cargarSelectUsuarioAsignado();
    this.cargarSelectTiposTarea();

    // Cuando se selecciona un tipo de tarea, se debe cargar el combo de 
    // estados para que contenga los estados para ese tipo de tarea
    this.formulario.get('id_tipo_tarea')?.valueChanges.subscribe(id_tipo_tarea => {      
      this.cargarSelectEstados(id_tipo_tarea);
    });  
  }
  ```


