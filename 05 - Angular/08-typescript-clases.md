# Clases
En este apartado vamos a tratar las clases en TypeScript. Las clases en TypeScript son realmente muy similares a las clases en JavaScript

## Conceptos básicos
Definimos aquí la clase persona en TypeScript. Ponemos además atributos de visibilidad que permiten uno u otro acceso.

```ts
    class Persona {

        // No permite acceder al atributo
        private nombre: string;
        
        // Permite acceder al atributo. Este modificador de acceso
        // es el que se usa por defecto
        public edad: number;

        // Este atributo es público
        genero: string;

        // Este atributo es un atributo de clase
        static numeroPersonas: number;
    }

    const paco = new Persona();
```
## Constructores I
Vamos a completar nuestra clase ahora añadiendo un constructor. El constructor es la función que se invoca en el momento de utilizar el operador new. La finalidad es inicializar la nueva instancia de la clase que se ha creado.

```ts
    class Persona {

        // No permite acceder al atributo
        private nombre: string;
        
        // Permite acceder al atributo. Este modificador de acceso
        // es el que se usa por defecto
        public edad: number;

        // Este atributo es público
        genero: string;

        // Este atributo es un atributo de clase
        static numeroPersonas: number;

        // Constructor
        constructor(nombre: string) {
            console.log("Persona inicializada");

            this.nombre = nombre;
        }
    }

    // Creo un objeto y lo inicializo
    const laura = new Persona('Laura');
```

## Constructores II
El anterior método de inicialización es muy claro, pero requiere escribir mucho código. En TS disponemos de otra forma más abreviada para definir una clase con un constructor. A continuación un ejemplo equivalente al anterior:

```ts
    class Persona {

        // Este atributo es un atributo de clase
        static numeroPersonas: number;

        // Constructor
        constructor(
            // Al poner aquí el modificador de acceso, se van a crear
            // las propiedades del mismo modo que si hubiéramos definido
            // antes los atributos.
            public nombre: string,
            public edad: number,
            public genero: string,
        ) {
            console.log("Persona inicializada");
        }
    }

    // Creo un objeto y lo inicializo
    const laura = new Persona('Laura', 10, 'Mujer');
```

## Herencia
Ahora vamos a hablar de la herencia entre clases. 

```ts
    // La clase alumno extiende a la clase persona. Añade el atributo nota
    class Alumno extends Persona {

        constructor(
            public nombre: string,
            public edad: number,
            public genero: string,
            public nota: number         // Define aquí el atributo nota
        ) {            
            // Invoca al constructor de Persona pasando los parámetros 
            // que requiere
            super(nombre, edad, genero);            
        }
    }

    // Creo un objeto y lo inicializo
    const laura = new Alumno('Laura', 10, 'Mujer', 5.5);
```




