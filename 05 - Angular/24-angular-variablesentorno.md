# Variables de entorno
Las variables de entorno están en los archivos:

- src
  - environments
    - environment.ts: variables de entorno para desarrollo. 
    - environment.prod.ts: variables de entorno para producción. Son las que se empaquetarán con nuestro proyecto cuando generemos la aplicación.

## Configuración de nuestro proyecto
En nuestro caso, vamos a configurar la ruta al servicio que vamos a utilizar y algunos otros parámetros. Como para nosotros no va a haber diferencia entre el entorno de desarrollo y el de producción, ya que van a estar en el mismo sitio, nuestos ficheros van a contener exactamente la misma configuración.

### environment.ts
Archivo para el entorno de desarrollo.

```ts
export const environment = {
  // Observad como está marcado que no es producción
  production: false,
  
  // URL Base de los servicios de taskman
  taskmanBaseUrl: 'http://localhost/daw/daw2-dwec-profesorado/taskman/01-taskman-php-ajax/', 

  // Tiempo en milisegundos que un usuario debe estar sin pulsar una tecla
  // para que se acepte la entrada para lanzar por ejemplo un desplegable
  userInputDebounceDelay: 500,

  // Activa el modo depuración. Desactiva la autenticación.
  debug: 1
};
```

### environment.prod.ts
Lo mismo que antes, pero en este caso en el fichero para producción
```ts
export const environment = {
  // Observad como está marcado que no es producción
  production: false,
  
  // URL Base de los servicios de taskman
  taskmanBaseUrl: 'http://localhost/daw/daw2-dwec-profesorado/taskman/01-taskman-php-ajax/', 

  // Tiempo en milisegundos que un usuario debe estar sin pulsar una tecla
  // para que se acepte la entrada para lanzar por ejemplo un desplegable
  userInputDebounceDelay: 500,

  // Activa el modo depuración. Desactiva la autenticación.
  debug: 1
};
```

## Referenciar las variables
La referencia la haríamos del mismo modo que se referencia al atributo de una clase. A continuación tenemos algunas líneas de referencia. **Es importante que nos aseguremos de que el import lo hacemos del fichero de desarrollo**.

```ts
// Importa el archivo environment. OJO. Hemos de importar el de desarrollo SIEMPRE
import { environment } from 'src/environments/environment';

// ...

export class TareasService {

  // Esto pasa a ser un atributo de nuestra clase donde tenemos el dato. 
  // Ahora podemos referenciarlo como this.baseUrl
  private baseUrl = environment.taskmanBaseUrl;

}

```
