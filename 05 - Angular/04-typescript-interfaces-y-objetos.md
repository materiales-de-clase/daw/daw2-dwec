# Objetos
En JavaScript, a la hora de definir nuestros objetos, podemos añadir las propiedades que queramos sin restricciones. Esto implica que hasta que nuestro script se ejecuta, realmente no podemos saber si cierto código es correcto. En TypeScript nos acercamos un poco más a los lenguajes de programación modernos. En este caso, se incluye el concepto de Interface, que viene a tener un significado similar al que tiene en Java.

## Declaración de un interface
Para declarar un interface se puede utilizar la siguiente sintaxis:

```ts
    interface Persona {

        nombre: string;
        edad: number;
        hermanos: string[];

        domicilio?: string;     // La interrogación indica que el atributo es
                                // opcional
    }
```

## Declaración de un interfaces con interfaces anidados
Si un interfaz contiene otros objetos, se recomienda definir dos interfaces del siguiente modo:

```ts
    interface Persona {

        nombre: string;
        edad: number;
        hermanos: string[];

        direccion: Direccion; // dirección será un objeto de tipo Dirección
    }

    // Define las características de una ddirección
    interface Direccion {
        ciudad: string;
        calle: string;
        cp: number;        
    }
```

## Declaración de un objeto 
Una vez que tenemos el interfaz declarado, podemos definir objetos que lo implementen. A continuación tenemos dos ejemplos con el interfaz Persona.

```ts
    // Este objeto no tiene atributo domicilio pero es válido, 
    // ya que estaba marcado como opcional
    const persona1: Persona = {
        nombre: 'Paco',
        edad: 98,
        hermanos: [ 'Manolo', 'Felipe' ]
    };

    // Aunque la propiedad no está, al ser opcional la puedo asignar posteriormente
    persona1.domicilio = 'Mi calle';

    // Este objeto declara valores para todas las propiedades
    const persona2: Persona = {
        nombre: 'Manolo',
        edad: 98,
        hermanos: [ 'Paco', 'Felipe' ],
        domicilio: 'Mi calle'
    };
```

