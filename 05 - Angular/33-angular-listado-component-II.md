# Lista de tareas
Ahora vamos a ir un poco más allá y vamos a implementar componentes tanto para el buscador como para la tabla. Esto permitirá tener código reuti

## Tabla
En primer lugar vamos a crear un componente que nos permita listar tareas. 

#### Crear el componente
Para crear el componente, se puede escribir un comando como el que sigue:

```cmd
C:\MIPROYECTO> ng generate component tasks/components/TablaTareas --skip-tests --style="none"
```

#### Plantilla HTML
El código HTML es muy similar al que ya teníamos.

```html
<!-- Aquí está la tabla donde se van a insetar los resultados -->
<table class="table table-bordered table-striped text-center table-light table-hover align-items-center">

    <!-- Encabezado de la tabla -->
    <thead class="table text-white bg-primary">
        <tr>
            <th scope="col">id</th>
            <th scope="col">Título</th>
            <th scope="col">Tipo</th>
            <th scope="col">Estado</th>
            <th scope="col">Fecha Vencimiento</th>
            <th scope="col">Hora Vencimiento</th>
            <th scope="col"></th> <!-- Mostrar detalles -->
            <th scope="col"></th> <!-- Editar -->
            <th scope="col"></th> <!-- Eliminar tarea -->
        </tr>
    </thead>
    
    <!-- 
        Cuerpo de la tabla. Aquí es donde se van a insertar las tareas
     -->
    <tbody>
        <tr *ngFor="let tarea of tareas; let i = index">
            <td>{{ tarea.id_tarea }}</td>
            <td>{{ tarea.titulo }}</td>
            <td>{{ tarea.tipo }}</td>
            <td>{{ tarea.estado }}</td>
            <td>{{ tarea.fecha_vencimiento | date }}</td>
            <td>{{ tarea.hora_vencimiento }}</td>
            <td>
                <!-- Enlace a ver la tarea -->
                <i [routerLink]="['../ver', tarea.id_tarea ]" class="btn btn-success bi bi-eye"></i>
            </td>                
            <td>
                <!-- Enlace a la edición de la tarea -->                                
                <i [routerLink]="['../editar', tarea.id_tarea ]" class="btn btn-warning bi bi-pencil"></i>
            </td>    
            <td>
                <!-- Eliminar la tarea -->
                <i (click)="borrarTarea(i)" class="btn btn-danger bi bi-trash"></i>
            </td>
        </tr>        
    </tbody>
</table>
```

#### Código TypeScript
Aquí tenemos el código de nuestro componente

```ts
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Tarea } from '../../interfaces/tarea.interface';

@Component({
  selector: 'app-tabla-tareas',
  templateUrl: './tabla-tareas.component.html'
})
export class TablaTareasComponent {

  /**
   * Esto es el array de tareas que se va a renderizar
   */
  @Input() tareas: Tarea[] = [];

  /**
   * Evento que se va a emitir desde este componente cuando se quiera 
   * borrar une tarea
   */
  @Output() onBorrar: EventEmitter<number> = new EventEmitter();

  constructor() { }

  /**
   * Bara borrar tarea se pasa el índice dentro de la tabla de tareas.
   * Más que nada porque luego se evita tener que recorrer la tabla para hacer la eliminación
   * 
   * @param indice 
   */
  borrarTarea(indice: number): void {
    this.onBorrar.emit(indice);
  }
}
```

Merece la pena resaltar

- **@Input**: Que podemos utilizar para enviar datos a nuestro componente
- **@Output**: Que podemos utilizar para declarar eventos personalizados que nuestro componente puede disparar.

#### Utilizar
Para utilizar este componente, dentro de lo que era nuestra página podemos utilizar cierto código tanto en el HTML como en el TypeScript

```html
<!-- 
    Tabla con los resultados.
 -->
<app-tabla-tareas [tareas]="tareas" (onBorrar)="borrarTarea($event)"></app-tabla-tareas>
```

```ts
  /**
   * Borrar tarea recibe el evento. El evento de la tabla de tareas emite el ID en la tabla
   * 
   * @param indice 
   */
  borrarTarea(indice: number) {
    // Aquí implementaríamos el borrar    
  }
```


## Buscador
Para el buscador algo similar, pero en este caso vamos a implementar un buscador que pueda ser utilizado para cualquier página en que necesitemos un buscador.

#### Crear el componente
La siguiente secuencia permite crear el componente

```cmd
C:\MIPROYECTO> ng generate component shared/components/FiltroBusqueda --skip-tests --style="none"
```

#### Plantilla HTML
No olvidar importar FormsModule. Es imprescindible para poder incluir las etiquetas de Angular.

```html
<form (ngSubmit)="buscar()" autocomplete="off">
    <input type="text" 
           name="termino" 
           class="form-control" 
           [placeholder]="placeholder" 
           [(ngModel)]="termino" 
           (keypress)="onKeyPress($event)">
</form>                                                              
``` 

#### Código TypeScript
```ts
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { debounceTime, Subject } from 'rxjs';
import { environment } from '../../../../environments/environment.prod';

@Component({
  selector: 'app-filtro-busqueda',
  templateUrl: './filtro-busqueda.component.html'
})
export class FiltroBusquedaComponent {

  // VARIABLES DE ENTORNO
  // Pausa para rebote
  private userInputDebounceDelay: number = environment.userInputDebounceDelay;

  // 1 Término que se relaciona con ngModel con el formulario
  termino: string = '';
  private ultimoTerminoEnviado: string = '??';

  // 2. Este input es para el placeholder. Permite recibirlo para mostrarlo de forma personalizada  
  // Al ser esto una propiedad, se puede referenciar ahora directamente en el modelo utilizando [] para
  // referenciar a su valor. De este modo, el valor que me han pasado pasa al modelo de forma transparente
  @Input() placeholder: string = '';

  // 3. Para notificar cuando se hace una búsqueda vamos a poner ahora que se haga
  // una notificación. Usaremos el output y un event Emmiter en este caso de tipo cadena
  // ya que lo que vamos a emitir es el término. Se podría utilizar un interfaz para emitir
  // un objeto con varios atributos.
  @Output() onBusquedaEjecutada: EventEmitter<string> = new EventEmitter();

  // 4 Declaramos un Subject que no es otra cosa que un sujeto observable  
  // Aquí lo vamos a utilizar para detectar cuando el usuario lleva más de 
  // X tiempo sin pulsar una tecla.
  debouncer: Subject<string> = new Subject();

  constructor() { }

  // 4 Implemento este método porque lo voy a necesitar para el debouncer
  // se lanza una única vez cuando se crea el compomente para inicializar
  // nuestro componente.
  ngOnInit(): void {

    // Se subscribe al debouncer. Que no es más que un objeto al que puedo suscribirme
    this.debouncer
      // 4. Finalmente, el pipe es algo que puedo usar aquí para transformar la información
      // En este caso, mete una pausa y solo envía información en caso de que pase un lapso de tiempo
      .pipe(
        debounceTime(this.userInputDebounceDelay)   
      )
      .subscribe(valor => {        
        // 3. Cuando llego aquí, el usuario lleva un tiempo sin pulsar ninguna tecla.
        // Lanzo ahora el evento que me va a permitir enviar el término        

        // Muestra el valor que se va a emitir
        this.fireOnBusquedaEjecutada(valor);
      }
    )

  }

  // 5. Ahora implemento el gestor del evento tecla presionada. Este evento lo tengo asignado
  // en el html par aque sea invocado cada vez que se pulsa una tecla
  onKeyPress(event: any) : void {

    // Cuandos e pulsa una tecla envía datos al debouncer. 
    this.debouncer.next( this.termino );    
  }

  // Lanza la búsqueda
  buscar() : void {

    // Esto hace que el enter fuerce la búsqueca
    this.ultimoTerminoEnviado = '??';

    // Lanza la búsqueda
    this.fireOnBusquedaEjecutada(this.termino);
  }

  /**
   * Lanza el evento 
   * 
   * @param termino 
   */
  private fireOnBusquedaEjecutada(termino: string) : void {
 
    // Solo se envía cuando el término ha cambiado
    if(termino == this.ultimoTerminoEnviado)
      return;

    // Guarda el término
    this.ultimoTerminoEnviado = termino;

    // Muestra el término
    console.log(termino);

    // Lanza el evento
    this.onBusquedaEjecutada.emit(this.termino);
  }

}
```

#### Utilizar
Para incluir el filtro, tendríamos que incluir el siguiente código HTML

```html
        <!-- Filtro de búsquedas -->
        <app-filtro-busqueda 
                placeholder="Introduce el término de búsqueda"
                (onBusquedaEjecutada)="buscar($event)">
        </app-filtro-busqueda>
```

Luego, el TypeScript quedaría algo como 

```ts
/**
   *  Método a invocar para lanzar la búsqueda 
   */   
  buscar(termino: string): void {
   
    // Aquí se hace la búsqueda por el término de búsqueda
    this.cargarTareas(termino);
  }
```


## Conclusiones
En este punto, deberías tener la página de tareas pero con la particularidad de que la funcionalidad la das con componentes.

