# Lista de tareas
Vamos ahora a actualizar nuestro listado de tareas para que utilice el servicio.

## Array de tareasç

```ts
  // Lista de tareas
  tareas: Tarea[] = [];
```

## Constructor

```ts
  constructor(
        
    // Servicio para mostrar diálogos
    private dialogService: DialogService,

    // Acceso al backend
    private tareasService: TareasService
    
    ) {}
```

## OnInit
Parte del ciclo de vida de un componente. Se llama después de que Angular haya concluido la inicialización del componente. Se pueden hacer inicializaciones adicionales.

```ts
  ngOnInit(): void {

    // Carga las tareas
    this.cargarTareas();
  }
```

## Descargar las tareas

```ts
/**
   * 
   * @param filtro Método para cargar las tareas
   * 
   */
  private cargarTareas(filtro: string | undefined = undefined) {
    
    // Cuando la pantalla se muestra se tienen que mostrar las tareas.
    this.tareasService.getTareasPorTitulo(filtro)
     
      .pipe(

        // Este tap lo hago solo para mostrar los datos que pasan por aquí
        tap(console.log)
      )
      
      .subscribe( response => {
        
        // Si la respuesta es OK, la lista de tareas se asigna a la respuesta
        if(response.ok) {
          
          this.tareas = response.datos;    

        } else {

          // Muestra el mensaje de error
          this.dialogService.mostrarMensaje(response.mensaje, 'ERROR');
        }
      });
  }
```

## Borrar tareas
Para borrar tareas, añadimos la implementación de nuestro borrar.

```ts
  /**
   * Borrar tarea recibe el evento. El evento de la tabla de tareas emite el ID en la tabla
   * 
   * @param indice 
   */
  borrarTarea(indice: number) {

    // Obtiene la tarea a eliminar
    const tarea = this.tareas[indice];
 
    // Si el usuario me confirma que quiere eliminar la tarea, la elimina
    this.dialogService.solicitarConfirmacion(`¿Está seguro de que quiere eliminar la tarea: ${tarea.titulo}?`, 'Atención',
      () => {

        // Elimina la tarea
        this.tareasService.borrarTarea(tarea).subscribe((response) => {

          // Si la respuesta es OK, la lista de tareas se asigna a la respuesta
          if(response.ok) {
        
            // Elimina la tarea del array
            this.tareas.splice(indice, 1);

          } else {

            // Muestra el mensaje de error
            this.dialogService.mostrarMensaje(response.mensaje, 'ERROR');
          }
        });      
      }
    );
  }
```

## Conclusión
En este punto, deberíamos tener un listado totalmente funcional que cargue los datos desde el servicio.


